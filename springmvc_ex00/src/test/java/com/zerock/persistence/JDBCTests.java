package com.zerock.persistence;

import static org.junit.Assert.fail;

import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Test;

import com.mysql.jdbc.Connection;

import lombok.extern.log4j.Log4j;

@Log4j
public class JDBCTests {
	static String driverClass = "com.mysql.jdbc.Driver";
	static String url = "jdbc:mysql://localhost:3306/shopmall";
	//String url = "jdbc:mariadb://localhost:3306/shopmall";
	static String id = "dev";
	static String pw = "pass";
	//String driverClass = "org.mariadb.jdbc.Driver";
	static {
		try {
			Class.forName(driverClass);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testConnection() {
		Connection conn = null;

		try {
			conn = (Connection) DriverManager.getConnection(url, id, pw);
			log.info(conn);
			log.info("mysql connected!!");
		} catch (SQLException e) {
			log.info("데이터베이스 연결 오류! " + e.getMessage());
			log.info("mysql failed to connect!!");
			fail(e.getMessage());
		}
	}
}
