#############################################
# QUERY 연습, 
# 2021. 10.13 CRUD 기능
# tutorials: w3schools mysql 
#############################################

CREATE DATABASE; 
CREATE TABLE;
 - PRIMARY key
 - FOREIGN key
 - NOT null
 - default
 - check
 - unique

DROP DATABASE;
DROP TABLE;
ALTER TABLE;

SELECT * FROM 테이블이름;
SELECT * FROM 테이블이름 WHERE 조건사항;

INSERT INTO 테이블이름(항목1이름, 항목2이름...) 
 VALUE (항목1의값, 항목2의 값);
INSERT INTO 테이블이름 
 VALUE (항목1의값, 항목2의 값); 
 -> 테이블의 생성된 항목의 순서대로 모든 항목의 값을 입력함
 -> DESC 테이블이름; 의 명령으로 항목순서를 확인해봐야함.
 
UPDATE 테이블이름 SET 항항목1의이름=항목1의값
 , 항목2의이름=항목2의값
 where 조건선택;
UPDATE 테이블이름 SET 항항목1의이름=항목1의값
 , 항목2의이름=항목2의값
 -> 테이블에 있는 데이터를 몽땅 다 바꿔버리므로 조심해야됨.
 -> 그래서 조건문장을 써서 꼭 한건에 대해서만 변경(업데이트)함.

DELETE FROM  테이블이름 WHERE 조건선택;
DELETE FROM  테이블이름;
 -> 조건문장을 사용해서 데이터를 선택하지 않으면 
    테이블에 있는 데이터들을 몽땅 다 지워버림.

SELECT 구문에서 기억해야 하는 내용!
  - subquery 의 사용
  - JOIN 구문의 사용

추가 학습할 목록:
 INDEX생성
  -> 데이터의 검색을 빠르게 할 수 있도록 키를 생성하는 것
 PROCEDURE 의 생성
  -> QUERY 구분의 응용을 함수처럼 생성하는 것
 VIEW의 생성
  -> QUERY결과를 만들어 놓는 가상의 테이블이라고 할 수 있음
  
ENUM
사과, 배, 감 
0 1 2     

데이터타입: 숫자, 문자, 날짜
데이터타입 사이의 변환:  
  숫자-> 문자, 
  날자-> 문자

#############################
# * 날자 데이터 형식: 
#############################
DATE - YYYY-MM-DD 형식
DATETIME - 형식: YYYY-MM-DD HH:MI:SS
TIMESTAMP - 형식: YYYY-MM-DD HH:MI:SS
YEAR - YYYY 또는 YY 형식
 
SELECT NOW();
 -- 2021-10-13 10:48:44
SELECT SYSDATE();
 -- 2021-10-13 11:18:27
SELECT CURRENT_TIMESTAMP;
 -- 2021-10-13 11:18:52
 
select str_to_date(NOW(), '%Y-%m-%d') AS str1;
 -- 2021-10-13
SELECT date_format(NOW(), '%Y-%m-%d') AS str2;
-- 2021-10-13 
SELECT date_format(NOW(), '%r') AS str3;
-- 10:57:15 AM
SELECT date_format(NOW(), '%M %e, %Y') AS str4;
-- October 13, 2021
SELECT DATE_FORMAT('2021-10-15 13:23:30', '%W %D %M %Y');
-- Friday 15th October 2021

사용하는 FORMAT: 
  '%Y-%m-%d' -- 년 월 일
  '%W, %M %Y' -- 요일 달 년도
  '%h:%i:%s' -- 시간 분 초
  '%p %h:%i' -- 오후/오전 시간 분
  참조)  https://blogpack.tistory.com/483
  https://www.w3resource.com/mysql/date-and-time-functions/mysql-date_format-function.php

SELECT CURRENT_TIME AS 현재시간;
-- 11:03:38
SELECT CURRENT_DATE AS 현재날자;
-- 2021-10-13
SELECT CURRENT_TIMESTAMP;
-- 2021-10-13 11:06:30
SELECT PERIOD_DIFF('202105', '202008') AS '기간(달)';
-- 9
SELECT PERIOD_ADD('202105', 5) AS '기간(달)';
-- 202110

SELECT DATE_ADD('2021-10-05', INTERVAL 10 DAY) as 날짜1;
-- 2021-10-15
SELECT DATE_ADD(NOW(), INTERVAL 10 DAY) as 날짜1;
-- 2021-10-23 11:13:00
SELECT DATE_ADD(NOW(), INTERVAL 10 MONTH) as 날짜2;
-- 2022-08-13 11:13:37


#------------------------------
# SELECT DISTINCT 
# : 중복된 이름을 갖는 항목은 한 건으로 처리해서 출력합니다.
#------------------------------employeesemployees
SELECT * FROM departments;
SELECT * FROM titles;
SELECT count(title) FROM titles;
-- 443308 # 역대 입사한 직원들의 수
-- (emp_no를 부여받을때 직군도 부여함)
SELECT count(distinct title) FROM titles;
-- 7  # 직군 7가지입니다.

SELECT * from titles WHERE title IN ('staff', 'engineer');
SELECT COUNT(*) from titles WHERE title IN ('staff', 'engineer');
SELECT COUNT(*) from titles WHERE title IN ('staff', 'engineer') 
 AND from_date > '1996-01-01';

SELECT * from titles WHERE title IN ('staff', 'engineer') 
 AND from_date > '1996-01-01'
 ORDER BY from_date desc;

SELECT * from titles WHERE title IN ('staff', 'engineer') 
 AND from_date > '1996-01-01'
 ORDER BY from_date desc
 LIMIT 10;

SELECT * from titles WHERE title IN ('staff', 'engineer') 
 AND from_date > '1996-01-01'
 ORDER BY from_date asc
 LIMIT 10;
 
SELECT COUNT(*) FROM titles
 WHERE from_date BETWEEN '2000-12-30' AND '2001-01-01'; 


USE classicmodels;
DESC customers;

SELECT * FROM Customers WHERE City LIKE 's%';
SELECT customername, addressline1, addressline2
 , city, postalcode, country
 FROM customers
 WHERE city LIKE 's%';

SELECT customername, addressline1, addressline2
 , city, postalcode, country
 FROM customers
 WHERE city LIKE '%de%';
 
SELECT distinct city FROM customers;
------------------------------
ref:  https://www.mysqltutorial.org/how-to-load-sample-database-into-mysql-database-server.aspx
DATABASE: classicmodels

USE classicmodels;
DESC customers;
SELECT * FROM customers LIMIT 10;
SELECT COUNT(*) FROM customers;
SELECT * FROM customers ORDER BY customerNumber ASC;
SELECT * FROM customers ORDER BY customerNumber DESC;


# where: and or not
SELECT * FROM Customers
 WHERE Country='Germany' AND City='Berlin';

SELECT * FROM customers
 WHERE contactlastname LIKE '%tor%' or country='France';

SELECT * FROM customers
 WHERE (contactlastname not LIKE '%tor%') AND (country !='France');

# select 정렬하기 
# order by desc, # order by asc  
SELECT * FROM Customers
 ORDER BY Country asc, CustomerName asc;
 
예시) 성적표 (이름, 성적)
SELECT * FROM 성적표
 ORDER BY 이름 ASC, 성적 DESC; 
 -- 이름오름차순 정렬, 이름같으면 성적 큰값에서 낮은 성적 순으로 출력

testdb   


DESC testdb.persons;

SELECT * FROM testdb.persons;
INSERT INTO testdb.persons 
 VALUE('0002', 'go', 'gildong', '30' );
INSERT INTO testdb.persons 
 VALUE(3, 'hong', 'gilseo', 22 );

-- 여러개의 데이터를 입력할 때: 
INSERT INTO testdb.persons 
 VALUE
 (4, 'hong', 'gilnam', 21 )
 ,(5, 'hong', 'gilbok', 24 ); 
 
 
 # is null ? null이라고 하는 것은 숫자 0, 문자 공백 ' ' 과는 다르다.
 # null은 아직 아무런 값을 입력하거나 할당하지 않은 상태.
USE clsicmodels;
SELECT CustomerName, ContactlastName, Addressline1, phone, Addressline2
 FROM Customers
 WHERE Addressline2 is NULL;

SELECT CustomerName, ContactlastName, Addressline1, phone, Addressline2
 FROM Customers
 WHERE Addressline2 is not NULL;
 
SELECT * FROM testdb.persons; 
UPDATE testdb.persons 
 SET firstname='gilseo', age=26 
 WHERE id=3 AND lastname='hong';

 # 제약조건
 # 제약사항(constraints 콘스트레인츠)
  - 꼭 이렇게 사용해주세요.
  - 이럴때는 사용할 수 없어요.
  - TABLE에 값을 입력할 때 (INSERT INTO)
    값을 조건에 맞게 입력하거나
	 , 사용할 수 없는 조건이 TABLE생성되어 있는 경우
	 
 # 대표적인 제약사항
  - NULL이 오면 안된다요
  - CHECK 조건의 예: 
    남녀구분코드에는 값이 m, w 두 값중에서 하나만 사용하세요.
  - UNIQUE 는 테이블에 입력되는 항목의 값이 
     항목내에서 같은 값이 존재하면 입력할 수 없어요.
     같지 않은 유일한 값만 입력해주세요.
    예) id 입력할 때 1,2,3,4,5,6,...의 값은 서로 같지 않아요.
	  
 # primary key : 주된 키, 
  : 테이블의 데이터를 구분하는 키(KEY)
  특징: NULL이 아님(테이블에 있는 데이터는 꼭 갖고 있는 값)
     -> NOT NULL (값이 꼭 있어요!)
   UNIQUE 한 값을 사용해요. 
	 같은 값을 갖고 있는 데이터는 테이블 안에 내것 뿐이 없어요. 
  
  SELECT * FROM testdb.persons;
  INSERT INTO testdb.persons VALUES(5, 'go', 'gilbok', '25');    
 
  DELETE from testdb.persons WHERE id=5 AND lastname='go';
 
  CREATE TABLE `persons` (
	`id` INT(11) NOT NULL,d
	`lastname` VARCHAR(255) NOT NULL,
	`firstname` VARCHAR(255) NULL DEFAULT NULL,
	`age` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`, `lastname`)
  );
  INSERT INTO testdb.persons value
   (1, 'hong', 'gildong', 20)
   ,(2, 'go', 'gildong', 30)
   ,(3, 'hong', 'gilseo', 26)
   ,(4, 'hong', 'gilnam', 21)
   ,(5, 'go', 'gilbok', 24);

# where없이 delete하면 테이블의 내용이 모두 삭제될 수 있어요.   
  DELETE FROM testdb.persons; 

# min(), max() 함수의 사용
DESC products;
SELECT * FROM products LIMIT 10;

SELECT MIN(buyprice) AS 최저가, MAX(buyprice) AS 최고가 
 FROM  products;
--  최저가: 15.91 , 최고가: 103.42
SELECT * FROM products 
 WHERE buyprice = 15.91 ;
    (SELECT MIN(buyprice) FROM products);

SELECT *  
1) 최고가 상품의 상품이름, 상품제조사, 재고수량, 가격을 출력하세요.
SELECT productname, productvendor, quantityinstock, buyprice
 FROM products 
 WHERE buyprice=(SELECT MAX(buyprice) FROM products);
조건사항: 최고가 
 SELECT MAX(buyprice) FROM products;
  
 
2) 최고가와 최저가 사이에 있는 상품은 몇 개가 있나요?

SELECT COUNT(*) FROM products
 WHERE buyprice > 최저가 AND buyprice < 최고가;
최저가: 
  SELECT MIN(buyprice) FROM products; -- 15.91
최고가: 
  SELECT MAX(buyprice) FROM products; -- 103.42

SELECT COUNT(*) FROM products
 WHERE buyprice > 15.91 AND buyprice < 103.42; 
   
SELECT COUNT(*) FROM products
 WHERE buyprice > (SELECT MIN(buyprice) FROM products) 
  AND buyprice < (SELECT MAX(buyprice) FROM products); 

# avg(), sum()
SELECT AVG(buyprice) FROM products; -- 54.395182

SELECT sum(buyprice) FROM products; -- 5983.47 

SELECT COUNT(buyprice) AS 총수량,
 SUM(buyprice) AS 총합산, 
 AVG(buyprice) as 구매평균가 FROM products;

# 집계함수: count(), min(), max(), avg(), sum()

# 와일드카드 문자:
  % : * java에서
  _ : ? java에서

# aliasing(앨리아싱), 별칭만들기
# select 항목 as 별칭
select min(buyprice) as 최저가 from products;


# join 조인
# : 
  두 개의 테이블이 있으면 
  두 개 테이블의 항목들을 합해서 SELECT를 수행하는 것이다.
# join 구문의 2가지 개념
: INNER (두 개 테이블의 공통된 것을 포함하는 방법) -> 교집합 포함
 OUTER (두 개 테이블의 공통된 데이터는 제외하는 방법) -> 교집합제외
   
 
SELECT Orders.OrderID, Customers.CustomerName, Orders.OrderDate
FROM Orders
INNER JOIN Customers ON Orders.CustomerID=Customers.CustomerID; 

예시) 
SELECT * FROM 테이블1 
  JOIN 테이블2 
    ON 테이블1.항목1 = 테이블2.항목2;
    
SELECT * FROM orders;
SELECT * FROM customers;

SELECT t1.orderNumber, t1.orderDate
, t2.customerName, t2.contactLastName, t2.phone
, t2.addressLine1, t2.postalCode, t2.state FROM orders t1 
  inner JOIN customers t2 
    ON t1.customerNumber = t2.customerNumber
     WHERE t1.customerNumber = 128;
     
SELECT * FROM offices  ;
SELECT * FROM employees;

사원 patterson이 근무하고 있는 사무실 정보를 출력해주세요.
출력정보에는 사원번호, lastname, 이메일, jobtitle, 
  사무실코드, 사무실이 있는 도시명, 사무실전화번호를 출력해주세요.

SELECT * FROM employees;
SELECT t1.employeeNumber, t1.lastName, t1.email, t1.jobTitle,  t1.officeCode 
 FROM employees t1;
SELECT t1.employeeNumber, t1.lastName, t1.email, t1.jobTitle ,t1.officeCode 
 FROM employees t1 WHERE  t1.lastName='Patterson';
SELECT * FROM offices; 
SELECT t2.officeCode, t2.city, t2.phone FROM offices t2;

SELECT t1.employeeNumber, t1.lastName, t1.email, t1.jobTitle ,t1.officeCode 
 ,t2.city, t2.phone
 FROM employees t1 JOIN offices t2
 ON t1.officeCode = t2.officeCode
 WHERE  t1.lastName='Patterson';
  	 
	  
SELECT * FROM products;
Q) 상품가가 90보다 큰 상품에 대해 
재고수량이 6000이상 상품이름과 상품제조사 정보를 출력해주세요
SELECT * FROM products WHERE  buyPrice > 90;
SELECT productname, productvender 
 FROM (상품가가 90보다 큰 상품에 대해) 
 WHERE 재고수량> 6000;

SELECT * FROM products WHERE  buyPrice > 90;

SELECT productname, productvendor 
 FROM (SELECT * FROM products WHERE  buyPrice > 90) t1
 WHERE quantityInStock > 6000;
 
SELECT * FROM employees; 


사원 patterson이 근무하고 있는
사무실코드, 사무실이 있는 도시명, 사무실전화번호를 출력해주세요.

SELECT * FROM offices;  
SELECT t1.officeCode, t1.city, t1.phone 
 FROM offices t1
 WHERE t1.officeCode = (patterson의 officecode);

patterson의 officecode: 
SELECT * FROM employees ;   
SELECT t2.employeeNumber FROM employees t2 WHERE t2.lastname='Patterson'; 

SELECT t1.officeCode, t1.city, t1.phone
 FROM offices t1
 WHERE t1.officeCode in
  (SELECT t2.officeCode FROM employees t2 
    WHERE t2.lastname='Patterson');
# join 구문으로 작성했던 코드구성과 같아요.
SELECT t1.officeCode, t1.city, t1.phone,t2.firstname, t2.lastname
 FROM offices t1, (SELECT * FROM employees 
    WHERE lastname='Patterson') t2
 WHERE t1.officeCode= t2.officeCode;

# JOIN 실습

# 주문 정보 , 고객 정보에서 주문id와 고객 이름을 출력해주세요.
SELECT * FROM orders;
SELECT * FROM customers;
SELECT t1.orderNumber, t2.CustomerName
FROM Orders t1
 INNER JOIN Customers t2 
 ON t1.customerNumber = t2.customerNumber
 WHERE t2.CustomerName LIKE '%Mini%';

SELECT * FROM orders;
SELECT * FROM customers;
SELECT * FROM shippers;
# 2개 이상의 정보테이블에서 데이터를 join해서 출력할 수 있음


# union 을 사용해서 두개의 검색 결과를 합칠 수 있어요.
SELECT COUNT(*) FROM(
 SELECT distinct city FROM customers
) t1; --중복된 이름 허용 122, 중복하지 않는 95

SELECT COUNT(*) FROM(
 SELECT  city FROM offices
) t1; --중복된 이름 허용 7, 중복하지 않는 7

# union 은 중복된 이름은 하나만 포함합니다.
SELECT COUNT(*) FROM (
SELECT city FROM customers
 UNION
 SELECT city FROM offices
 ORDER BY city asc
) t1; --97
# union all 은 중복된 이름을 허용합니다.
SELECT COUNT(*) FROM (
SELECT city FROM customers
 UNION ALL 
 SELECT city FROM offices
 ORDER BY city asc
) t1; --129

# group by
# 학년별로 이름,성적을 출력해주세요. 
  -> group by 학년
# 3학년에 대해서 반별로 이름, 성적을 출력해주세요.
  -> GROUP BY 반

예) 고객정보에서 국가이름과 국가별 고객의 수를 
 국가별로 구분해서 출력해주세요.
SELECT COUNT(CustomerNumber), Country
 FROM Customers 
 GROUP BY Country;

SELECT * FROM orders;  
예) 주문정보에서 주문처리상태에 따라(~별로)  
  주문수와 고객수의평균을 출력해주세요.
SELECT status,COUNT(ordernumber), AVG(customerNumber)
 FROM orders
 GROUP BY STATUS;

-- status의 알파벳 오름차순으로 정렬해주세요.
SELECT status,COUNT(ordernumber), AVG(customerNumber)
 FROM orders
 GROUP BY STATUS
 ORDER BY STATUS ASC;
-- 고객수의평균이 큰 것부터 내림차순으로 정렬해주세요.
SELECT STATUS, COUNT(ordernumber) AS 주문수량
 , AVG(customerNumber) AS 평균고객수
 FROM orders
 GROUP BY STATUS
 ORDER BY 평균고객수 desc;


# select 문장을 작성할 때 순서를 참고하세요
SELECT column_name(s)
 FROM TABLE_NAME
 WHERE CONDITION
  GROUP BY column_name(s)
  HAVING CONDITION
 ORDER BY column_name(s);
 
# group by ~ having 출력조건 을 사용합니다

예) 고객정보에서 국가이름과 국가별 고객의 수를 
 국가별로 구분해서 출력해주세요.
 단, 고객수가 5명 이상에 대해 출력해주세요.
SELECT COUNT(CustomerNumber), Country
 FROM Customers
 GROUP BY Country
 HAVING COUNT(CustomerNumber) > 5;

SELECT COUNT(CustomerNumber), Country
 FROM Customers
 GROUP BY Country
 HAVING COUNT(CustomerNumber) > 5
 ORDER BY COUNT(CustomerNumber) DESC;

SELECT * FROM customers;
예) 고객정보에서판매대표인이 1000명이상인 데이터에 대해
 국가이름과 국가별 고객의 수를 
 국가별로 구분해서 출력해주세요.
 단, 고객수가 5명 이상에 대해 출력해주세요.
 출력은 고객수가 많은 순서대로 내림차순으로 정렬해주세요.
SELECT COUNT(CustomerNumber), Country
 FROM Customers
 WHERE salesRepEmployeeNumber >1000
 GROUP BY Country
 HAVING COUNT(CustomerNumber) > 5
 ORDER BY COUNT(CustomerNumber) DESC;
 
# any, all, exist
 - any 하나 이상 해당하면, 
 - ALL 검색된 데이터가 제시한 조건에 모두 다 해당되면
 - exits 조건에 의해 검색된 결과가 있으면
SELECT *   FROM OrderDetails

SELECT * FROM products;
SELECT COUNT(*) FROM (

SELECT ProductName, ProductCode
 FROM Products
 WHERE ProductCode = ANY
  (SELECT ProductCode
  FROM OrderDetails
  WHERE QuantityOrdered > 40)

  ) t1; --109
SELECT COUNT(*) FROM 
 (SELECT ProductCode
  FROM OrderDetails
  WHERE QuantityOrdered > 40 ) t1; --979



SELECT *
  FROM OrderDetails
  WHERE QuantityOrdered = 50
SELECT MIN(QuantityOrdered) FROM OrderDetails;
SELECT * FROM OrderDetails WHERE QuantityOrdered IS null
  
SELECT ProductName, ProductCode
 FROM Products
 WHERE exists
  (SELECT ProductCode
  FROM OrderDetails
  WHERE QuantityOrdered > 40); 
  
SELECT * FROM persons; 
#데이터를 복사해서 다른 테이블에 입력할 수 있어요

create table user_item_list_backup 
 AS (SELECT * FROM user_item_list);

CREATE TABLE persons_copy 
 AS (SELECT * FROM persons);
SELECT * FROM PERSONS_COPY;

SELECT * FROM orders;
CREATE TABLE orders_copy
 AS (SELECT * FROM orders);
SELECT * FROM orders_copy; 

# crate TABLE persons_copy;
CREATE TABLE `persons_copy` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`lastname` VARCHAR(255) NOT NULL,
	`firstname` VARCHAR(255) NULL DEFAULT NULL,
	`age` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='euckr_korean_ci'
ENGINE=MyISAM
;

# alter table persons_copy
# auto_increment, primary key(id) 추가하는 명령어
ALTER TABLE `persons_copy`
	CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST,
	ADD PRIMARY KEY (`id`);

# case 문장을 사용해서 sql query 문장을 조건 실행합니다.
# 문법구조:
CASE
    WHEN condition1 THEN result1
    WHEN condition2 THEN result2
    WHEN conditionN THEN resultN
    ELSE result
END;

# case ~ when 의 예)

SELECT * FROM OrderDetails;
SELECT OrderNumber, QuantityOrdered,
CASE
    WHEN QuantityOrdered > 30 THEN 'The quantity is greater than 30'
    WHEN QuantityOrdered = 30 THEN 'The quantity is 30'
    ELSE 'The quantity is under 30'
END 
 AS QuantityText
FROM OrderDetails;

# case~when 예2)
SELECT * FROM Customers;

SELECT CustomerName, City, Country
FROM Customers
ORDER BY
(CASE
    WHEN City IS NULL THEN Country
    ELSE City
END) desc;	


# ifnull(), coalesce() 를 사용해서 null항목을 치환합니다.
# 항목이 null일 때는 그 항목을 포함한 계산값은 null이 된다.
# 그래서 null이 있는 항목은 치환해주어야합니다.

예)
SELECT ProductName, buyPrice * (UnitsInStock + UnitsOnOrder)
FROM Products;

SELECT ProductName, UnitPrice * (UnitsInStock + IFNULL(UnitsOnOrder, 0))
FROM Products;

SELECT ProductName, UnitPrice * (UnitsInStock + COALESCE(UnitsOnOrder, 0))
FROM Products;


SELECT * FROM products;
CREATE VIEW view_highprice_products AS
 SELECT ProductName, buyPrice
  FROM Products
  WHERE buyPrice > (SELECT AVG(buyPrice) FROM Products);
  --54.395182

SELECT * FROM view_highprice_products;

# view 생성 연습 1)
CREATE VIEW view_patterson_office AS (
SELECT t1.employeeNumber, t1.lastName, t1.email, t1.jobTitle ,t1.officeCode 
 ,t2.city, t2.phone
 FROM employees t1 JOIN offices t2
 ON t1.officeCode = t2.officeCode
 WHERE  t1.lastName='Patterson' );
# # view 생성 연습 1) select from view  	 
SELECT * FROM view_patterson_office WHERE city='Boston';

# view 생성 연습 2)  	 
# join 구문으로 작성했던 코드구성과 같아요.
SELECT t1.officeCode, t1.city, t1.phone,t2.firstname, t2.lastname
 FROM offices t1, (SELECT * FROM employees 
    WHERE lastname='Patterson') t2
 WHERE t1.officeCode= t2.officeCode;

# view 생성 연습 3)
-- status의 알파벳 오름차순으로 정렬해주세요.
CREATE VIEW view_status_orders AS(
SELECT status,COUNT(ordernumber), AVG(customerNumber)
 FROM orders
 GROUP BY STATUS
 ORDER BY STATUS ASC);
 
SELECT * FROM  view_status_orders;

-- view를 삭제하세요...
DROP VIEW view_status_orders;
DROP VIEW view_patterson_office;
DROP VIEW view_highprice_products;

SELECT concat('abc', '123') AS 결합문자열; --abc123
SELECT * FROM customers;
SELECT CONCAT(첫번째검색결과,두번째검색결과) AS 결합문자열; --abc123
# 이름이 peter인 고객의 이름과 전화번호를 결합해서 
# 고객_전화번호 로 항목을 생성해주세요.
SELECT * FROM customers where contactfirstname='Peter'
첫번째검색결과:
 SELECT customerName FROM customers WHERE contactfirstname='Peter'
두번째검색결과:
 고객의 폰번호
  SELECT phone FROM customers WHERE contactfirstname='Peter'
SELECT 
 CONCAT(
  (SELECT customerName FROM customers 
    WHERE contactfirstname='Peter' AND customerNumber='114')
  ,(SELECT phone FROM customers 
    WHERE contactfirstname='Peter' AND customerNumber='114')) 
  AS 결합문자열; 
-- Australian Collectors, Co.03 9520 4555

# 문자열 합칠 때, 
# 참고: oracle에서는 '문자열1'||'문자열2' 으로 자주 사용합니다.

# format 을 사용하는 예)
SELECT FORMAT(250500.5634, 2);
-- 250,500.56
# 콤마(,) 를 사용해서 출력합니다. 
# 숫자는 소수점 둘째자리까지 출력합니다.
SELECT FORMAT(250500.5664, 2);
-- 250,500.57

# 소문자로 문자열을 변환합니다.
SELECT LOWER("abc Abc aBC 123");--'abc abc abc 123'
# 대문로 문자열을 변환합니다.
SELECT UCASE("abc Abc aBC 123");--'ABC ABC ABC 123'
SELECT UPPER("abc Abc aBC 123");--'ABC ABC ABC 123'

SELECT * FROM employees;


SELECT * FROM testdb.persons;
SELECT CustomerName, City, Country
# 복합예제:
# testdb.persons의 정보에서 
 firstname, lastname을 결합해서 NAME으로 출력하고
 나이는 25이상이면 A, 20이상이면 B, 20미만이면 C로 구분해서 출력하는
 뷰를 생성하세요.
 그리고 뷰로부터 이름이 bok이 포함된 사람의 
  이름과 나이클래스를 출력하세요.
  단, bok은 대소문자 구분하지 않습니다.
CREATE VIEW view_ageclass_persons AS(
SELECT CONCAT(lastname, firstname) AS NAME,
	case
	 	when age > 25 then 'A'
	 	when age > 20 then 'B'
	 	ELSE 'C'
	end
	AS age_class
FROM testdb.persons); 	

SELECT * FROM view_ageclass_persons 
 where lower(NAME) LIKE '%bok%';



FROM Customers
ORDER BY
(CASE
    WHEN City IS NULL THEN Country
    ELSE City
END) desc;

#############################################
# 문자열 처리하는 함수 기능
#--------------------------------
# LPAD, TRIM
# LPAD, RPAD: 제시문자열의 왼쪽(L), 오른쪽(R)위치에 ...
# 주어진값으로 제시된자리수가 되도록 채워넣기를 합니다.
SELECT LPAD("SQL Tutorial", 20, "ABC");
SELECT LPAD("123", 10, '0');
SELECT RPAD("123", 10, '0');
SELECT LPAD("ABC", 10, '^');
SELECT RPAD("ABC", 10, '^');
SELECT LPAD("ABC", 10, ' ');--'       ABC'
SELECT RPAD("ABC", 10, ' ');--'ABC       '
select TRIM('       ABC');--'ABC'
select LTRIM('       ABC');--'ABC'
select LTRIM('ABC       ');--'ABC       '
select RTRIM('ABC       ');--'ABC'
select length('ABC       ');--10

# MID() 제시문자열에서 시작인덱스로부터 제시된 문자수만큼 문자열을 출력
SELECT MID("abcde 2345", 5, 3) AS ExtractString;--'e 2'
SELECT MID("abcde 2345", -5, 3) AS ExtractString;--' 23'
SELECT SUBSTR("abcde 2345", 5, 3) AS ExtractString;--'e 2'
SELECT SUBSTRING("abcde 2345", 5, 3) AS ExtractString;
SELECT SUBSTRING_INDEX("www.w3schools.com", ".", 1);--'www'
SELECT SUBSTRING_INDEX("www.w3schools.com", ".", 2);--'www.w3schools'

# POSITION() 은 문자열에서 제시된 문자가 처음 발견된 인덱스번호를 출력
SELECT POSITION("3" IN "abcdabcd1234 1234") AS MatchPosition;
SELECT POSITION("1234" IN "abcdabcd1234 1234") AS MatchPosition;
SELECT POSITION(" 12" IN "abcdabcd1234 1234") AS MatchPosition;

# repeat()
SELECT REPEAT("abc-", 3);

# replace()
SELECT REPLACE("hello abc 123 abc456", "abc", "hi~");

# big endian, little endian - linux, window os
SELECT REVERSE("SQL Tutorial");
SELECT RIGHT("abc 123456", 4) AS ExtractString;
SELECT LEFT("abc 123456", 4) AS ExtractString;
SELECT SPACE(10);--'          '
SELECT STRCMP("abc 1234", "abc 1234"); --1000 - 1000 = 0
SELECT STRCMP("abc 1234", "abc 1234  ");--0
SELECT STRCMP("abc 1234  ", "abc 1234");--0

# length() 등의 함수는 table, view로부터 ...
#  선택된 항목의 문자열 길이를 측정
SELECT length(phone) FROM customers  WHERE customernumber='112';
CREATE VIEW view_phone AS
 SELECT phone FROM customers where customernumber='112';
SELECT LENGTH(phone) FROM view_phone;
# subquery 결과로부터의 length() 측정은 사용되지 않음!
SELECT length(SELECT phone FROM customers where customernumber='112') 

#############################################
# 숫자 처리하는 함수 기능
#--------------------------------
# ABS : 절대값을 출력합니다.
SELECT ABS(-123);--'123'
SELECT ABS(123);--'123'
# AVG() 평균구하기
SELECT AVG(buyPrice) AS AveragePrice FROM Products;
# 반올림, 내림, 올림 계산하기
SELECT CEIL(25.25);--26
SELECT CEILING(25.75);
SELECT FLOOR(25.75);--25
SELECT ROUND(135.375, 2);--'135.38'
SELECT ROUND(135.374, 2);--'135.37'
SELECT TRUNCATE(135.375, 2); --135.37

#나머지를 구하는 MOD
SELECT MOD(18, 4);
SELECT 18 MOD 4;
SELECT 18 % 4;
# 몫을 구하는 나눗셈
SELECT 10 DIV 5; --2
SELECT 10 DIV 5;

#  가장 큰값, 가장 작은값 구하는 함수
SELECT GREATEST(3, 12, 34, 8, 25);--34
SELECT LEAST(3, 12, 34, 8, 25);--3

# 랜덤 숫자 출력하기 
SELECT RAND();--0.89170460020351
SELECT RAND(123); --0.92774286114401

SELECT POW(4, 2);
SELECT POW(2, 5);--32
SELECT SQRT(64);--8

SELECT FLOOR(RAND()*(10-5+1)+5);

SELECT SIN(2);

SELECT ACOS(-0.8);
 -- 1 / cos() 코사인함수
SELECT EXP(2);
e^2
2.7178..
SELECT LOG(2);
SELECT LOG10(2); -> 10^2 = 100

#######################################
# 날자 계산하는 함수
SELECT LAST_DAY("2021-10-13");
SELECT DATEDIFF(NOW(), "2021-10-01");
SELECT DAYOFWEEK("2021-10-12");--화: 3
SELECT DAYOFWEEK("2021-10-09");--토: 7
SELECT DAYOFWEEK("2021-10-10");--일: 1

SELECT DAYOFYEAR("2021-10-01");--274
SELECT DAYOFYEAR("2021-12-31");--365

SELECT SEC_TO_TIME(-6897);
SELECT STR_TO_DATE("August 10 2021", "%M %d %Y");
SELECT TIME_FORMAT("19:30:10", "%H %i %s");


# 조건문장으로 로직(logic: 논리) 구성하기 

SELECT IF(500<1000, "YES", "NO");

SELECT * FROM employees;