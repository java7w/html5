SELECT host, user, PASSWORD, super_priv FROM USER;

CREATE DATABASE testdb;
USE testdb;

-- DROP DATABASE testdb;

CREATE TABLE persons(
  personid INT,
  lastname VARCHAR(255),
  firstname VARCHAR(255),
  address VARCHAR(255),
  city VARCHAR(255)
);
DESC persons;

INSERT INTO 
persons(personid, lastname, firstname, address, city)
VALUES('00001', 'hong', 'gildong', '555-1', 'seoul');

SELECT * FROM persons;

backup DATABASE testdb
TO DISK = 'c:\works2\SQL\testdb.backup';


--mysqldump 를 사용한 데이터베이스 백업하기
mysqldump  -u root -p testdb > testdb_backup_20211012.sql

DROP TABLE persons;



testdbtestdbtestdb

SELECT * FROM departments;


SHOW TABLES;

CREATE TABLE persons(
  id INT NOT NULL, 
  lastname VARCHAR(255) NOT NULL, 
  firstname VARCHAR(255), 
  age INT,
  CONSTRAINT pk_person PRIMARY KEY(id, lastname)
);

DESC persons;

INSERT INTO persons VALUES('00001', 'hong', 'gildong', 20);
SELECT * FROM persons;

--FK
CREATE TABLE orders(
  orderid INT NOT NULL AUTO_INCREMENT,
  ordernumber INT NOT NULL,
  personid INT, 
  CONSTRAINT pk_orders PRIMARY KEY(orderid),
  CONSTRAINT pk_orders_persons FOREIGN KEY(personid)
    REFERENCES persons(personid)
);


create TABLE persons2(
  id INT NOT NULL, 
  lastname VARCHAR(255) NOT NULL,
  firstname VARCHAR(255),
  age INT, 
  CHECK(age>=18)
);

-- DROP TABLE persons2;
INSERT INTO persons2(id, lastname, firstname, age)
 VALUES ('00001', 'hong', 'gildong', 21);
INSERT INTO persons2(id, lastname, firstname, age)
 VALUES ('00001', 'hong', 'gildong', 17);
SELECT * FROM persons2;


CREATE TABLE persons3 (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    City varchar(255),
    CONSTRAINT CHK_Person CHECK (Age>=18 AND City='Sandnes')
);

INSERT INTO persons3(id, lastname, firstname, age, city)
 VALUES ('00002', 'hong', 'gildong', 21, 'seoul');

SELECT * FROM persons3;


DESC orders;
SELECT * FROM orders;
SELECT * FROM persons;
INSERT INTO orders(ordernumber,personid)
VALUES('00001', '00001');


SELECT  
count(distinct first_name) AS dist_name_count
, count(first_name) AS name_count 
FROM employees;

SELECT * FROM employees;
SELECT * FROM employees WHERE hire_date > '1995-01-01';
SELECT COUNT(*) FROM employees 
 WHERE hire_date > '1995-01-01' AND birth_date < '1960-01-01';




