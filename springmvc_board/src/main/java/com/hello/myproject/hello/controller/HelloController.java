package com.hello.myproject.hello.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hello.myproject.hello.service.HelloService;
import com.hello.myproject.hello.service.IHelloService;

public class HelloController {
	private static final Logger log = LoggerFactory.getLogger(HelloController.class);
	
	IHelloService hello = new HelloService();
	
	public void hello(String name) {
		log.info("HelloController: " + hello.sayHello(name));
	}
	
	
}
