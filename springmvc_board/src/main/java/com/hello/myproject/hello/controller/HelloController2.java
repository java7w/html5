package com.hello.myproject.hello.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hello.myproject.hello.service.HelloService;
import com.hello.myproject.hello.service.IHelloService;

public class HelloController2 {
	private static final Logger log = LoggerFactory.getLogger(HelloController2.class);
	
	public HelloController2(IHelloService hello) {
		super();
		this.hello = hello;
	}

	IHelloService hello;
	
	public void hello(String name) {
		log.info("HelloController: " + hello.sayHello(name));
	}
}
