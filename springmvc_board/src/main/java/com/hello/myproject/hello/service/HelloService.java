package com.hello.myproject.hello.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloService implements IHelloService {
	private static final Logger log = LoggerFactory.getLogger(HelloService.class);
	
	@Override
	public String sayHello(String name) {
		log.info("sayHello() ����");
		String msg = "Hello! " + name;
		return msg;
	}
}
