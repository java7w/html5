package com.hello.myproject.hello.service;

public interface IHelloService {
	String sayHello(String name);
}
