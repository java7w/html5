package com.hello.myproject.hello;

import com.hello.myproject.hello.controller.HelloController;
import com.hello.myproject.hello.controller.HelloController2;
import com.hello.myproject.hello.controller.HelloController3;
import com.hello.myproject.hello.service.HelloService;
import com.hello.myproject.hello.service.IHelloService;

public class HelloMain {

	public static void main(String[] args) {
		//nonDi();
		//useDiConstructor();
		useDiSetter();
	}

	private static void useDiSetter() {
		IHelloService serv = new HelloService();
		HelloController3 cont = new HelloController3();
		cont.setHello(serv);
		cont.hello("ȫ�泲");
	}

	private static void useDiConstructor() {
		IHelloService serv = new HelloService();
		HelloController2 cont = new HelloController2(serv);
		cont.hello("ȫ�漭!");
	}

	private static void nonDi() {
		IHelloService serv = new HelloService();
		HelloController cont = new HelloController();
		cont.hello("ȫ�浿!");
	}

}
