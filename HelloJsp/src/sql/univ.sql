create database univ;
use univ;
DROP TABLE student;
create table student(
	hakbun int not NULL COMMENT '학번, pkey', 
	name varchar(5) COMMENT '이름',
	year TINYINT COMMENT '학년',
	dept varchar(10) COMMENT '학과',
	addr varchar(50) COMMENT '주소',
	constraint pk_student PRIMARY KEY ( hakbun )
) COMMENT '대학생 관리 테이블';

DESC univ.student;

INSERT INTO student VALUES
	('201001', '홍길동', '4', 'java', '서울시'),
	('202002', '홍길서', '3', 'java', '서울시'),
	('202003', '홍길남', '3', 'java', '서울시'),
	('203004', '홍길북', '2', 'java', '서울시');
	
SELECT * FROM univ.student;