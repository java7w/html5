
create database univ;
use univ;

DROP TABLE univ.student;
create table student(
	hakbun int not null, 
	name varchar(5),
	year tinyint,
	dept varchar(10),
	addr varchar(50),
	constraint pk_student PRIMARY KEY ( hakbun )
);
DELETE from TABLE univ.student;
SELECT * FROM univ.student;
insert into student values
(160001, '한국인', 4, '컴공', '서울'),
(195712, '조아라', 1, '멀티', '부산'),
(179752, '홍길동', 3, '전상', '광주'),
(184682, '나매력', 2, '전상', '제주'),
(172634, '이천사', 3, '컴공', '광주'),
(183517, '김보배', 2, '멀티', '전남'),
(160739, '신입생', 4, '컴공', '광주');

SELECT * FROM univ.student;

insert into student values
(160001, 'hong1', 4, 'comp', 'seoul'),
(195712, 'hong2', 1, 'multi', 'busan'),
(179752, 'hong3', 3, 'tech', 'kwangju'),
(184682, 'na1', 2, 'tech', 'jeju'),
(172634, 'na2', 3, 'comp', 'kwangju'),
(183517, 'me1', 2, 'multi', 'junnam'),
(160739, 'me2', 4, 'comp', 'kwangju');

select * from student order by hakbun ASC;