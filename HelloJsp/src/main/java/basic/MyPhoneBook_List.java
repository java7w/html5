package basic;

import java.util.ArrayList;
import java.util.List;

public class MyPhoneBook_List {
	List<PBook> pb = new ArrayList<PBook>();
	
	MyPhoneBook_List(){
		
	}

	@Override
	public String toString() {
		return "MyPhoneBook_List [pb=" + pb + "]";
	}
	
	// 데이터추가
	public void addPerson(PBook p1) {
		pb.add(p1);
	}
	// 데이터변경
	public void updatePerson(int index, PBook p1) {
		pb.set(index, p1);
	}
	// 데이터삭제
	public void deletePerson(int index) {
		pb.remove(index);
	}
	// 데이터전체삭제
	public void clearPerson() {
		pb.clear();
	}
	// 데이터검색
	public void showPerson(int index) {
		PBook person = pb.get(index);
		
		System.out.println("\t" + index 
				  + " : [" + person.name +"," +person.phone
				         + "," +person.email+","+ person.addr +"]");	
	}
	// 데이터전체출력
	public void showData() {
		System.out.println("* 전체 출력: ");
		for(int i=0; i<pb.size(); i++) {
			PBook person = pb.get(i);
			System.out.println("\t" + i 
					  + " : [" + person.name +"," +person.phone
					         + "," +person.email+","+ person.addr +"]");
		}
	}
	
	
}
