package basic;

public class MyFamillyName {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Child child1 = new Child(21, 0);
		System.out.println("나의 속성:"+ child1.childProperty);
		String myFullName = child1.familyName +" "+ child1.myName; 
		System.out.println("나의 이름은: " + myFullName);
		System.out.println("나의 나이는: "+child1.age);		
	}

}
class Person{
	int age;
	int nation;
	int height;
	int weight;
	int gender;
	int mbti; 
	String name;
}
class GrandPa extends Person{
	private String myName = "Oldest Granpa";
	String familyName = "King";	
}
class Pa extends GrandPa {
	String myName = "PaPa";
	String myPaPa = "rich man";
	
	
}

class Child extends Pa{
	String myName = "honggil";
	String childProperty = "play all day";
	public Child(String myName, String childProperty) {
		super();	
		this.myName = myName;
		this.childProperty = childProperty;
	}
	public Child(int age, int gender) {
		super.age = age;
		super.gender = gender;
	}
	
}
