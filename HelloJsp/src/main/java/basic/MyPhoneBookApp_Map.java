package basic;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MyPhoneBookApp_Map {

	public static void main(String[] args) {
		// Map을 사용해서 전화번호부(폰북: PhoneBook)를 만들어봅니다.

		pbInit();
		MyPhoneBook_Map map = new MyPhoneBook_Map();

		while (true) {
			int sel;
			int id;
			int resCode = 0;
			PBook person = null;
			sel = pbMenu();
			switch (sel) {
			case 0:// 종료
				System.out.println("프로그램을 종료합니다.");
				resCode = programExit();
				break;
			case 1: // 주소록 추가하기
				person = pbAddPerson();
				map.addData(person);
				if (resCode == 999) {
					System.out.println("주소록 추가에서 오류가 발생했습니다.");
				}

				break;

			case 2: // 주소록 전체 조회하기
				resCode = pbShowData();
				map.showData();
				if (resCode == 998) {
					System.out.println("주소록 전체출력에서 오류가 발생했습니다.");
				}
				break;
			case 3: // 주소록 검색하기
				id = pbSearchPerson();
				map.showPersonData(id);
				if (resCode == 997) {
					System.out.println("주소록 검색에서 오류가 발생했습니다.");
				}
				break;
			case 4: // 주소록 변경하기
				resCode = pbUpdatePerson();
				id = pbSearchPerson();
				person = pbGetPerson();
				map.updatePersonData(id, person);
				if (resCode == 996) {
					System.out.println("주소록 변경에서 오류가 발생했습니다.");
				}
				break;
			case 5: // 주소록에서 삭제하기
				resCode = pbDeletePerson();
				id = pbRemovePerson();
				map.removePersonData(id);
				if (resCode == 995) {
					System.out.println("주소록 삭제할 때 오류가 발생했습니다.");
				}
				break;
			case 6: // 주소록을 초기화(전체삭제)하기
				resCode = pbDeleteAllData();
				int ok = confirmClearData();
				if(ok==1) {
					map.removeData();
				}
				if (resCode == 994) {
					System.out.println("주소록 전체삭제할 때 오류가 발생했습니다.");
				}
				break;

			default:
				System.out.println("입력메뉴를 다시 선택하세요");
				break;
			}
		}
	}

	private static int confirmClearData() {
		String ok = null;
		int nok=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("정말 전체 삭제하겠습니까? (yes/or 입력하세요)");
		System.out.print("입력 > ");
		ok = sc.next();
		if(ok.toLowerCase().equals("yes")||ok.toLowerCase().equals("y")) {
			System.out.println("전체 삭제를 진행합니다.");
			try {
				for (int i = 0; i < 20; i++) {
					TimeUnit.MILLISECONDS.sleep(100);
					System.out.print(".");
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			nok = 1;
		}else {
			System.out.println("전체 삭제를 취소합니다.");
			nok = 0;
		}
		return nok;
	}

	private static int programExit() {
		try {
			for (int i = 0; i < 20; i++) {
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print(".");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.exit(0);
		return 0;
	}

	private static int pbShowData() {
		System.out.println("주소록 전체 출력!!");
		return 0;
	}

	private static int pbDeleteAllData() {
		System.out.println("주소록 전체 삭제!!");
		return 0;
	}

	private static int pbDeletePerson() {
		System.out.println("주소록 선택 삭제!!");
		return 0;
	}
	private static int pbUpdatePerson() {
		System.out.println("주소록 변경!!");
		return 0;
	}
	private static PBook pbGetPerson() {
		String name, phone, email, addr;
		Scanner sc = new Scanner(System.in);
		System.out.print("이름> ");
		name = sc.next();
		System.out.print("전화번호> ");
		phone = sc.next();
		System.out.print("이메일> ");
		email = sc.next();
		System.out.print("주소> ");
		addr = sc.next();

		PBook p1 = new PBook(name, phone, email, addr);

		return p1;
	}

	private static int pbSearchPerson() {
		int nid = 0;
		System.out.println("주소록 검색!!");
		Scanner sc = new Scanner(System.in);
		System.out.println("검색하려는 주소록의 id를 입력하세요");
		while (true) {
			System.out.print("id선택> ");
			String id = sc.next();
			
			if (id == "" || id.equals(null)) {
				continue;
			} else {
				nid = Integer.valueOf(id);
				break;
			}
		}
		return nid;
	}
	private static int pbRemovePerson() {
		int nid = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("삭제하려는 주소록의 id를 입력하세요");
		while (true) {
			System.out.print("id선택> ");
			String id = sc.next();
			
			if (id == "" || id.equals(null)) {
				continue;
			} else {
				nid = Integer.valueOf(id);
				break;
			}
		}
		return nid;
	}
	private static PBook pbAddPerson() {
		System.out.println("주소록 추가!!");
		String name, phone, email, addr;
		Scanner sc = new Scanner(System.in);
		System.out.print("이름> ");
		name = sc.next();
		System.out.print("전화번호> ");
		phone = sc.next();
		System.out.print("이메일> ");
		email = sc.next();
		System.out.print("주소> ");
		addr = sc.next();

		PBook p1 = new PBook(name, phone, email, addr);

		return p1;
	}

	private static int pbMenu() {
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("메뉴!!");
		System.out.println("-----------------------------------------------");
		System.out.println("메뉴: 종료0,추가1,전체출력2,검색3,변경4,삭제5,전체삭제6");
		System.out.println("-----------------------------------------------");
		System.out.print("선택> ");
		int sel = sc.nextInt();
		return sel;
	}

	private static void pbInit() {
		System.out.println("PhoneBook v1.0을 시작합니다.");
		System.out.println("PhoneBook 데이터를  초기화합니다. ");
		try {
			for (int i = 0; i < 20; i++) {
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print(".");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
