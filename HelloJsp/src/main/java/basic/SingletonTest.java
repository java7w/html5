package basic;

final class Singleton {
	private static Singleton s = new Singleton();
	
	static int count;
	private Singleton() {
		//...
		System.out.println("클래스 instance 생성 !!!");
		this.count++;
	}

	public static Singleton getInstance() {
		if(s==null) {
			s = new Singleton();
		}
		return s;
	}	
	
	public int getCount() {
		return this.count;
	}

	//...
}

class SingletonTest {
	public static void main(String args[]) {
//		Singleton s = new Singleton();
		Singleton s = Singleton.getInstance();
		System.out.println("s: 클래스 생성 횟수: " + s.getCount());
		
		Singleton s2 = Singleton.getInstance();
		System.out.println("s2: 클래스 생성 횟수: " + s2.getCount());
	}
}
