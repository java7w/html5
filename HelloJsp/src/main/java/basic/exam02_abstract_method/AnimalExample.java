package basic.exam02_abstract_method;

public class AnimalExample {
	public static void main(String[] args) {
		Dog dog = new Dog();
		Cat cat = new Cat();
		dog.sound();//�۸�
		cat.sound();//�Ŀ�
		System.out.println("-----");
		
		//������ �ڵ� Ÿ�� ��ȯ
		Animal animal = null;
		animal = new Dog();
		animal.sound();//�۸�
		animal = new Cat();
		animal.sound();//�Ŀ�
		System.out.println("-----");
		
		//�Ű������� �ڵ� Ÿ�� ��ȯ
		animalSound(new Dog());
		animalSound(new Cat());
		
		animalSound(new Cow());
		animalSound(new Chick());
		
	}
	
	public static void animalSound(Animal animal) {
		animal.sound();
	}
}
