package basic;

class InstanceofTest {
	public static void main(String args[]) {
		FireEngine fe = new FireEngine();

		if(fe instanceof FireEngine) {//true
			System.out.println("인스턴스변수 fe는 "
					+ "FireEngine의 클래스 설계도대로 만든 instance입니다.");
		} 

		if(fe instanceof Car) { //true
			System.out.println("인스턴스변수 fe는 "
					+ "Car클래스의 설계도대로 만든 instance입니다.");
		} 

		if(fe instanceof Object) { //true
			System.out.println("인스턴스변수 fe는 "
					+ "Object클래스의 설계도대로 만든 instance입니다.");
		} 

		System.out.println(fe.getClass().getName()); // 클래스의 이름을 출력
	}
} // class
//class Car {}
//class FireEngine extends Car {}
