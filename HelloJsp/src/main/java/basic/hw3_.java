package basic;

import java.io.IOException;
import java.util.Scanner;

public class hw3_ {

	private static final String PROMPT = "> ";

	public static void main(String[] args) {
		Scanner sc1 = new Scanner(System.in);
		int y = 1900;
		int m = 1;
		int d = 1;
		String date;
		String [] date2 = new String[3];
		boolean isYoon = false;

		System.out.println(PROMPT);
		//2021-01-01
		
		int sel = 0;
		do {
			printMenu();
			System.out.println("메뉴를 선택하세요");
			System.out.print("> ");
			sel = sc1.nextInt();
			switch (sel) {
			case 1:
				System.out.println(" 1 검색날자 달력보기");
				date = inputDate();
				date2 =date.split("-");
				y=Integer.valueOf(date2[0]);
				m=Integer.valueOf(date2[1]);
				d=Integer.valueOf(date2[2]);
				if(m==1) {
					printMonthSub(y-1,12);
					printMonth(y,m);
					printMonthSub(y,m+1);
				}else if(m==12) {
					printMonthSub(y,m-1);
					printMonth(y,m);
					printMonthSub(y+1,1);
				}else {
					printMonthSub(y,m-1);
					printMonth(y,m);
					printMonthSub(y,m+1);
				}
				break;
			case 2:
				System.out.println(" 2 검색년도 달력보기 ");
				y = inputYear();
				for(int i=0;i<12;i++) {
					printMonthSub(y,i+1);
				}
				break;
			case 9:
				System.out.println(" 9 종료하기 ");
				System.exit(0);
				break;
			default:
				break;
			}
		} while (sel != 9);

		isYoon = isYoonYear(y);
		printMonth(y, m);
	}

	private static int inputYear() {
		Scanner sc1 = new Scanner(System.in);
		System.out.println("보기 원하는 달력의 년도를 입력해주세요");
		System.out.print("년> ");
		
		int y = sc1.nextInt();
		return y;
	}

	private static void printMonthSub(int y, int m) {
		int[] month = new int[12];
		int[] month1 = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };// 365
		int[] month2 = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };// 366
		int[] weeks = new int[7];
		char[] title = {'일','월','화','수','목','금','토'};
		String [][] months = new String[6][7];
		boolean isYoon = isYoonYear(y);
		int []w = getWeekYear(y);
		System.out.printf("[%d년도 %d월 달력]\n", y, m);
		for(int i=0;i<title.length;i++) {
			System.out.printf("%3c  ", title[i]);
		}
		System.out.println();

		for (int j = 0; j < w[m-1]; j++) {
			System.out.printf("%3c", ' ');
			months[0][j]="";
		}

		if (isYoon) {
			System.arraycopy(month2, 0, month, 0, month2.length);
		} else {
			System.arraycopy(month1, 0, month, 0, month1.length);
		}
		for (int i = 0; i < month[m - 1]; i++) {

			if ((i + w[m-1]) % 7 == 0) {// 0 1 2 3 4 5 6 7 
				System.out.println();
			}
			System.out.printf("%3d", i + 1);
		}
		System.out.println();		
	}

	private static String inputDate() {
		Scanner sc1 = new Scanner(System.in);
		
		int y = 1900;
		int m = 1;
		int d = 1;
		
		System.out.println("보기 원하는 달력의 날자를 입력해주세요");
		System.out.print("년> ");
		y = sc1.nextInt();
		System.out.print("월> ");
		m = sc1.nextInt();
		System.out.print("일> ");
		d = sc1.nextInt();
		String date = y+"-"+m+"-"+d;
		System.out.printf("입력날자는 %d년 %d월 %d일 입니다 \n", y, m, d);
		return date;
	}

	private static void printMenu() {
		System.out.println("==================================");
		System.out.println("달력 출력 프로그램 v1.0");
		System.out.println("----------------------------------");
		System.out.println(" 1 검색날자 달력보기");
		System.out.println(" 2 검색년도 달력보기 ");
		System.out.println(" 9 종료하기 ");
		System.out.println("----------------------------------");
	}

	private static void printTitle() {
		System.out.println("==================================");
		System.out.println("달력 출력 프로그램 v1.0");
//		System.out.println("----------------------------------");
	}

	private static boolean isYoonYear(int y) {
		boolean isYoon = false;
		if (y % 400 == 0 || y % 4 == 0 && y % 100 != 0)
			isYoon = true;
//		if (isYoon) {
//			System.out.println("윤년입니다.");
//		} else {
//			System.out.println("평년입니다.");
//		}
		return isYoon;
	}

	private static void printMonth(int y, int m) {
		int[] month = new int[12];
		int[] month1 = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };// 365
		int[] month2 = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };// 366
		int[] weeks = new int[7];
		String [][] months = new String[6][7];
		boolean isYoon = isYoonYear(y);
		int []w = getWeekYear(y);
		System.out.printf("[%d년도 %d월 달력]\n", y, m);
		System.out.printf("일\t월\t화\t수\t목\t금\t토\n");
		// 월요일부터~~
//		for(int i:w) {
//			System.out.printf("%3d", i);
//		}
//		System.out.println();
		//1900: 1  4  4  0  2  5  0  3  6  1  4  6
		for (int j = 0; j < w[m-1]; j++) {
			System.out.printf("\t");
			//months[0][j]="";
		}

		if (isYoon) {
			System.arraycopy(month2, 0, month, 0, month2.length);
		} else {
			System.arraycopy(month1, 0, month, 0, month1.length);
		}
		for (int i = 0; i < month[m - 1]; i++) {

			if ((i + w[m-1]) % 7 == 0) {// 0 1 2 3 4 5 6 7 
				System.out.println();
			}
			System.out.printf("%d\t", i + 1);
		}
		System.out.println();
	}

	private static int[] getWeekYear(int y) {
		int[] month = new int[12];
		int[] month1 = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };// 365
		int[] month2 = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };// 366
		// 시작하는 요일 구하기
		int year=1900;
		int[] w = new int[13];
		//w[]={1, 4, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6, 2}
		boolean isYoon = isYoonYear(year);
		int wparam = 365;
		
		if(y>=year) {
			for (int i=year;i<y+1;i++) {
				isYoon = isYoonYear(i);
				if (isYoon) {
					System.arraycopy(month2, 0, month, 0, month2.length);
					wparam = 366;
				} else {
					System.arraycopy(month1, 0, month, 0, month1.length);
					wparam = 365;
				}
				if(i==year) {
					w[0]=1;
				}else {
					//w[0]=w[13]%7;
					w[0]=(w[0]+wparam)%7;
				}
				
				for(int j=0;j<12;j++) {
					w[j+1]=w[j]+month[j]%7;
					w[j+1]%=7;
				}
			}
		} else {
			for (int i=year;i>=y;i--) {
				isYoon = isYoonYear(i);
				if (isYoon) {
					System.arraycopy(month2, 0, month, 0, month2.length);
				} else {
					System.arraycopy(month1, 0, month, 0, month1.length);
				}
				if(i==year) {
					w[0]=1;
				}else {
					w[0]=(w[0]-wparam)%7;
				}
				for(int j=0;j<12;j++) {
					w[j+1]=w[j]+month[j]%7;
					w[j+1]%=7;
				}
			}
		}	
		
		int[] w2 = new int[12];
		System.arraycopy(w, 0, w2, 0, w2.length);
//		for(int i:w2) {
//			System.out.printf("%3d", i);
//		}
//		System.out.println();
		return w2;
	}
}
