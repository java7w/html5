package basic;

public class MethodTest {

	public static void main(String[] args) {
		// 인스턴스메소드, 클래스메소드의 차이점을 실습합니다.
		MethodTest_MyMath2 my1 = new MethodTest_MyMath2();
		my1.a = 100;
		my1.b = 200;
		System.out.println("my1: " + my1.add());

//- 주소공간을 할당해놓지 않아서 변수의 값을 저장할 수 없습니다.
//		MyMath2 my2 = null;
//		my2.a = 200;
//		my2.b = 300;
//		System.out.println("my2: " + my2.add());
		
		System.out.println("my3: " + MethodTest_MyMath2.add2(100, 400));
		System.out.println("static c: " + MethodTest_MyMath2.c);
	}

}

class MethodTest_MyMath2{
	long a, b;
	static long c = 1000;
	long add() {
		return a+b;
	}
	long add(long a, long b) {
		return a+b;
	}
	
	static long add2(long a, long b) {
		return a+b;
	}
}
