package basic;

import java.util.Objects;

class Data1 {
	int value;
}

class Data2 {
	int value;
	Data2(){
		System.out.println("default 생성자: Data()");
	}
	Data2(int x) { 	// 매개변수가 있는 생성자.
		value = x;
	}
}

class Data3 {
	int value;
	int p1;
	int p2;
	String p3;
	Data3(){
		System.out.println("default 생성자: Data()");
	}
	Data3(int value) { 	// 매개변수가 있는 생성자.
		this(value, 100, 200, "this() 생성자 호출");
		System.out.println("default 생성자: Data(int value)");
		this.value = value;
		
	}
	
	Data3(int value, int p1, int p2, String p3) {
		super();
		this.value = value;
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	
	Data3(Data3 d3){
		this(d3.value, d3.p1, d3.p2, d3.p3);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(p1, p2, p3, value);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Data3 other = (Data3) obj;
		return p1 == other.p1 && p2 == other.p2 && Objects.equals(p3, other.p3) && value == other.value;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public int getP1() {
		return p1;
	}
	public void setP1(int p1) {
		this.p1 = p1;
	}
	public int getP2() {
		return p2;
	}
	public void setP2(int p2) {
		this.p2 = p2;
	}
	public String getP3() {
		return p3;
	}
	public void setP3(String p3) {
		this.p3 = p3;
	}
	@Override
	public String toString() {
		return "Data3 [value=" + value + ", p1=" + p1 + ", p2=" + p2 + ", p3=" + p3 + "]";
	}
	
}
class ConstructorTest {
	public static void main(String[] args) {
		System.out.println("1. main 프로그램 시작!!");
		System.out.println("2. new Data1() 인스턴스 생성 시작!!");
		Data1 d1 = new Data1();
		System.out.println("3. new Data2() 인스턴스 생성 시작!!");
		Data2 d2 = new Data2();		// compile error발생
		
		System.out.println("4. d2.value: "+d2.value);
		
		Data3 d3 = new Data3(100, 200, 300, "hello Data3!");
		System.out.println("d3: "+d3.toString());
		System.out.println("d3.hash: "+d3.hashCode());
		
		Data3 d4 = new Data3(d3);
		System.out.println("d4: "+d3.toString());
		System.out.println("d4.hash: "+d3.hashCode());
	}
}
