package basic;

import java.util.Scanner;

/*
달력을 출력하는 프로그램을 작성하세요.
달력은 검색일 기준으로 이전 달, 현재 달, 다음 달을 출력하세요.

조건) 
1900년 1월 1일은 월요일입니다.
윤년을 적용하세요.
윤년은 4년으로 나눠지는 해이고 단 100년으로 나눠지면 윤년이 아닙니다. 
그중에서 400으로 나눠지면 윤년입니다.
윤년은 2월 29일까지 입니다.
*/
public class MyCalendar {

	static int[] month_yoon = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	static int[] month_nyoon = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	public static void main(String[] args) {
		// 달력만들기
		// 검색 날자에 해당하는 달력을 출력합니다.

		// 1900년 1월 1일 해당 달력을 출력하기
		//initCalendar();
		myMenuCalendar();

	}

	private static void myMenuCalendar() {
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("출력하고 싶은 년월일을 입력하세요");
			System.out.print("년도> ");
			int y = sc.nextInt();
			System.out.print("월> ");
			int m = sc.nextInt();
			System.out.print("일> ");
			int d = sc.nextInt();

			thisMonth(y, m, d);
			

			System.out.println("계속 하려면 1을, 중지하려면 0을 입력하세요");
			System.out.print("계속할까요? > ");
			int c = sc.nextInt();
			if (c == 0) {
				System.exit(0);
			}
			System.out.println();
		}
	}

	private static void thisMonth(int y, int m, int d) {//1900, 1, 1
		System.out.printf("[%d년 %d월 %d일]\n", y, m, d);
		System.out.println("----------------------------------------------------");
		System.out.println("일\t월\t화\t수\t목\t금\t토");
		System.out.println("----------------------------------------------------");
		boolean b1 = isYoon(y);//윤년이 아닙니다.
		int days = d;
		if (b1 == true) {
			days = month_yoon[m - 1];
		} else {
			days = month_nyoon[m - 1];
		}
		// 1월의 달력을 만들어보세요....
		//int wdays = 1; // 월요일, 0일요일, 1월, 2화, 3수, 4목, 5금, 6토

		int wdays = getWdays(y, m, d);

		// 달력의 시작공백을 비워두기
		for (int i = 0; i < wdays; i++) {
			System.out.print("\t");
		}
//		_  1  2  3  4  5  6
//		7  8  9  10 11 12 13
//		14 15 16 17 18 19 20

		for (int i = 0; i <=days; i++) {
			System.out.printf("%d\t", (i + 1));
			if ((wdays + (i + 1)) % 7 == 0) {
				System.out.println();
			}
		}
	}

	private static int getWdays(int y, int m, int d) {
		// 1990년이후 ok
		int y1990_wdays = 1;
		boolean b1 = isYoon(y);
		int day = (y-1)*365 + (y-1)/4 - (y-1)/100 + (y-1)/400;
		if(b1 == true) {
			for(int i=0;i<m-1;i++) {
				day += month_yoon[i];
			}
		}else {
			for(int i=0;i<m-1;i++) {
				day += month_nyoon[i];
			}
		}
		
		day+=1;
		int wdays = day%7;
		
		return wdays;
	}

	private static void initCalendar() {
		System.out.println("[1900년 1월 1일]");
		System.out.println("----------------------------------------------------");
		System.out.println("일\t월\t화\t수\t목\t금\t토");
		System.out.println("----------------------------------------------------");
		// 1900년 1월 1일은 월요일입니다.
		int y = 1900;
		boolean b1 = isYoon(y);
		if (b1 == true) {
			// System.out.println("윤년이에요!!");
		} else {
			// System.out.println("윤년이 아니에요!!");
		}
		int m = 1;
		int days = 0;
		if (b1 == true) {
			days = month_yoon[m - 1];
		} else {
			days = month_nyoon[m - 1];
		}
		// System.out.println("1900년 1월의 날자수는: "+days);

		// 1월의 달력을 만들어보세요....
		int wdays = 1; // 월요일, 0일요일, 1월, 2화, 3수, 4목, 5금, 6토

		// 달력의 시작공백을 비워두기
		for (int i = 0; i < wdays; i++) {
			System.out.print("\t");
		}
//		_  1  2  3  4  5  6
//		7  8  9  10 11 12 13
//		14 15 16 17 18 19 20

		for (int i = 0; i < days; i++) {
			System.out.printf("%d\t", (i + 1));
			if ((wdays + (i + 1)) % 7 == 0) {
				System.out.println();
			}
		}

	}

	private static boolean isYoon(int y) {
		boolean b1 = false;
		if (y % 4 == 0) {
			b1 = true;
		}
		if (y % 100 == 0) {
			b1 = false;
		}
		if (y % 400 == 0) {
			b1 = true;
		}

		return b1;
	}

}
