package basic;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MyPhoneBookApp_List {
	public static void main(String[] args) {
		descSystInfo();
		pbInit();
		MyPhoneBook_List pb = new MyPhoneBook_List();
		int sel;
		int index;
		PBook person = null;
		while (true) {
			// 종료0,추가1,전체출력2,검색3,변경4,삭제5,전체삭제6
			sel = pbMenu();
			if(sel==0) {
				System.out.println("종료");
				programExit();
				break;
			}else if(sel==1) {
				System.out.println("추가");
				person = inputPerson();
				pb.addPerson(person);
				
			}else if(sel==2) {
				System.out.println("전체출력");	
				pb.showData();
				
			}else if(sel==3) {
				System.out.println("검색");
				index = selectPerson();
				pb.showPerson(index);
				
			}else if(sel==4) {
				System.out.println("변경");
				index = selectPerson();
				person = inputPerson();
				pb.updatePerson(index, person);
				
			}else if(sel==5) {
				System.out.println("삭제");
				index = selectPerson();
				pb.deletePerson(index);
			}else if(sel==6) {
				System.out.println("전체삭제");
				int ok = confirmClear();
				if(ok==1) {
					System.out.println("전체삭제를 수행합니다....");
					timeRetrieve();
					pb.clearPerson();
					System.out.println();
					System.out.println("전체삭제되었습니다");
				}else {
					System.out.println("전체삭제를 취소했습니다");
					continue;
				}
				
			}else {
				System.out.println("다시 입력하세요");
			}
		}
	}

	private static int confirmClear() {
		int ok = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("계속 진행하겠습니까? 진행1, 중단0");
		System.out.print("선택> ");
		ok = sc.nextInt();
		return ok;
	}

	private static int selectPerson() {
		// TODO Auto-generated method stub
		int index;
		Scanner sc = new Scanner(System.in);
		System.out.println("인덱스 번호를 선택하세요");
		System.out.print("선택> ");
		index = sc.nextInt(); 
		return index;
	}

	private static PBook inputPerson() {
		String name;
		String phone;
		String email;
		String addr;
		Scanner sc =  new Scanner(System.in);
		System.out.println("주소록 데이터를 입력하세요~");
		System.out.print("이름> ");
		name = sc.next();
		System.out.print("전화번호> ");
		phone = sc.next();
		System.out.print("이메일> ");
		email = sc.next();
		System.out.print("주소> ");
		addr = sc.next();
		
		PBook person =  new PBook(name, phone, email, addr);
		return person;
	}

	private static void programExit() {
		System.out.println("프로그램을 종료합니다.");
		timeRetrieve();
		System.out.println("GoodBye!!");
		System.exit(0);
	}

	private static void descSystInfo() {
		System.out.println("PhoneBook v1.0을 시작합니다.");
		System.out.println("PhoneBook 데이터를  초기화합니다. ");
	}

	private static void pbInit() {
		timeRetrieve();
	}
	

	private static void timeRetrieve() {
		try {
			for (int i = 0; i < 20; i++) {
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print(".");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private static int pbMenu() {
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("-----------------------------------------------");
		System.out.println("메뉴: 종료0,추가1,전체출력2,검색3,변경4,삭제5,전체삭제6");
		System.out.println("-----------------------------------------------");
		System.out.print("선택> ");
		int sel = sc.nextInt();
		return sel;
	}

}
