package basic;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class MyPhoneBook_Map {
	Map<Integer, PBook> pb = new HashMap<Integer, PBook>();
	int id;
	
	MyPhoneBook_Map() {
		pb.clear();
	}

	public Map<Integer, PBook> getPb() {
		return pb;
	}

	public void setPb(Map<Integer, PBook> pb) {
		this.pb = pb;
	}
	
	public void addData(PBook pb) {
		this.id++;
		this.pb.put(id, pb);
	}
	
	//개별 데이터 변경 (key, value)
	public void updatePersonData(int key, PBook value) {
		if(pb.containsKey(key)) {
			pb.put(key, value);
		}else {
			System.out.println("검색하는 데이터가 없습니다.");
		}
	}
	//개별 데이터 출력 (key)
	public void showPersonData(int id) {
		int key = id;
		if(pb.containsKey(id)) {
			PBook value = pb.get(id);
			System.out.println("\t" + key 
					  + " : [" + value.name +"," +value.phone
					         + "," +value.email+","+ value.addr +"]");
			
		}else {
			System.out.println("검색하는 데이터가 없습니다.");
		}
	}
	//선택된 id의 개별 주소록을 삭제합니다.
	public void removePersonData(int id) {
		int key = id;
		if(pb.containsKey(key)) {
			pb.remove(key);
		}else {
			System.out.println("검색하는 데이터가 없습니다.");		
		}
	}
	//전체 주소록을 삭제합니다.
	public void removeData() {
		if(pb.size()>0) {
			pb.clear();
		}else {
			System.out.println("데이터가 없습니다.");		
		}
	}
	//전체출력
	public void showData() {
		System.out.println("주소록등록개수: "+ pb.size());
				
		Set<Integer> keySet = pb.keySet();
		Iterator<Integer> keyIterator = keySet.iterator();
		while(keyIterator.hasNext()) {
		  Integer key = keyIterator.next();
		  PBook value = pb.get(key);
		  System.out.println("\t" + key 
				  + " : [" + value.name +"," +value.phone
				         + "," +value.email+","+ value.addr +"]");
		}		
		System.out.println();
		
	}

	@Override
	public String toString() {
		return "MyPhoneBook_Map [pb=" + pb + ", id=" + id + "]";
	}

}
