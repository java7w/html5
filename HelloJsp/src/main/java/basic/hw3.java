package basic;

import java.io.IOException;
import java.util.Scanner;

public class hw3 {

	public static void main(String[] args) {
		int y = 1900;
		int m = 1;
		int d = 1;

		boolean isYoon = false;

		String date;
		String[] date2;
		int sel = 0;
		do {
			sel = printMenu();
			switch (sel) {
			case 1:
				System.out.println("달력을 출력합니다.");
				date = inputDate();
				date2 = date.split("-");
				y = Integer.valueOf(date2[0]);
				m = Integer.valueOf(date2[1]);
				d = Integer.valueOf(date2[2]);
				printMonth(y, m, d);
				break;
			case 9:
				System.out.println("달력보기를 중지합니다.");
				System.exit(0);
				break;
			}

		} while (sel != 9);
		System.out.println("프로그램을 종료합니다.");
	}

	private static void printMonth(int y, int m, int d) {
		// 시작요일
		// 12달동안의 끝나는 날자 -> 평년, 윤년
		int[] month1 = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int[] month2 = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int[] month = new int[12];

		boolean isYoon = isYoonYear(y);

		if (isYoon) {
			System.arraycopy(month2, 0, month, 0, month.length);
		} else {
			System.arraycopy(month1, 0, month, 0, month.length);
		}

		int[] w = getWeekYear(y);
		System.out.printf("[%d년도 %d월 의 달력]\n", y, m);
		System.out.println("일\t월\t화\t수\t목\t금\t토");
		for (int i = 0; i < 1; i++) {
			System.out.printf("\t");
		}

		for (int i = 0; i < month[m - 1]; i++) {
			if ((i + 1) % 7 == 0) {
				System.out.println();
			}
			System.out.printf("%d\t", i + 1);
		}
		System.out.println();

	}

	private static int[] getWeekYear(int y) {
		int[] month1 = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int[] month2 = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int[] month = new int[12];
		// 1900
		// {1, 4, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6, 2}
		int[] w = new int[13];
		int year = 1900;
		int wparam = 365;
		boolean isYoon = isYoonYear(year);
		if (y >= year) {
			for (int i = 0; i < y + 1; i++) {
				isYoon = isYoonYear(y);
				if (isYoon) {
					System.arraycopy(month2, 0, month, 0, month.length);
					wparam = 366;
				} else {
					System.arraycopy(month1, 0, month, 0, month.length);
					wparam = 365;
				}
				if (i == year) {
					w[0] = 1;
				} else {
					w[0] = (w[0] + wparam) % 7;
				}

				for (int j = 0; j < 12; j++) {
					w[j + 1] = w[j] + month[j] % 7;
					w[j + 1] %= 7;
				}
			}
		}
		return null;
	}

	private static boolean isYoonYear(int y) {
		boolean isYoon = false;
		if (y % 400 == 0 || y % 4 == 0 && y % 100 != 0) {
			isYoon = true;
		}
		return isYoon;
	}

	private static String inputDate() {
		Scanner sc1 = new Scanner(System.in);
		int y = 1900;
		int m = 1;
		int d = 1;

		System.out.println("보기 원하는 달력의 날자를 입력하세요.");
		System.out.print("년> ");
		y = sc1.nextInt();
		System.out.print("월> ");
		m = sc1.nextInt();
		System.out.print("일> ");
		d = sc1.nextInt();
		String date = y + "-" + m + "-" + d;
		return date;
	}

	private static int printMenu() {
		Scanner sc1 = new Scanner(System.in);
		System.out.println("=========================================");
		System.out.println("달력 출력 프로그램 v1.0");
		System.out.println("-----------------------------------------");
		System.out.println(" 1 달력보기");
		System.out.println(" 9 종료하기");
		System.out.println("-----------------------------------------");
		System.out.print("선택> ");
		int sel = sc1.nextInt();
		System.out.println();
		return sel;
	}
}
