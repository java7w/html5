package basic;

public class FactorialTest {

	public static void main(String[] args) {
		// TODO 재귀함수 사용 실습
		long result = factorial(4);// 4* 3* 2*1 = 24
		System.out.println("factorial(4)의 결과 = " + result);
	}

	private static long factorial(int n) {
		// 팩토리얼 계산 하는 함수 구현...
		long result = 0;
		if (n == 1) {
			result = 1;
		} else {
			result = n * factorial(n - 1);
		}
		return result;
	}

}
