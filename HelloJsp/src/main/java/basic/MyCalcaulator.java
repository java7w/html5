package basic;

import java.util.Scanner;

public class MyCalcaulator {

	public static void main(String[] args) {
		// -- 계산기 만들기
		// 목표: 덧셈, 뺄셈, 곱셈, 나눗셈을 계산하는 간단한 계산기 프로그램 제작
		// 메뉴구성을 할것
		// ---------------------------------------
		// "계산기 만들기 v1.0"
		// 메뉴안내:
		// 1번 두수를 입력하세요
		// 2번 계산기능을 선택하세요.
		// 3번 종료, 계속을 선택하세요.
		// ---------------------------------------

		menu();
	}

	private static void menu() {
		Scanner sc = new Scanner(System.in);
		while(true) {
			System.out.println("-----------------------");
			System.out.println("계산기 만들기 v1.0");
			System.out.println("종료하려면 0를 입력하고, 계속하려면 1입력하세요");
			System.out.println("-----------------------");
			System.out.print("메뉴선택> ");
			int m = sc.nextInt();
			if (m == 0) {
				System.out.println("계산기를 종료합니다.");
				System.exit(0);
			} else {
				System.out.println("1번 두수를 입력하세요");
				System.out.print("n1> ");
				int n1 = sc.nextInt();
				System.out.print("n2> ");
				int n2 = sc.nextInt();
				Scanner sc2 = new Scanner(System.in);
				System.out.println("2번 계산기능을 선택하세요.");
				System.out.println("+, -, *, / 문자 하나를 입력하세요 ");
				System.out.print("메뉴선택> ");
				String s1 = sc2.next();
				double result = 0;
				if (s1.equals("+")) {
					result = n1 + n2;
				}
				if (s1.equals("-")) {
					result = n1 - n2;
				}
				if (s1.equals("*")) {
					result = n1 * n2;
				}
				if (s1.equals("/")) {
					result = n1 / n2;
				}
				System.out.println("계산결과: " + result);
			}
		}

	}
}
