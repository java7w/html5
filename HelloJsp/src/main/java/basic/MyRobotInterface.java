package basic;

public interface MyRobotInterface {
	public void leg();
	public void head();
	public void gps();
	public default void romance() {
		System.out.println("LoveLove!~");
	}
}
