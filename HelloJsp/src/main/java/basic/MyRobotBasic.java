package basic;

public class MyRobotBasic implements MyRobotInterface {
	int dist = 0;//거리
	int dir = 0;//방향
	int org = 0;
	
	@Override
	public void leg() {
		System.out.println("다리를 움직입니다!!");
		this.dist++;
	}
	@Override
	public void head() {
		System.out.println("주변상황을 탐지합니다!!");
		this.dir++;// 동1 서2 남3 북4
		if(this.dir>4) {
			this.dir = 1;
		}else if(this.dir<1) {
			this.dir =4;
		}
	}
	@Override
	public void gps() {
		System.out.println("위치탐지를 위해 gps를 동작시킵니다!!");
		this.org++;
		
	}

	public int getDist() {
		return dist;
	}
	public int getDir() {
		return dir;
	}
	public int getOrg() {
		return org;
	}
	public static void main(String[] args) {
		MyRobotBasic rob = new MyRobotBasic();
		rob.romance();
		rob.gps();
		System.out.println("현재 gps 위치: " + rob.getOrg());
		

	}

}
