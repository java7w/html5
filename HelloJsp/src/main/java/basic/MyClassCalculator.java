package basic;

import java.util.Scanner;

public class MyClassCalculator {

	public static void main(String[] args) {
		// 기본계산기, 중급계산기, 고급계산기를 만듦니다.
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("--------------------------------------");
			System.out.println("메뉴: 1:기본계산기, 2:중급계산기, 3:고급계산기");
			System.out.println("     0:종료");
			System.out.println("--------------------------------------");
			System.out.println("계산기를 선택하세요");
			System.out.print("선택> ");
			int calc = sc.nextInt();
			if (calc == 1) {
				System.out.println("기본계산기");
				basicCalcProcess();
			} else if (calc == 2) {
				System.out.println("중급계산기");
				regularCalcProcess();
			} else if (calc == 3) {
				System.out.println("고급계산기");
				advCalcProcess();
			} else if (calc == 0) {
				System.out.println("계산기 프로그램을 종료합니다. bye~!");
				System.exit(0);
			} else {
				System.out.println("계산기를 다시선택하세요");
			}
		}

	}

	private static void advCalcProcess() {
		System.out.println("계산기를 만듦니다. !!");
		AdvCalculator calc = new AdvCalculator();
		Scanner sc = new Scanner(System.in);
		System.out.println("adv> 계산기를 계속사용하려면 c을 누르고 종료하려면 q을 입력하세요!");
		System.out.println("adv> 덧셈+, 뺄셈-, 곱셈*, 나눗셈/, 나머지%을 선택하세요");
		int n1, n2;
		double res;
		while (true) {
			System.out.print("adv:입력1> ");
			String sel = sc.next();
			n1 = Integer.parseInt(sel);
			calc.n1 = n1;
			System.out.print("adv:입력2> ");
			sel = sc.next();
			n2 = Integer.parseInt(sel);
			calc.n2 = n2;
			
			System.out.print("basic:연산> ");
			sel = sc.next();
			if (sel.equals("q")) {
				break;
			} else if (sel.equals("c")) {
				continue;
			} else if (sel.equals("+")) {
				System.out.println("+연산을 합니다.");
				res = calc.plus();
				System.out.println("계산결과> "+ res);
			} else if (sel.equals("-")) {
				System.out.println("-연산을 합니다.");
				res = calc.minus();
				System.out.println("계산결과> "+ res);
			} else if (sel.equals("*")) {
				System.out.println("*연산을 합니다.");
				res = calc.multi();
				System.out.println("계산결과> "+ res);
			} else if (sel.equals("/")) {
				System.out.println("/연산을 합니다.");
				res = calc.divid();
				System.out.println("계산결과> "+ res);
			} else if (sel.equals("%")) {
				System.out.println("%연산을 합니다.");
				res = calc.modul();
				System.out.println("계산결과> "+ res);
			}
		}

	}

	private static void regularCalcProcess() {
		System.out.println("계산기를 만듦니다. !!");
		RegularCalculator calc = new RegularCalculator();
		Scanner sc = new Scanner(System.in);
		System.out.println("regular> 계산기를 계속사용하려면 c을 누르고 종료하려면 q을 입력하세요!");
		System.out.println("regular> 덧셈+, 뺄셈-, 곱셈*, 나눗셈/을 선택하세요");
		int n1, n2;
		double res;
		while (true) {
			System.out.print("basic:입력1> ");
			String sel = sc.next();
			n1 = Integer.parseInt(sel);
			calc.n1 = n1;
			System.out.print("basic:입력2> ");
			sel = sc.next();
			n2 = Integer.parseInt(sel);
			calc.n2 = n2;
			
			System.out.print("basic:연산> ");
			sel = sc.next();
			if (sel.equals("q")) {
				break;
			} else if (sel.equals("c")) {
				continue;
			} else if (sel.equals("+")) {
				System.out.println("+연산을 합니다.");
				res = calc.plus();
				System.out.println("계산결과> "+ res);
			} else if (sel.equals("-")) {
				System.out.println("-연산을 합니다.");
				res = calc.minus();
				System.out.println("계산결과> "+ res);
			} else if (sel.equals("*")) {
				System.out.println("*연산을 합니다.");
				res = calc.multi();
				System.out.println("계산결과> "+ res);
			} else if (sel.equals("/")) {
				System.out.println("/연산을 합니다.");
				res = calc.divid();
				System.out.println("계산결과> "+ res);
			}
		}
	}

	private static void basicCalcProcess() {
		System.out.println("계산기를 만듦니다. !!");
		BasicCalculator calc = new BasicCalculator();
		Scanner sc = new Scanner(System.in);
		System.out.println("basic> 계산기를 계속사용하려면 c을 누르고 종료하려면 q을 입력하세요!");
		System.out.println("basic> 두 숫자를 연속으로 입력하고 덧셈과 뺄셈을 선택하세요");
		int n1, n2;
		double res;
		while (true) {
			System.out.print("basic:입력1> ");
			String sel = sc.next();
			n1 = Integer.parseInt(sel);
			calc.n1 = n1;
			System.out.print("basic:입력2> ");
			sel = sc.next();
			n2 = Integer.parseInt(sel);
			calc.n2 = n2;
			System.out.print("basic:연산> ");
			sel = sc.next();
			if (sel.equals("q")) {
				break;
			} else if (sel.equals("c")) {
				continue;
			} else if (sel.equals("+")) {
				System.out.println("+연산을 합니다.");
				res = calc.plus();
				System.out.println("계산결과> "+ res);
			} else if (sel.equals("-")) {
				System.out.println("-연산을 합니다.");
				res = calc.minus();
				System.out.println("계산결과> "+ res);
			}
		}
	}

}

class BasicCalculator {
	int n1, n2;
	double res;
	BasicCalculator() {
		System.out.println("BasicCalculator를 생성합니다.!");
		this.n1=0;
		this.n2=0;
		this.res=0;
	}
	
	double plus(){
		res = n1+n2;
		return res ;
	}
	double minus(){
		res = n1-n2;
		return res ;
	}
}

class RegularCalculator extends BasicCalculator{
	int n1, n2;
	double res;
	RegularCalculator() {
		System.out.println("RegularCalculator를 생성합니다. !");
	}
	double multi(){
		System.out.println("n1: "+n1+", n2: " + n2);
		res = n1*n2;
		return res ;
	}
	double divid(){
		System.out.println("n1: "+n1+", n2: " + n2);
		res = n1/n2;
		return res ;
	}
	
}

class AdvCalculator extends RegularCalculator{
	int n1, n2;
	double res;
	AdvCalculator() {
		System.out.println("AdvCalculator를 생성합니다. !");
	}
	double modul(){
		res = n1%n2;
		res = Math.round(res*100)/100.0;
		return res;
	}
	double multi(){
		System.out.println("n1: "+n1+", n2: " + n2);
		res = n1*n2;
		//33.3333*100 => (3333) => 3333/100 => 33.33
		res = Math.round(res*100)/100.0;
		return res ;
	}
	double divid(){
		System.out.println("n1: "+n1+", n2: " + n2);
		res = n1/n2;
		res = Math.round(res*100)/100.0;
		return res ;
	}
	
	
}