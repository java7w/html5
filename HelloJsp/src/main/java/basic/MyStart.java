package basic;

public class MyStart {

	public static void main(String[] args) {
		// printing....star shape

		// starShape1();
		// starShape2();
		// starShape3();
		starShape4();
	}
	
/*---------------별표 만들기
    *
   ***
  *****
 *******
*********
***********
*********
 *******
  *****
   ***
    *
----------------------*/
	private static void starShape4() {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5 - i; j++) {
				System.out.print(" ");
			}
			for (int j = 0; j < i * 2 + 1; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
		for (int i = 6; i > 0; i--) {
			for (int j = 0; j < 6 - i; j++) {
				System.out.print(" ");
			}
			for (int j = 0; j < (i - 1) * 2 + 1; j++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}

	/*----------------별만들기
	*********
	 *******
	  *****
	   ***
	    *
	--------------------*/
	private static void starShape3() {
		for (int i = 5; i > 0; i--) {
			for (int j = 0; j < 5 - i; j++) {
				System.out.print(" ");
			}
			for (int j = 0; j < (i - 1) * 2 + 1; j++) {
				System.out.print("*");
			}
			System.out.println();
		}

	}

	/*----------------별표찍기
		  *
		 ***
		*****
	   *******
	  *********
	---------------*/
	private static void starShape2() {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 4 - i; j++) {
				System.out.print(" ");
			}
			for (int j = 0; j < 2 * i + 1; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
	}

	/*--------별표 찍기
	 *****
	  ****
	   ***
	    **
	     *
	 ---------------*/
	private static void starShape1() {
		// TODO Auto-generated method stub
		for (int i = 0; i < 5; i++) {// 줄번호....
			for (int j = 0; j < 5; j++) {// 5개의 별을 찍기

			}
			System.out.println();
		}

	}

}
