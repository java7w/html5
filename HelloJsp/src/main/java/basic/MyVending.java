package basic;

import java.util.Scanner;

public class MyVending {

	public static void main(String[] args) {
		// 자판기프로그램을 만듦니다.
		// 자판기 v1.0
		// 코인은 1000원, 500원, 100원, 50원, 10원 입력받습니다.
		// 거스름돈도 입력받는 코인을 사용합니다
		// 메뉴를 만듧니다.
		// 상품종류는: 카페라떼 2550원, 갈아만든배 1520원, 딸기라떼 3510원, 먹는샘물 1110원, 포카리 1650원

		// 자판기를 시작합니다.
		// 코인을 입력하세요
		// 상품을 선택하세요
		// 계속주문할건아요? 1번이면 계속, 0번이면 종료
		// 종료이면 프로그램을 종료합니다.
		// 계속이면 현재잔액을 비교해서 코인을 입력하세요 또는 상품을 선택하세요를 출력합니다.
		// 거스름 반환할 때는 큰돈부터 1000원, 500원, 100원, 50원, 10원 순서로 거스름돈의 개수를 출력합니다.
		myVendingInit();

	}

	private static void myVendingInit() {
		System.out.println("자판기를 시작합니다.");
		for(int i=0;i<10;i++) {
			try {
				Thread.sleep(1000);
				System.out.print(".");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("--------------------------");
		System.out.println("자판기 v1.0");
		System.out.println("--------------------------");
		int money = 0;
		System.out.println("현재잔액: "+ money);
		System.out.println("코인을 입력하세요");
		System.out.println("코인: 1번 1000원, 2번 500원, 3번 100원, 4번 50원, 5반 10원 하나씩 선택하세요");
		Scanner sc = new Scanner(System.in);
		int coin = sc.nextInt();
		switch(coin) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			money += coin;
			break;
		default:
			System.out.println("금액입력이 잘못 되었습니다. ");
			break;
		}
		
		
		// 상품을 선택하세요
		// 계속주문할건아요? 1번이면 계속, 0번이면 종료
		// 종료이면 프로그램을 종료합니다.
		// 계속이면 현재잔액을 비교해서 코인을 입력하세요 또는 상품을 선택하세요를 출력합니다.
		// 거스름 반환할 때는 큰돈부터 1000원, 500원, 100원, 50원, 10원 순서로 거스름돈의 개수를 출력합니다.
	}

}
