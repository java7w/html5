package basic;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MyLogger {

	private static MyLogger instance = new MyLogger();
	Logger logger = Logger.getLogger("mylogger");

	// Level별 Log를 생성할 파일 지정
	public static final String errorLog = "log.txt";
	public static final String warningLog = "warning.txt";
	public static final String fineLog = "fine.txt";

	private FileHandler logFile = null;
	private FileHandler warningFile = null;
	private FileHandler fineFile = null;

	static int count;

	private MyLogger() {
		System.out.println("클래스 instance 생성 !!!");
		this.count++;

		try {
			// path, append 방식으로 생성
			logFile = new FileHandler(errorLog, true);
			warningFile = new FileHandler(warningLog, true);
			fineFile = new FileHandler(fineLog, true);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		logFile.setFormatter(new SimpleFormatter());
		warningFile.setFormatter(new SimpleFormatter());
		fineFile.setFormatter(new SimpleFormatter());

		logFile.setLevel(Level.ALL);
		fineFile.setLevel(Level.FINE);
		warningFile.setLevel(Level.WARNING);

		logger.addHandler(logFile);
		logger.addHandler(fineFile);
		logger.addHandler(warningFile);
	}

	public static MyLogger getInstance() {
		if (instance == null) {
			instance = new MyLogger();
		}
		return instance;
	}

	public int getCount() {
		return this.count;
	}

	public void log(String msg) {
		logger.finest(msg);
		logger.finer(msg);
		logger.fine(msg);
		logger.config(msg);
		logger.info(msg);
		logger.warning(msg);
		logger.severe(msg);
	}

	public void fine(String msg) {
		logger.fine(msg);
	}

	public void warning(String msg) {
		logger.warning(msg);
	}

}
