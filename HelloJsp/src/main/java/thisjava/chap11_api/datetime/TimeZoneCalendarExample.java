package thisjava.chap11_api.datetime;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeZoneCalendarExample {
	public static void main(String[] args) {
		Calendar now = Calendar.getInstance();
		//TimeZone timeZone = TimeZone.getTimeZone("Asia/Seoul");
		//TimeZone timeZone = TimeZone.getTimeZone("Australia/Sydney");
		TimeZone timeZone = TimeZone.getTimeZone("America/New_York");
		
//		Australia/Sydney
//		America/New_York
		
		
		TimeZone time;
		Date date = new Date();
		DateFormat df = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss (z Z)");

		time = TimeZone.getTimeZone("Asia/Seoul");
		df.setTimeZone(time);
		System.out.format("%s%n%s%n%n", time.getDisplayName(),
				df.format(date));

		time = TimeZone.getTimeZone("JST");
		df.setTimeZone(time);
		System.out.format("%s%n%s%n%n", time.getDisplayName(),
				df.format(date));

		time = TimeZone.getTimeZone("America/Los_Angeles");
		df.setTimeZone(time);
		System.out.format("%s%n%s%n%n", time.getDisplayName(),
				df.format(date));

		time = TimeZone.getTimeZone("America/New_York");
		df.setTimeZone(time);
		System.out.format("%s%n%s%n%n", time.getDisplayName(),
				df.format(date));

		time = TimeZone.getTimeZone("Pacific/Honolulu");
		df.setTimeZone(time);
		System.out.format("%s%n%s%n%n", time.getDisplayName(),
				df.format(date));
	}
}