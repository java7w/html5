package thisjava.chap11_api.math;

import java.util.Scanner;

public class MathExample {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		mathBasic();
		
		while (true) {
			System.out.println("-------------------------------");
			System.out.println("math program v1.0");
			System.out.println("math함수를 사용합니다.");
			System.out.println("-------------------------------");
			System.out.println("숫자를 입력하세요");
			System.out.print("숫자1> ");
			String s1 = sc.next();
			
			System.out.println("________절대값 abs():정수_________");
			System.out.println("________절대값 abs():double_________");
			double n1 = Double.valueOf(s1);
			double v1 = Math.abs(n1);
			System.out.println("v1=" + v1);

			System.out.println("________올림값 ceil():double_________");
			double v2 = Math.ceil(n1);

			System.out.println("________내림값 floor():double_________");
			double v3 = Math.floor(n1);
			
			System.out.println("2번째 숫자를 입력하세요");
			System.out.print("숫자2> ");
			String s2 =  sc.next();
			double n2 = Double.valueOf(s2);
			
			System.out.println("________최대값선택 max(n1,n2)_________");
			double v4 = Math.max(n1, n2);
			System.out.println("최대값은 v4=" + v4);

			System.out.println("________최소값선택 max(n1,n2)_________");
			double v5 = Math.min(n1, n2);
			System.out.println("최소값 v5=" + v5);

			System.out.println("________랜덤값선택 random()_________");
			double v6 = Math.random();
			System.out.println("랜덤선택값: v6=" + v6);

			System.out.println("________가장가까운 정수값 선택 rint()_________");
			double v7 = Math.rint(n1);
			double v8 = Math.rint(n2);
			System.out.println("n1에 가까운 숫자: v7=" + v7);
			System.out.println("n2에 가까운 숫자: v8=" + v8);

			System.out.println("________반올림값선택 round()_________");
			long v9 = Math.round(n1);
			long v10 = Math.round(n2);
			System.out.println("n1의 반올림: v9=" + v9);
			System.out.println("n2의 반올림: v10=" + v10);

			System.out.println("________계산을 통한 반올림, 소수점이하 2자리수구하기()_________");
			System.out.println("숫자를 입력하세요");
			System.out.print("숫자3> ");
			String s3 = sc.next();
			Double n3 =  Double.valueOf(s3);
			double temp1 = n3 * 100;
			long temp2 = Math.round(temp1);
			double v11 = temp2 / 100.0;
			System.out.println("반올림, 소수점2째자리구하기: v11=" + v11);
		}
		
		
	}

	private static void mathBasic() {
		System.out.println("________절대값 abs():정수_________");
		int v1 = Math.abs(-5);
		System.out.println("v1=" + v1);
		System.out.println("________절대값 abs():double_________");
		double v2 = Math.abs(-3.14);
		System.out.println("v2=" + v2);

		System.out.println("________올림값 ceil():double_________");
		double v3 = Math.ceil(5.3);
		double v4 = Math.ceil(-5.3);
		System.out.println("v3=" + v3);
		System.out.println("v4=" + v4);

		System.out.println("________내림값 floor():double_________");
		double v5 = Math.floor(5.3);
		double v6 = Math.floor(-5.3);
		System.out.println("v5=" + v5);
		System.out.println("v6=" + v6);
		System.out.println("________최대값선택 max(n1,n2)_________");
		int v7 = Math.max(5, 9);
		double v8 = Math.max(5.3, 2.5);
		System.out.println("v7=" + v7);
		System.out.println("v8=" + v8);

		System.out.println("________최소값선택 max(n1,n2)_________");
		int v9 = Math.min(5, 9);
		double v10 = Math.min(5.3, 2.5);
		System.out.println("v9=" + v9);
		System.out.println("v10=" + v10);

		System.out.println("________랜덤값선택 random()_________");
		double v11 = Math.random();
		System.out.println("v11=" + v11);

		System.out.println("________가장가까운 정수값 선택 rint()_________");
		double v12 = Math.rint(5.3);
		double v13 = Math.rint(5.7);
		System.out.println("v12=" + v12);
		System.out.println("v13=" + v13);

		System.out.println("________반올림값선택 round()_________");
		long v14 = Math.round(5.3);
		long v15 = Math.round(5.7);
		System.out.println("v14=" + v14);
		System.out.println("v15=" + v15);

		System.out.println("________계산을 통한 소수점이하 자리수구하기()_________");
		double value = 12.3456;
		double temp1 = value * 100;
		long temp2 = Math.round(temp1);
		double v16 = temp2 / 100.0;
		System.out.println("v16=" + v16);
		
	}
}
