package thisjava.chap11_api.newinstance;

import java.util.concurrent.TimeUnit;

public class NewInstanceExample {
	public static void main(String[] args) {
		try {
			Class clazz = Class.forName("thisjava.chap11_api.newinstance.SendAction");
			//Class clazz = Class.forName("thisjava.chap11_api.newinstance.ReceiveAction");
			Action action = (Action) clazz.newInstance();
			action.execute();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		System.out.println("전송합니다.....");
		try {
			for (int i = 0; i < 20; i++) {
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print(".");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println();
		try {
			//Class clazz = Class.forName("thisjava.chap11_api.newinstance.SendAction");
			Class clazz = Class.forName("thisjava.chap11_api.newinstance.ReceiveAction");
			Action action = (Action) clazz.newInstance();
			action.execute();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		
	}
}


