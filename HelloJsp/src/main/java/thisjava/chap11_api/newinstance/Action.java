package thisjava.chap11_api.newinstance;

public interface Action {
	public void execute();
}
