package thisjava.chap11_api;

import java.util.concurrent.TimeUnit;

public class SystemTimeExample {
	public static void main(String[] args) {
		long time1 = System.nanoTime();
		
		int sum = 0;
		for(int i=1; i<=5000000; i++) {
			sum += i;
		}
		try {
			for (int i = 0; i < 20; i++) {
				TimeUnit.MILLISECONDS.sleep(100);
				System.out.print(".");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		long time2 = System.nanoTime();
		System.out.println();
		System.out.println("1~1000000까지의 합: " + sum);
		System.out.println("계산에 " + (time2-time1)*0.000000001 + " 초가 소요되었습니다.");
	}
}
