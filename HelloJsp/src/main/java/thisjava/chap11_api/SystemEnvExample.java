package thisjava.chap11_api;

public class SystemEnvExample {
	public static void main(String[] args) {
		String javaHome = System.getenv("JAVA_HOME");		
		System.out.println("[ JAVA_HOME ]  " + javaHome);
		
		String systemos = System.getenv("OS");		
		System.out.println("[ OS ]  " + systemos);
	}
}
