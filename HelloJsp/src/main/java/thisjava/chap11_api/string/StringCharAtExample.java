package thisjava.chap11_api.string;

import java.util.Scanner;

public class StringCharAtExample {
	public static void main(String[] args) {
		String ssn = "010624-1230123";
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("주민번호를 입력하세요");
			System.out.print("입력> ");
			ssn = sc.next();
			char sex = ssn.charAt(7);
			switch (sex) {
			case '1':
			case '3':
				System.out.println("남자 입니다.");
				break;
			case '2':
			case '4':
				System.out.println("여자 입니다.");
				break;
			default:
				System.out.println("주민번호가 잘못 입력되었습니다.");
				break;
			}
		}
	}
}
