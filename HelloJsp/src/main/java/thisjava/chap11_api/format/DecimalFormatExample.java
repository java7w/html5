package thisjava.chap11_api.format;

import java.text.DecimalFormat;
import java.util.Scanner;

public class DecimalFormatExample {
	public static void main(String[] args) {
		numberFormatPrint();
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("----------------------------------");
			System.out.println("숫자변환포맷 v1.0");
			System.out.println("----------------------------------");
			System.out.println("소수점을 포함한 숫자를 입력하세요");
			System.out.print("입력> ");

			double num = sc.nextDouble();
			System.out.println("-> 1234567.89을 입력한 출력 예");
			System.out.println("_____DecimalFormat('0'): 0을 채워넣는 표기-소수점없이표기_____");
			DecimalFormat df = new DecimalFormat("0");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('0.0'): 소수점 한자리수 표기_____");
			df = new DecimalFormat("0.0");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('0000000000.00000'): 자리수를 0으로 채워넣는 표기_____");
			df = new DecimalFormat("0000000000.00000");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('#'): #은 숫자가 올수있음_____");
			df = new DecimalFormat("#");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('#.#'): 소수점 한자리 표기_____");
			df = new DecimalFormat("#.#");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('#.#'): 비워있는 자리는 0으로 채우지않는 표기_____");
			df = new DecimalFormat("##########.#####");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('#.0'): #표기와 0표기를 섞어서 사용_____");
			df = new DecimalFormat("#.0");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('+#.0'): +양수임을 나타내는 표기_____");
			df = new DecimalFormat("+#.0");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('-#.0'): -음수임을 나타내는 표기_____");
			df = new DecimalFormat("-#.0");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('#,###.0'): ,를 사용해서 자리수표시_____");
			df = new DecimalFormat("#,###.0");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('0.0E0'): 지수형태표시_____");
			df = new DecimalFormat("0.0E0");
			System.out.println(df.format(num));

			df = new DecimalFormat("+#,### ; -#,###");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('#.# %'): % 표시_____");
			df = new DecimalFormat("#.# %");
			System.out.println(df.format(num));

			System.out.println("_____DecimalFormat('\\u00A4 #,###'): unicode문자를 사용할 수 있음_____");
			df = new DecimalFormat("\u00A4 #,###");
			System.out.println(df.format(num));
		}
	}

	private static void numberFormatPrint() {
		double num = 1234567.89;
		System.out.println("-> 1234567.89을 입력한 출력 예");
		System.out.println("_____DecimalFormat('0'): 0을 채워넣는 표기-소수점없이표기_____");
		DecimalFormat df = new DecimalFormat("0");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('0.0'): 소수점 한자리수 표기_____");
		df = new DecimalFormat("0.0");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('0000000000.00000'): 자리수를 0으로 채워넣는 표기_____");
		df = new DecimalFormat("0000000000.00000");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('#'): #은 숫자가 올수있음_____");
		df = new DecimalFormat("#");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('#.#'): 소수점 한자리 표기_____");
		df = new DecimalFormat("#.#");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('#.#'): 비워있는 자리는 0으로 채우지않는 표기_____");
		df = new DecimalFormat("##########.#####");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('#.0'): #표기와 0표기를 섞어서 사용_____");
		df = new DecimalFormat("#.0");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('+#.0'): +양수임을 나타내는 표기_____");
		df = new DecimalFormat("+#.0");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('-#.0'): -음수임을 나타내는 표기_____");
		df = new DecimalFormat("-#.0");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('#,###.0'): ,를 사용해서 자리수표시_____");
		df = new DecimalFormat("#,###.0");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('0.0E0'): 지수형태표시_____");
		df = new DecimalFormat("0.0E0");
		System.out.println(df.format(num));

		df = new DecimalFormat("+#,### ; -#,###");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('#.# %'): % 표시_____");
		df = new DecimalFormat("#.# %");
		System.out.println(df.format(num));

		System.out.println("_____DecimalFormat('\\u00A4 #,###'): unicode문자를 사용할 수 있음_____");
		df = new DecimalFormat("\u00A4 #,###");
		System.out.println(df.format(num));

	}
}
