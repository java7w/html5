package thisjava.chap15.sec02.exam01_arraylist;

import java.util.*;

public class ArrayListExample {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		
		System.out.println("_______데이터 추가______");
		list.add("Java");
		list.add("JDBC");
		list.add("Servlet/JSP");
		System.out.println("_______2번 인덱스_데이터 추가______");
		list.add(2, "Database");
		list.add("iBATIS");
		System.out.println("* 전체 출력: ");
		for(int i=0; i<list.size(); i++) {
			String str = list.get(i);
			System.out.println(i + ":" + str);
		}
		System.out.println();
		
		System.out.println("_______size() 데이터의 수______");
		int size = list.size();
		System.out.println("총 객체수: " + size);		
		System.out.println();
		
		System.out.println("_______get(index) 데이터검색______");
		String skill = list.get(2);
		System.out.println("인덱스 2의 리스트 객체: " + skill);
		System.out.println();
		
		System.out.println("_______set(index) 데이터변경______");
		list.set(2, "HTML");
		
		
		System.out.println("* 전체 출력: ");
		for(int i=0; i<list.size(); i++) {
			String str = list.get(i);
			System.out.println(i + ":" + str);
		}
		System.out.println();
		
		System.out.println("_______remove(index) 데이터삭제______");
		System.out.println("삭제(remove) 를 실행합니다. 2번인덱스데이터를 삭제!!");
		list.remove(2);
		System.out.println("삭제(remove) 를 실행합니다. 2번인덱스데이터를 삭제!!");
		list.remove(2);
		System.out.println("삭제(remove) 를 실행합니다. 'iBatis' 데이터를 삭제!!");
		list.remove("iBATIS");		
		
		System.out.println("* 전체 출력: ");
		for(int i=0; i<list.size(); i++) {
			String str = list.get(i);
			System.out.println(i + ":" + str);
		}
		
		System.out.println("_______clear() 데이터 전체삭제______");
		list.clear();
		System.out.println("* 전체 출력: ");
		for(int i=0; i<list.size(); i++) {
			String str = list.get(i);
			System.out.println(i + ":" + str);
		}
	}
}

