package mvc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dbconnclose.DbConnClose;
import mvc.MemberVO;

public class MemberDao {
	private static MemberDao dao = new MemberDao();
	
	public static MemberDao getInstance() {
		return dao;
	}
	
	public Connection connect() {
		Connection conn = null;
		conn = DbConnClose.getConnection();
		return conn;
	}
	
	public void close(Connection conn, PreparedStatement pstmt, ResultSet rs) {
		DbConnClose.resourceClose(rs, pstmt, conn);
	}
	
	public void close(Connection conn, PreparedStatement pstmt) {
		DbConnClose.resourceClose(pstmt, conn);
	}
	
	public void memberInsert(MemberVO member) {
		Connection conn;
		PreparedStatement pstmt = null;
		String sql = "insert into member(id, pw, name, mail) VALUES(?,?,?,?)";
		conn = connect();
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,  member.getId());
			pstmt.setString(2, member.getPw());
			pstmt.setString(3, member.getName());
			pstmt.setString(4, member.getMail());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("insert 오류발생"+e.getMessage());
			e.printStackTrace();
		}finally {
			this.close(conn, pstmt);
		}
	}
	
	public MemberVO memberSearch(String id) {
		MemberVO member = null;
		
		Connection conn = null; 
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from member where id=?";
		
		conn = DbConnClose.getConnection();
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				member = new MemberVO();
				member.setId(rs.getString("id"));
				member.setPw(rs.getString("pw"));
				member.setName(rs.getString("name"));
				member.setMail(rs.getString("mail"));
			}
		} catch (SQLException e) {
			System.out.println("검색오류! "+e.getMessage());
			e.printStackTrace();
		} finally {
			DbConnClose.resourceClose(rs, pstmt, conn);
		}
		return member;
	}
	public ArrayList<MemberVO> memberSearchName(String name) {
		ArrayList<MemberVO> list = new ArrayList<MemberVO>();

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		MemberVO member = null;
		String sql = "select * from member where name like lower('%?%')";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				member = new MemberVO();
				member.setId(rs.getString("id"));
				member.setPw(rs.getString("pw"));
				member.setName(rs.getString("name"));
				member.setMail(rs.getString("mail"));
				list.add(member);
			}
		} catch (SQLException e) {
			System.out.println("search name 오류 발생!! " + e.getMessage());
			e.printStackTrace();
		}		
		return list;
	}
	
	public void memberDelete(String id) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "delete from member where ?";
		
		conn = DbConnClose.getConnection();
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("delete 오류!!"+e.getMessage());
			e.printStackTrace();
		} finally {
			DbConnClose.resourceClose(pstmt, conn);
		}
	}
	
	public void memberUpdate(MemberVO member) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "update member set pw=?, name=?, mail=? where id=?";
		
		conn = DbConnClose.getConnection();
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, member.getPw());
			pstmt.setString(2, member.getName());
			pstmt.setString(3, member.getMail());
			pstmt.setString(4, member.getId());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println("update 오류발생! "+e.getMessage());
			e.printStackTrace();
	 	} finally {
	 		DbConnClose.resourceClose(pstmt, conn);
	 	}
	}
	
	public ArrayList<MemberVO> memberList(){
		ArrayList<MemberVO> list = new ArrayList<MemberVO>();
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from member";
		MemberVO member = null;
		
		conn = DbConnClose.getConnection();
		try {
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				member = new MemberVO();
				member.setId(rs.getString("id"));
				member.setPw(rs.getString("pw"));
				member.setName(rs.getString("name"));
				member.setMail(rs.getString("mail"));
				list.add(member);
			}
		} catch (SQLException e) {
			System.out.println("select * 에서 오류발생!! " + e.getMessage());
			e.printStackTrace();
		} finally {
			DbConnClose.resourceClose(rs, pstmt, conn);
		}
		return list;
	}
	
}
