package mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.MemberVO;
import mvc.service.MemberService;
import mvc.util.HttpUtil;

public class MemberInsertController implements Controller {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("MemberInsertController");
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		MemberVO member = new MemberVO();
		
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = request.getParameter("name");
		String mail = request.getParameter("mail");
		
		member.setId(id);
		member.setPw(pw);
		member.setName(name);
		member.setMail(mail);
		
		if(id.isEmpty() || pw.isEmpty()|| name.isEmpty() || mail.isEmpty()) {
			String errMsg = "모든 항목을 입력하세요!";
			request.setAttribute("error", errMsg);
			
			String path="/mvc/memberInsert.jsp";
			HttpUtil.forward(request, response, path);
		}
		
		MemberService service =  MemberService.getInstance();
		service.memberInsert(member);
		out.println("회원정보가 입력되었습니다!");
		
		try {
			out = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		request.setAttribute("id", id);
		String path="/mvc/memberInsertResult.jsp";
		HttpUtil.forward(request, response, path);
//		out.println("<script>");
//		out.println(" alert('회원등록 되었습니다!!');");
//		out.println(" location.href("+path+");");
//		out.println("</script>");
	}
	
	

}
