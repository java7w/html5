package mvc.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.MemberVO;
import mvc.service.MemberService;
import mvc.util.HttpUtil;

public class MemberSearchController implements Controller {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) {
		//id와 같은 member검색
		//member를 request.setAttriute()를 사용해서 저장
		//출력화면을 호출해서 member를 꺼내서 출력하기
		
		String id = request.getParameter("id");
		String job = request.getParameter("job");
		
		String path = null;
		
		String errMsg = null;
		PrintWriter out = null;
		try {
			out = response.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		if(id.isEmpty()) {
			errMsg = "아이디를 입력하세요!";
			request.setAttribute("error", errMsg);
			if(job.equals("search")) {
				path = "/mvc/memberSearch.jsp";
			}else if(job.equals("update")) {
				path = "/mvc/memberUpdate.jsp";
			}
			HttpUtil.forward(request, response, path);
//			out.println("<script>");
//			out.println("alert("+errMsg+");");
//			out.println(" history.back();");
//			out.println("</script>");
			return;
		}
		 MemberService service = MemberService.getInstance();
		 MemberVO member = service.memberSearch(id);
		 
		 if(member==null) {
			 request.setAttribute("result", "검색된 회원정보가 없습니다!");
		 }else {
			 request.setAttribute("result", null);
			 request.setAttribute("member", member);
		 }
		 if(job.equals("search")) {
			 path="/mvc/memberSearchResult.jsp";
			 HttpUtil.forward(request, response, path);
		 } else if(job.equals("update")) {
			 path="/mvc/memberUpdate.jsp";
			 HttpUtil.forward(request, response, path);
		 
		} else if(job.equals("delete")) {
			path="/mvc/memberDelete.jsp";
			HttpUtil.forward(request, response, path);
		}
		 
	}

}
