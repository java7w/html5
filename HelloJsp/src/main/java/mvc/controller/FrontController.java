package mvc.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FrontController
 */
//@WebServlet("/FrontController")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	String charset= null;
	HashMap<String, Controller> list = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FrontController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		charset = config.getInitParameter("charset");
		list = new HashMap<String, Controller>();
		list.put("/mvc/memberInsert.do", new MemberInsertController());
		list.put("/mvc/memberList.do", new MemberListController());
		list.put("/mvc/memberSearch.do", new MemberSearchController());
		list.put("/mvc/memberUpdate.do", new MemberUpdateController());
		list.put("/mvc/memberDelete.do", new MemberDeleteController());
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		request.setCharacterEncoding(charset);
		
		String url = request.getRequestURI();
		//   /HelloJsp/mvc/mvcindex.jsp
		String contextPath = request.getContextPath();
		//   /HelloJsp
		String path = url.substring(contextPath.length());
		
		Controller subController = list.get(path);
		subController.execute(request, response);
		
	}

}
