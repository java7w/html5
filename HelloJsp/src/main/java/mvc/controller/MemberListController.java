package mvc.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.MemberVO;
import mvc.service.MemberService;
import mvc.util.HttpUtil;

public class MemberListController implements Controller {

	@Override
	public void execute(HttpServletRequest request
			, HttpServletResponse response) {
		MemberService service = MemberService.getInstance();
		ArrayList<MemberVO> list = null;
		list = service.memberList();
		
		request.setAttribute("list", list);
		
		String path = "/mvc/memberListResult.jsp";
		
		HttpUtil.forward(request, response, path);
	}

}
