package mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.service.MemberService;
import mvc.util.HttpUtil;

public class MemberDeleteController implements Controller {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) {
		String id = request.getParameter("id");
		
		MemberService service = MemberService.getInstance();
		service.memberDelete(id);
		
		String path = "/mvc/memberDeleteResult.jsp";
		HttpUtil.forward(request, response, path);
	}

}
