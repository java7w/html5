package mvc.service;

import java.util.ArrayList;

import mvc.MemberVO;
import mvc.dao.MemberDao;

public class MemberService {
	private static MemberService service = new MemberService();
	public MemberDao dao = MemberDao.getInstance();
	
	public static MemberService getInstance() {
		return service;
	}
	
	public void memberInsert(MemberVO member) {
		dao.memberInsert(member);
	}
	
	public MemberVO memberSearch(String id) {
		MemberVO member = dao.memberSearch(id);
		return member;
	}
	
	public ArrayList<MemberVO> memberSearchName(String name){
		ArrayList<MemberVO> list = null;
		list = dao.memberSearchName(name);
		return list;
	}
	
	public void memberUpdate(MemberVO member) {
		dao.memberUpdate(member);
	}
	
	public void memberDelete(String id) {
		dao.memberDelete(id);
	}
	
	public ArrayList<MemberVO> memberList(){
		ArrayList<MemberVO> list = null;
		list = dao.memberList();
		return list;
	}
}
