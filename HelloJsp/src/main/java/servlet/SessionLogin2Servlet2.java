package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SessionLogin2Servlet
 */
//@WebServlet("/sessionlogin3")
public class SessionLogin2Servlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionLogin2Servlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		request.setCharacterEncoding("euc-kr");
		response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out = response.getWriter();
		
		String id = request.getParameter("id");
		String pwd = request.getParameter("pwd");
		out.print("<script>");
		out.print(" alert('doGet이 호출되었습니다!');");
		//out.print(" history.back();");
		out.print("</script>");
		if(id.equals("id01")&& pwd.equals("pw")) {//login exactly!!
			out.print("<script>");
			out.print(" alert('로그인성공!!');");
			out.print(" history.back();");
			out.print("</script>");
		}else {
			out.print("<script>");
			out.print(" alert('로그인실패!!');");
			out.print(" history.back();");
			out.print("</script>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=euc-kr");
		request.setCharacterEncoding("euc-kr");
		PrintWriter out = response.getWriter();
		out.print("<script>");
		out.print(" alert('doPost가 호출되었습니다!');");
		out.print(" history.back();");
		out.print("</script>");
	}
}
