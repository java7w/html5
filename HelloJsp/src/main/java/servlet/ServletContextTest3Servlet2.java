package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/context6")
public class ServletContextTest3Servlet2 extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/html;charset=euc-kr");
		PrintWriter out = resp.getWriter();

		ServletContext sc = this.getServletContext();

		out.print("서블릿 버젼 :" + sc.getMajorVersion() + "." + sc.getMinorVersion());
		out.print("<br>서버 정보 : " + sc.getServerInfo());
		out.print("<br>웹어플리케이션 경로 : " + sc.getContextPath());
		out.print("<br>웹어플리케이션 이름 : " + sc.getServletContextName());
		out.print("<br>파일 실제 경로 : " + sc.getRealPath("/name.html"));
		
		out.println("<hr>");
		
		ShareObject obj1 = (ShareObject) sc.getAttribute("person1");
		if(obj1==null) {
			out.println("person1 not found! <br>");	
		}else {
			out.println("person1: "+obj1.toString()+"<br>");
			out.println("  name: "+obj1.getName()+"<br>");
			out.println("  age: "+obj1.getAge()+"<br>");
		}
		ShareObject obj2 = (ShareObject) sc.getAttribute("person2");
		if(obj2==null) {
			out.println("person1 not found! <br>");	
		}else {
			out.println("person2: "+obj2.toString()+"<br>");
			out.println("  name: "+obj2.getName()+"<br>");
			out.println("  age: "+obj2.getAge()+"<br>");
		}
		sc.log("로그 기록: 객체출력 완료!!");
		out.close();
	}
}
