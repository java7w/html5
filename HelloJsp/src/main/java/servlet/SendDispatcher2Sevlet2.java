package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SendDispatcherSevlet
 */
@WebServlet("/bookinfo")
public class SendDispatcher2Sevlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendDispatcher2Sevlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		request.setCharacterEncoding("euc-kr");
		PrintWriter out = response.getWriter();
		HttpSession session = null;
		ServletContext sc =  this.getServletContext();
		response.getWriter().append("Served at: ").append(request.getContextPath());
		out.println("<hr>");
		out.println("<h3>book객체내용 출력!, /bookinfo</h3");
		
		Book book = (Book) request.getAttribute("book");
		
		out.println("<h3>책제목: "+ book.getTitle()+"</h3>");
		out.println("<h3>책저자: "+book.getAuthor()+"</h3>");
		out.println("<h3>출판사: "+book.getPublisher()+"</h3>");
		sc.log("[로그 출력] /bookinfo 처리!");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		//request.setCharacterEncoding("euc-kr");
		doGet(request, response);
	}

}
