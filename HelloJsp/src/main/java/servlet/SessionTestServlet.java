package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SessionTestServlet
 */
@WebServlet("/session1")
public class SessionTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionTestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ServletContext sc =  this.getServletContext();
		response.setContentType("text/html;charset=euc-kr");
		PrintWriter out = response.getWriter();
		String param = request.getParameter("p");
		String msg = null;
		HttpSession session = null;
		response.getWriter().append("Served at: ").append(request.getContextPath());
		out.println("<hr>");
		
		if(param.equals("create")) {
			out.println("p=create <br>");
			session = request.getSession();
			if(session.isNew()) {
				msg = "세션을 새로 생성합니다!";
			}else {
				msg = "기존 세션을 리턴합니다!";
			}
		}else if(param.equals("delete")) {
			out.println("p=delete <br>");
			session = request.getSession(false);
			if(session!=null) {
				session.invalidate();
				msg = "기존 세션을 삭제합니다!";
			}else {
				msg = "삭제할 세션이 없습니다!";	
			}
		}else if(param.equals("add")) {
			out.println("p=add <br>");
			session = request.getSession(true);
			String name = "hong1";
			String age = "20";
			session.setAttribute("name", name);
			session.setAttribute("age", age);
			msg = "세션에 속성 데이터를 저장: name:"+name+"  age:"+age;
		}else if(param.equals("get")) {
			out.println("p=get <br>");
			session = request.getSession(false);
			if(session !=null) {
				String name = (String) session.getAttribute("name");
				String age = (String) session.getAttribute("age");
				msg = "세션에서 추출한 데이터: name:"+name+"  age:"+age;
			}else{
				msg = "가져올 세션 객체가 없습니다!";
			}
		}else if(param.equals("remove")) {
			out.println("p=remove <br>");
			session = request.getSession(false);
			if(session !=null) {
				session.removeAttribute("name");
				session.removeAttribute("age");
				msg = "세션에서 name, age 의 속성을 삭제합니다!";
			}else {
				msg = "삭제할 세션 객체가 없습니다!";
			}
			
		}else if(param.equals("replace")) {
			out.println("p=replace <br>");
			session = request.getSession();
			session.setAttribute("name", "hong2");
			session.setAttribute("age", "100");
			msg = "세션에서 name, age 속성을 새로운 값으로 등록합니다!";
		}
		
		out.println("[세션처리] "+msg);
		sc.log("[세션처리] "+msg);
		
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
