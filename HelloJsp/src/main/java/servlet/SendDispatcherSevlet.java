package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SendDispatcherSevlet
 */
@WebServlet("/dispatcher1")
public class SendDispatcherSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendDispatcherSevlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		PrintWriter out = response.getWriter();
		HttpSession session = null;
		ServletContext sc =  this.getServletContext();
		response.getWriter().append("Served at: ").append(request.getContextPath());
		out.println("<hr>");
		
		out.println("<h3>dispatcher 요청1, /dispatcher1 </h3>");
		RequestDispatcher rd = sc.getRequestDispatcher("/dispatcher2");
		rd.include(request, response);
		//rd.forward(request, response);
		sc.log("[로그 출력] /dispatcher1 ");
		sc.log("[로그 출력] rd.forward(/dispatcher2) ");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
