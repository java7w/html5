package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/context5")
public class ServletContextTest3Servlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/html;charset=euc-kr");
		PrintWriter out = resp.getWriter();

		ServletContext sc = this.getServletContext();

		out.print("서블릿 버젼 :" + sc.getMajorVersion() + "." + sc.getMinorVersion());
		out.print("<br>서버 정보 : " + sc.getServerInfo());
		out.print("<br>웹어플리케이션 경로 : " + sc.getContextPath());
		out.print("<br>웹어플리케이션 이름 : " + sc.getServletContextName());
		out.print("<br>파일 실제 경로 : " + sc.getRealPath("/name.html"));
		
		out.println("<hr>");
		ShareObject obj1 = new ShareObject();
		obj1.setAge(100);
		obj1.setName("hong1");
		sc.setAttribute("person1", obj1);
		
		ShareObject obj2 = new ShareObject();
		obj2.setAge(20);
		obj2.setName("hong2");
		sc.setAttribute("person2", obj2);
		out.println("ServletContext 객체:ShareObject데이터(person1, person2)를 등록했습니다.");
		
		sc.log("로그 기록: 객체등록 완료!!");
		out.close();
	}
}
