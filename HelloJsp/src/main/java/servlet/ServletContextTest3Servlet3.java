package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/context7")
public class ServletContextTest3Servlet3 extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/html;charset=euc-kr");
		PrintWriter out = resp.getWriter();

		ServletContext sc = this.getServletContext();

		out.print("서블릿 버젼 :" + sc.getMajorVersion() + "." + sc.getMinorVersion());
		out.print("<br>서버 정보 : " + sc.getServerInfo());
		out.print("<br>웹어플리케이션 경로 : " + sc.getContextPath());
		out.print("<br>웹어플리케이션 이름 : " + sc.getServletContextName());
		out.print("<br>파일 실제 경로 : " + sc.getRealPath("/name.html"));
		
		out.println("<hr>");
		
		sc.removeAttribute("person1");
		out.println("person1 삭제");
		sc.log("로그 기록: 객체삭제 완료!!");
		out.close();
	}
}
