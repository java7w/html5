package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SendDispatcherSevlet
 */
@WebServlet("/bookreg")
public class SendDispatcher2Sevlet1 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendDispatcher2Sevlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		request.setCharacterEncoding("euc-kr");
		PrintWriter out = response.getWriter();
		HttpSession session = null;
		ServletContext sc =  this.getServletContext();
		response.getWriter().append("Served at: ").append(request.getContextPath());
		out.println("<hr>");
		
		String title = request.getParameter("title");
		String author = request.getParameter("author");
		String publisher = request.getParameter("publisher");
		System.getProperty("file.encoding","euc-kr");
		
		out.println("<h3>book객체에 저장!, /bookreg</h3");
		Book book = new Book();
		book.setAuthor(author);
		book.setPublisher(publisher);
		book.setTitle(title);
		
		request.setAttribute("book", book);
		RequestDispatcher rd = request.getRequestDispatcher("/bookinfo");
		rd.forward(request, response);
		
		sc.log("[로그 출력] /bookreg 처리!");
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		//request.setCharacterEncoding("euc-kr");
		doGet(request, response);
	}

}
