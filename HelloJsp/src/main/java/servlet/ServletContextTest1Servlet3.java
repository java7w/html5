package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/context3")
public class ServletContextTest1Servlet3  extends HttpServlet{
	ServletContext sc;
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=euc-kr");
		PrintWriter out = resp.getWriter();
		out.print("Context: "+sc);
		
		String location = sc.getInitParameter("contextConfig");
		out.println("<br>");
		out.println("location: "+location);

		location = sc.getInitParameter("contextConfigLocation");
		out.println("<br>");
		out.println("location: "+location);
		
		location = sc.getInitParameter("persistentConfigLocation");
		out.println("<br>");
		out.println("location: "+location);
		
		
		out.close();
	
	}
}



//ServletContext sc = this.getServletContext();
//String location  = sc.getInitParameter("contextConfig");
//      out.print("location : "+ location);
//	out.close();
