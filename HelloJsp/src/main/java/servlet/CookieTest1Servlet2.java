package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CookieTest1Servlet
 */
@WebServlet("/cookie2")
public class CookieTest1Servlet2 extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CookieTest1Servlet2() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		PrintWriter out = response.getWriter();
		
		ServletContext sc = this.getServletContext();
		out.println("<hr>");
		
		Cookie[] list = request.getCookies();
		out.println("*쿠키 정보 출력: <br>");
		for(int i=0;list!=null&&i<list.length;i++) {
			out.println(list[i].getName()+":"+list[i].getValue()+"<br>");
		}
				
		out.println("쿠키 조회 완료!!");
		sc.log("로그 기록: 쿠키:[id, code, subject] 조회");
		for(int i=0;list!=null&&i<list.length;i++) {
			sc.log(list[i].getName()+":"+list[i].getValue());
		}
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
