package servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SessionLoginServlet
 */
@WebServlet("/sessionlogin")
public class SessionLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html;charset=euc-kr");
		PrintWriter out = response.getWriter();
		HttpSession session = null;
		ServletContext sc =  this.getServletContext();
		response.getWriter().append("Served at: ").append(request.getContextPath());
		out.println("<hr>");
		
		session = request.getSession(false);
		if(session!=null && session.getAttribute("id")!=null) {
			session.invalidate();
			out.print("<script>");
			out.print(" alert('로그아웃하였습니다!');");
			out.print(" history.back();");
			out.print("</script>");
		}else {
			out.print("<script>");
			out.print(" alert('로그인상태가 아닙니다! <br> 로그인을 하세요!');");
			out.print(" history.back();");
			out.print("</script>");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html;charset=euc-kr");
		PrintWriter out = response.getWriter();
		HttpSession session = null;
		ServletContext sc =  this.getServletContext();
		response.getWriter().append("Served at: ").append(request.getContextPath());
		out.println("<hr>");
		
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String name = "hong1";
		
		if(id.isEmpty()||pw.isEmpty()) {
			out.print("<script>");
			out.print(" alert('id 또는 비밀번호를 입력해주세요!');");
			
			out.print(" history.back();");
			out.print("</script>");
			return;
		}
		session = request.getSession();
		if(session.isNew()||session.getAttribute("id")==null) {
			session.setAttribute("id", id);
			session.setAttribute("name", name);
			out.println(name+"님 로그인하였습니다!");
		}else {
			out.println(name+"님 로그인 상태입니다!");
		}
	}

}
