package servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/context2")
public class ServletContextTest1Servlet2  extends HttpServlet{
	ServletContext sc;
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=euc-kr");
		PrintWriter out = resp.getWriter();
		out.print("Context: "+sc);
		out.close();
	
	}
}



//ServletContext sc = this.getServletContext();
//String location  = sc.getInitParameter("contextConfig");
//      out.print("location : "+ location);
//	out.close();
