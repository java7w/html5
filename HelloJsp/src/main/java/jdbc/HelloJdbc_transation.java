package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class HelloJdbc_transation {

	static final String url = "jdbc:mariadb://localhost/testdb";
	static final String id = "dev";
	static final String pass = "pass";
	static String sql = "SELECT id, first, last, age FROM Employees";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello Jdbc transaction test!!");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Savepoint sp1 = null;
		Savepoint sp2 = null;
		try {
			conn = DriverManager.getConnection(url, id, pass);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);

			conn.setAutoCommit(false);
			sp1 = conn.setSavepoint("MysavePoint");
			System.out.println("__savepoint sp1 저장__");
			
			sql = "insert into employees values(110, 20, 'hong', 'mari')";
			stmt.executeUpdate(sql);
			System.out.println("__insert 110번 hong 입력 처리 완료__");
			//throw new SQLException("110번 입사지 입력완료!");
			
			sp2 = conn.setSavepoint("MysavePoint2");
			System.out.println("__savepoint sp1 저장__");
			
			sql = "insert in employees values( 111, 22, 'hwang', 'bori')";
			stmt.executeUpdate(sql);
			System.out.println("__insert hwang 입력 처리 완료__");
			
			System.out.println("__commit__");
			conn.commit();
			
		} catch (SQLException e) {
			System.out.println("__SQLException__");
			// TODO Auto-generated catch block
			try {
				System.out.println("__rollback(sp1)__");
				conn.rollback(sp1);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			e.printStackTrace();
		} finally {

		}

		System.out.println("--- select 처리 완료 ---");
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("___rs, stmt, conn 객체 제거 완료___");
	}
}
