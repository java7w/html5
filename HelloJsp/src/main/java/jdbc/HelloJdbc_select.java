package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HelloJdbc_select {

	static final String url = "jdbc:mariadb://localhost/testdb";
	static final String id = "dev";
	static final String pass = "pass";
	static final String sql = "SELECT id, first, last, age FROM Employees";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello Jdbc!!");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url, id, pass);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
		            // Retrieve by column name
				System.out.print("id:"+ rs.getInt("id"));
				System.out.print(", age: " + rs.getInt("age"));
				System.out.print(", first: " + rs.getString("first"));
				System.out.println(", last: " + rs.getString("last"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
		}
		
		System.out.println("--- select 처리 완료 ---");
		if(rs!=null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(stmt!=null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(conn!=null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("___rs, stmt, conn 객체 제거 완료___");
	}
}
