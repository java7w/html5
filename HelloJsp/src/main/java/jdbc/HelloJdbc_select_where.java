package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HelloJdbc_select_where {

	static final String url = "jdbc:mariadb://localhost/testdb";
	static final String id = "dev";
	static final String pass = "pass";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello Jdbc!!");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		
		try {
			conn = DriverManager.getConnection(url, id, pass);
			stmt = conn.createStatement();
						
			//sql = "SELECT * FROM Employees where age > '25'";
			sql = "select * from employees";
			rs = stmt.executeQuery(sql);
			System.out.printf("\t%s\t%s\t%s\t%s\n", "id", "age", "first", "last");
			while(rs.next()) {
				System.out.printf("\t%s\t%s\t%s\t%s\n"
						, rs.getInt("id"), rs.getInt("age")
						, rs.getString("first"), rs.getString("last"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
		}
		
		System.out.println("--- select 처리 완료 ---");
		if(rs!=null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(stmt!=null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(conn!=null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("___rs, stmt, conn 객체 제거 완료___");
	}
}
