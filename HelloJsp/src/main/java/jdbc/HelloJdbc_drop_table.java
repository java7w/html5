package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class HelloJdbc_drop_table {

	static final String url = "jdbc:mariadb://localhost/testdb";
	static final String id = "dev";
	static final String pass = "pass";
	static String sql = "drop TABLE REGISTRATION";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello Jdbc transaction test!!");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Savepoint sp1 = null;
		Savepoint sp2 = null;
		try {
			conn = DriverManager.getConnection(url, id, pass);
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}

		System.out.println("___처리 완료___");
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("___rs, stmt, conn 객체 제거 완료___");
	}
}
