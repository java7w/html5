package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class HelloJdbc_connect {

	static final String url = "jdbc:mariadb://localhost/testdb";
	static final String id = "dev";
	static final String pass = "pass";
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello Jdbc transaction test!!");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			conn = DriverManager.getConnection(url, id, pass);
			System.out.println("__database에 연결되었습니다__");			
		} catch (SQLException e) {
			System.out.println("__database에 연결중 오류가발생하였습니다__");
			e.printStackTrace();
		} finally {

		}

		System.out.println("___처리 완료___");
		
		System.out.println("___리소스를 닫습니다(result, statement, connection)___");
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("___rs, stmt, conn 객체 제거 완료___");
	}
}
