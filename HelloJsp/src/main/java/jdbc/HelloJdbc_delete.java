package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class HelloJdbc_delete {

	static final String url = "jdbc:mariadb://localhost/testdb";
	static final String id = "dev";
	static final String pass = "pass";
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello Jdbc delete test!!");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		String sql1 = "delete from employees";
		String sql2 = "INSERT INTO Employees VALUES (100, 20, 'hong', 'gildong')";
		String sql3 = "INSERT INTO Employees VALUES (101, 21, 'go', 'gildong');";
		try {
			conn = DriverManager.getConnection(url, id, pass);
			System.out.println("__database에 연결되었습니다__");
			stmt = conn.createStatement();
			stmt.executeUpdate(sql1);
			stmt.executeUpdate(sql2);
			stmt.executeUpdate(sql3);
			
			System.out.println("__delete 하기 전:__");
			sql = "select * from employees";
			rs = stmt.executeQuery(sql);
			System.out.printf("\t%s\t%s\t%s\t%s\n", "id", "age", "first", "last");
			while(rs.next()) {
				System.out.printf("\t%s\t%s\t%s\t%s\n"
						, rs.getInt("id"), rs.getInt("age")
						, rs.getString("first")
						, rs.getString("last"));
			}
			
			System.out.println("__delete (id = 101) from employees...__");
			sql =  "DELETE FROM employees " +
		            "WHERE id = 101";
			stmt.executeUpdate(sql) ;
			System.out.println("__delete 한 후:__");
			sql = "select * from employees";
			rs = stmt.executeQuery(sql);
			System.out.printf("\t%s\t%s\t%s\t%s\n", "id", "age", "first", "last");
			while(rs.next()) {
				System.out.printf("\t%s\t%s\t%s\t%s\n"
						, rs.getInt("id"), rs.getInt("age")
						, rs.getString("first")
						, rs.getString("last"));
			}
		} catch (SQLException e) {
			System.out.println("__database에 연결중 오류가발생하였습니다__");
			e.printStackTrace();
		} finally {

		}

		System.out.println("___처리 완료___");
		
		System.out.println("___리소스를 닫습니다(result, statement, connection)___");
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("___rs, stmt, conn 객체 제거 완료___");
	}
}
