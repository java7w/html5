package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class HelloJdbc_update {

	static final String url = "jdbc:mariadb://localhost/testdb";
	static final String id = "dev";
	static final String pass = "pass";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Hello Jdbc transaction test!!");

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		Savepoint sp1 = null;
		Savepoint sp2 = null;
		String sql;// = "UPDATE Employees SET age = 30 WHERE id in (100, 101, 103)";
		try {
			conn = DriverManager.getConnection(url, id, pass);
			stmt = conn.createStatement();
			sql = "select * from employees";
			rs = stmt.executeQuery(sql);
			System.out.println("__update 하기 전:__");
			System.out.printf("\t%s\t%s\t%s\t%s\n", "id", "age", "first", "last");
			while(rs.next()) {
				System.out.printf("\t%s\t%s\t%s\t%s\n"
						, rs.getInt("id"), rs.getInt("age")
						, rs.getString("first"), rs.getString("last"));
			}
			System.out.println("__update...__");
			sql = "UPDATE Employees SET age = 30 WHERE id in (100, 101, 103)";
			stmt.executeUpdate(sql);
			
			sql = "select * from employees";
			rs = stmt.executeQuery(sql);
			System.out.println("__update 한 후:__");
			System.out.printf("\t%s\t%s\t%s\t%s\n", "id", "age", "first", "last");
			while(rs.next()) {
				System.out.printf("\t%s\t%s\t%s\t%s\n"
						, rs.getInt("id"), rs.getInt("age")
						, rs.getString("first"), rs.getString("last"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}

		System.out.println("___처리 완료___");
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("___rs, stmt, conn 객체 제거 완료___");
	}
}
