<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>서버와의 연결정보 보관: cookie</title>
</head>
<body>
<h3>cookie create</h3>
<hr>

<%
Cookie cookie = new Cookie("id", "admin");
int expiry = 300;//300sec = 6min
cookie.setMaxAge(expiry);
response.addCookie(cookie);

out.println("쿠키가 생성되었습니다!!!"+"<br>");
%>

<ul>
	<li>쿠키이름: <%=cookie.getName() %>
	<li>쿠키값: <%=cookie.getValue() %>
	<li>쿠키유효시간: <%=cookie.getMaxAge() %>
</ul>
<p><a href="./cookie_check.jsp">[쿠키-확인]</a></p>

</body>
</html>