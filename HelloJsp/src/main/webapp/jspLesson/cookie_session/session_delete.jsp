<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>서버와의 연결정보 보관: session</title>
</head>
<body>
<h3>세션 삭제하기</h3>
<hr>
<h5>세션정보:</h5>
<ul>
<li>세션속성(id): <%=session.getAttribute("id") %>
<li>세션속성(name): <%=session.getAttribute("name") %>
<li>세션 아이디(JSESSIONID): <%=session.getId() %>
<li>세션 생성시간: <%=session.getCreationTime() %>초
<li>세션 유효시간: <%=session.getMaxInactiveInterval() %>초
<li>세션 마지막접속시간: <%=session.getLastAccessedTime() %>초
<li>세션 유지시간: <%=(session.getLastAccessedTime()- session.getCreationTime()) %>초
</ul>
<hr>
<%
session.removeAttribute("id");
session.removeAttribute("name");
%>
<p>세션 객체 속성(id, name)을 삭제했습니다!!<br>
<ul>
<li>세션속성(id): <%=session.getAttribute("id") %>
<li>세션속성(name): <%=session.getAttribute("name") %>
</ul>
<hr>
<%
session.invalidate();
%>
<p>모든 세션 정보를 삭제했습니다!!<br>
<br>
<p><a href="./session_check.jsp">[세션정보확인하기]</a>
</body>
</html>