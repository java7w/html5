<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>jdbc connect : mariadb(mysql) </title>
</head>
<body>
<h3>JDBC: table insert 테스트!!!</h3>
<hr>

<%
String driverClass = "com.mysql.jdbc.Driver";
//String driverClass = "org.mariadb.jdbc.Driver";

try{
	Class.forName(driverClass);
	out.println("jdbc driver loading 성공!!");
}catch(ClassNotFoundException e){
	out.println("jdbc driver loading 실패!! ");
	out.println("web-inf/lib 폴더를 확인하세요! ");
}

String url = "jdbc:mysql://localhost:3306/univ";
//String url = "jdbc:mariadb://localhost:3306/univ";
String id = "dev";
String pw = "pass";

Connection conn = null;


try {
	conn = DriverManager.getConnection(url, id, pw);
	out.println("db연결 성공!!"+"<br>");
	
	
} catch (SQLException e) {
	// TODO Auto-generated catch block
	out.println(e.getMessage()+"<br>");
	out.println("db연결 실패!!"+"<br>");
} finally{
	out.println("db연결 처리 완료!!"+"<br>");
}


String sql = "insert into student values"
  	+"(160001, '한국인', 4, '컴공', '서울'),"
	+"(195712, '조아라', 1, '멀티', '부산'),"
	+"(179752, '홍길동', 3, '전상', '광주'),"
	+"(184682, '나매력', 2, '전상', '제주'),"
	+"(172634, '이천사', 3, '컴공', '광주'),"
	+"(183517, '김보배', 2, '멀티', '전남'),"
	+"(160739, '신입생', 4, '컴공', '광주')";
PreparedStatement pstmt = null;
try {
	pstmt = conn.prepareStatement(sql);
	pstmt.executeUpdate();
	out.println("db table insert 성공!!"+"<br>");
} catch (SQLException e) {
	out.println(e.getMessage()+"<br>");
	out.println("db table insert 실패!!"+"<br>");
} finally{
	if(conn!=null){
		try{
		conn.close();
		out.println("db연결을 종료합니다!!"+"<br>");
		}catch(Exception e){
			out.println(e.getMessage()+"<br>");
		}
	}
}
%>


</body>
</html>