<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>jdbc connect : mariadb(mysql) </title>
</head>
<body>
<h3>JDBC: delete 테스트!!!</h3>
<hr>

<%
String driverClass = "com.mysql.jdbc.Driver/univ";
//String driverClass = "org.mariadb.jdbc.Driver/univ";

try{
	Class.forName(driverClass);
	out.println("jdbc driver loading 성공!!");
}catch(ClassNotFoundException e){
	out.println("jdbc driver loading 실패!! ");
	out.println("web-inf/lib 폴더를 확인하세요! ");
}

//String url = "jdbc:mysql://localhost:3306";
String url = "jdbc:mariadb://localhost:3306";
String id = "dev";
String pw = "pass";

Connection conn = null;


try {
	conn = DriverManager.getConnection(url, id, pw);
	out.println("db연결 성공!!"+"<br>");
	
	
} catch (SQLException e) {
	out.println(e.getMessage()+"<br>");
	out.println("db연결 실패!!"+"<br>");
} finally{
	out.println("db연결 처리 완료!!"+"<br>");
}


String sql = "delete from table student";
PreparedStatement pstmt = null;
try {
	pstmt = conn.prepareStatement(sql);
	pstmt.executeUpdate();
	out.println("db delete table 성공!!"+"<br>");
} catch (SQLException e) {
	// TODO Auto-generated catch block
	out.println(e.getMessage()+"<br>");
	out.println("db delete table 실패!!"+"<br>");
} finally{
	if(conn!=null){
		try{
		conn.close();
		out.println("db연결을 종료합니다!!"+"<br>");
		}catch(Exception e){
			out.println(e.getMessage()+"<br>");
		}
	}
}
%>


</body>
</html>