<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>jdbc connect: mariadb(mysql) </title>
<link rel="stylesheet" href="../common/CSS/student_insert.css">

<style>
  table {width:400px; border-collapse:collapse; border:2px solid black;}
  caption {font-size:20pt; font-weight:bold;}
  th, td {border:1px solid grey; padding:3px;}
  th {width:25%; background-color:#CFD0ED; text-align:left;}
  td {background-color:#FAFAEE; text-align:left;}
</style>
</head>
<body>
<h3>JDBC: table insert form!!!</h3>
<hr>

<form method="post" name="student_form" action="./jdbc_table_insert_proc.jsp">
<table>
<caption>학생정보 입력폼</caption>
<tr>
<th>학번</th>
<td><input name="hakbun" type="text" size="10" 
	required></td>
</tr>
<tr>
<th>이름</th>
<td><input name="name" type="text" size="10" maxlength="5" 
	required></td>
</tr>
<tr>
<th>학년</th>
<td><input name="year" type="radio" value="1" required>1학년
	<input name="year" type="radio" value="2" required>2학년
	<input name="year" type="radio" value="3" required>3학년
	<input name="year" type="radio" value="4" required>4학년
</td>
</tr>
<tr>
<th>학과</th>
<td><input name="dept" type="text" size="10" maxlength="10"
	required></td>
</tr>
<tr>
<th>주소</th>
<td><input name="addr" type="text" size="40" maxlength=40" 
	required></td>
</tr>
<tr>
<td colspan="2" style="text-align:center;">
 	<input type="submit" value="전송">
	<input type="reset" value="취소">
</td>
</tr>
</table>

</form>
</body>
</html>