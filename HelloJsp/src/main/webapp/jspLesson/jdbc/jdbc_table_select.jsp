<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>jdbc connect > select : mariadb(mysql) </title>
</head>
<body>
<h3>DB데이터 조회(select)!!!</h3>
<hr>

<%
String driverClass = "com.mysql.jdbc.Driver";
//String driverClass = "org.mariadb.jdbc.Driver";

try{
Class.forName(driverClass);
out.println("jdbc driver loading 성공!!");
}catch(ClassNotFoundException e){
	out.println("jdbc driver loading 실패!! ");
	out.println("web-inf/lib 폴더를 확인하세요! ");
}

String url = "jdbc:mysql://localhost:3306/univ";
//String url = "jdbc:mariadb://localhost:3306";
String id = "dev";
String pw = "pass";

Connection conn = null;
PreparedStatement pstmt = null;
ResultSet rs = null;
String sql = "select * from student order by hakbun asc";
try {
	conn = DriverManager.getConnection(url, id, pw);
	out.println("db연결 성공!!"+"<br>");
	
	pstmt = conn.prepareStatement(sql);
	rs = pstmt.executeQuery();
 	int i=0;
 	out.println("<br>");
	while(rs.next()){
		String hakbun = rs.getString("hakbun");
		String name = rs.getString("name");
		String year = rs.getString("year");
		year +="학년";
		String dept = rs.getString("dept");
		String addr = rs.getString("addr");
		
		out.println((++i)+": " + hakbun+","+name+","+year+","+name+","+dept+","+addr +"<br>");
	}
} catch (SQLException e) {
	// TODO Auto-generated catch block
	out.println(e.getMessage()+"<br>");
	out.println("db연결 실패!!"+"<br>");
} finally{
	if(conn!=null){
		try{
		conn.close();
		out.println("db연결을 종료합니다!!"+"<br>");
		}catch(Exception e){
			out.println(e.getMessage()+"<br>");
		}
	}
}

%>


</body>
</html>