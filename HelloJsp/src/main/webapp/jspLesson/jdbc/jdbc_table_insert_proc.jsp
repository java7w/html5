<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>jdbc connect : mariadb(mysql) </title>
</head>
<body>
<h3>JDBC: table insert 테스트!!!</h3>
<hr>

<%
String driverClass = "com.mysql.jdbc.Driver";
//String driverClass = "org.mariadb.jdbc.Driver";

try{
	Class.forName(driverClass);
	out.println("jdbc driver loading 성공!!");
}catch(ClassNotFoundException e){
	out.println("jdbc driver loading 실패!! ");
	out.println("web-inf/lib 폴더를 확인하세요! ");
}

String url = "jdbc:mysql://localhost:3306/univ";
//String url = "jdbc:mariadb://localhost:3306/univ";
String id = "dev";
String pw = "pass";

Connection conn = null;


try {
	conn = DriverManager.getConnection(url, id, pw);
	out.println("db연결 성공!!"+"<br>");
	
	
} catch (SQLException e) {
	// TODO Auto-generated catch block
	out.println(e.getMessage()+"<br>");
	out.println("db연결 실패!!"+"<br>");
} finally{
	out.println("db연결 처리 완료!!"+"<br>");
}

// form에서 입력한 학생 1명에 대한 전송 데이터
String hakbun = request.getParameter("hakbun");
String name = request.getParameter("name");
String year = request.getParameter("year");
String dept = request.getParameter("dept");
String addr = request.getParameter("addr");

// 전송받아온 데이터를 입력하는 곳은 ?으로 자리를 비워둔다!
String sql = "insert into student values"
  	+"(?, ?, ?, ?, ?)";
PreparedStatement pstmt = null;
try {
	pstmt = conn.prepareStatement(sql);
	
	//sql구문의 ?의 빈자리를 form에서 받은 값으로 채워넣는다!!
	pstmt.setString(1, hakbun);
	pstmt.setString(2, name);
	pstmt.setString(3, year);
	pstmt.setString(4, dept);
	pstmt.setString(5, addr);
			
	pstmt.executeUpdate();
	out.println("db table insert 성공!!"+"<br>");
} catch (SQLException e) {
	out.println(e.getMessage()+"<br>");
	out.println("db table insert 실패!!"+"<br>");
} finally{
	if(conn!=null){
		try{
		conn.close();
		out.println("db연결을 종료합니다!!"+"<br>");
		}catch(Exception e){
			out.println(e.getMessage()+"<br>");
		}
	}
}
%>
<%
response.sendRedirect("./jdbc_table_select_view.jsp");
%>
<a href="./jdbc_table_insert_form.jsp">[학생정보 입력폼]</a>
<a href="./jdbc_table_select_view.jsp">[전체학생정보 조회폼]</a>
</body>
</html>