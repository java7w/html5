<%@page import="dbconnclose.DbConnClose"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    
<%
request.setCharacterEncoding("utf-8");
%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>로그인체크 및 세션설정</title>
</head>
<body>


<%
//DB저장되어 있는 정보를 가정....
Connection conn = null;
PreparedStatement pstmt = null;
ResultSet rs = null;
int nErrorCode = 0;
//로그인했을 때 저장되어 있는 session정보....
String cust_id = request.getParameter("cust_id");
String cust_pw = request.getParameter("cust_pw");

conn = DbConnClose.getConnection();

String sql = "select * from customer where cust_id=?";
pstmt = conn.prepareStatement(sql);
pstmt.setString(1, cust_id);
rs = pstmt.executeQuery();
boolean isLoginFail = false;
if(!rs.isBeforeFirst()){
	isLoginFail = true;
	nErrorCode = 101;
}

String db_cust_id=null;
String db_cust_pw=null;
String db_cust_name=null;
if(rs.next()){
	db_cust_id = rs.getString("cust_id");
	db_cust_pw = rs.getString("cust_pw");
	db_cust_name = rs.getString("cust_name");
}

String user_id = "root";
String user_pw = "1234";
String user_name = "관리자";
if(cust_id.isEmpty()||cust_pw.isEmpty()){
	isLoginFail = true;
	nErrorCode = 103;
}
if(isLoginFail){
	out.print("<script>");
	out.print("  alert('아이디와 비밀번호를 입력하세요!"+nErrorCode+"');");
	out.print("  history.back();");
	out.print("</script>");
	// response.sendRedirect("./login_signon.jsp");
	//out.print("<p><a href='#''>[회원로그폼]</a></p>");
}

//로그인 체크: 로그인 입력정보 == 데이이터베이스의 정보
if(cust_id.equals(db_cust_id) && cust_pw.equals(db_cust_pw)){
	//같다! -> 로그인 -> 로그인 id, pwd, name ->세션에 저장하기
	out.print("<h3>로그인 성공!!</h3");
	//세션저장하기...
	session.setAttribute("cust_id", cust_id);
	session.setAttribute("cust_pw", cust_pw);
	session.setAttribute("cust_name", db_cust_name);
	out.print("<h3>"+user_name+"님이 로그인했습니다!!</h3");
	// response.sendRedirect("./login_mypage.jsp");
	out.print("<p><a href='#'>[마이페이지(개별)]</a></p>");
}else{
	//다르다! -> 로그인하지 못하고-> 로그인 다시 입력하거나, 회원가입페이지이동하거나.
	out.print("<h3>로그인 실패!!</h3");
	out.print("<h3>회원가입을 한 후에 이용해주세요!!</h3");
	// response.sendRedirect("./login_signup.jsp");
	out.print("<p><a href='#'>[회원가입폼]</a></p>");
}

%>
<%
// 즉시 url을 이동하도록 명령을 작성합니다.
// response.sendRedirect("./login_form.jsp");
%>
<hr>
<p><a href="./login_form.jsp">[로그인폼]</a></p>
<p><a href="#">[회원가입폼]</a></p>
<p><a href="#">[마이페이지(개별)]</a></p>
</body>
</html>