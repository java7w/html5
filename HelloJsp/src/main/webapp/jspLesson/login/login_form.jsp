<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
	
<%
String cust_id = (String)session.getAttribute("cust_id");
String cust_pw = (String)session.getAttribute("cust_pw");
String cust_name = (String)session.getAttribute("cust_name");
Boolean login = false;

if((cust_id!=null)&&(cust_pw!=null)){ //세션에 로그인 아이디를 저장하고 있으면...
	login=true; //로그인한 상태라고 판단-
}
%>	
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>로그인:session사용+화면이동</title>
<style type="text/css">
	table{
		width:250px;
		text-align:center;
		border-collapse:collapse;
	}
	caption{
		font-size:20pt;
		font-weight:bold;
	}
	th, td{
		border:1px solid grey;
		padding:2px;
	}
	th{
		width:40%;
		background-color:#CFD0ED;
	}
	td{
		text-align:left;
	}
	
</style>
</head>
<body>
	<h2>로그인화면만들기</h2>
	<hr>
	<form action="./login_check2.jsp">
		<table>
			<caption>로그인폼</caption>
			<tr>
				<th>아이디</th>
				<td><input name="cust_id" type="text" size="12" maxlength="10"
					required autofocus></td>
			</tr>
			<tr>
				<th>패스워드</th>
				<td><input name="cust_pw" type="password" size="12"
					maxlength="10" required></td>
			</tr>
<%
if(login==false){
%>

<!-- 			로그인하지않았을 때 -->
			
			<tr>
				<td colspan="2" style="text-align:center">
				
					<input type="submit" value="로그인"> 
					<input type="button" value="로그아웃" disabled>
				</td>
			</tr>
<%
}else{
%>
<!-- 			로그인했을을 때 -->
			<tr>
				<td colspan="2" style="text-align:center">
					<input type="submit" value="로그인" disabled> 
					<input type="button" value="로그아웃" 
						onClick=location.href="./logout.jsp" 
						style="cursor:pointer;">
				</td>
			</tr>
<%} %>			
		</table>
	</form>

</body>
</html>