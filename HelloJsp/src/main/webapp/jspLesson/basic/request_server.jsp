<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>

<!DOCTYPE html>
<html>
<head>
 <meta charset="euc-kr">
 <title>request 내장 객체(request_server.jsp)</title>
</head>
<body>
 <b>request 내장 객체 - [서버관련 정보]</b><Br>
 서버 IP 주소 : <%= request.getLocalAddr()%><Br>
 호스트  이름 : <%= request.getLocalName()%><Br>
 서버 포트 : <%= request.getLocalPort()%><Br>
 서버 포트 : <%= request.getServerPort()%><Br>
 서버 이름 : <%= request.getServerName()%>

</body>
</html>