<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>

<%-- post방식 한글 데이터 전송 경우 깨짐 현상 방지 --%>
<% request.setCharacterEncoding("euc-kr"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset="euc-kr">
<title>response 내장객체 강제 이동(response_redirect_check.jsp)</title>
</head>
<body>
 <b>response 내장객체 강제 이동 페이지</b><Br>
 <%
  // 전송 데이터 변수 할당 및 확인
  String cust_id = request.getParameter("cust_id"); 
  String cust_pw = request.getParameter("cust_pw");
  
  String user_id = "id123";
  String user_pw = "pass";
  boolean loginState = false;
  if(cust_id.equals(user_id)){
	  if(cust_pw.equals(user_pw)){
		  %>
		  <script>
		  alert('로그인성공하였습니다.');
		  </script>
		  <%
		  loginState = true;
	  }else{
		  %>
		  <script>
		  alert('로그인실패하였습니다.');
		  </script>
		  <%
		  loginState = false;
	  }
  }
  
  out.print("아이디 = " + cust_id + "<Br>");
  out.print("비밀번호 = " + cust_pw + "<Br>");
  
  if(loginState == true){
  // 지정 페이지(URL)로 강제 이동
 // response.sendRedirect("./response_redirect_form.jsp");
  response.sendRedirect("./customer_insert_form.jsp");
  }else{
	  response.sendRedirect("./response_redirect_form.jsp");
  }
 %>

<!--  <p><a href="./response_redirect_form.jsp">[ redirect 폼 ]</a>   -->
</body>
</html>