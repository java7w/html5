<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>

<%@ page import="java.net.URLEncoder"%>

<!DOCTYPE html>
<html>
<head>
 <meta charset="euc-kr">
 <title>response 내장객체 강제 이동(response_para.jsp)</title>
</head>
<body>
 <b>response 내장객체 강제 이동 - 한글 파라미터 전송</b><Br>
 <%
   
   String para = "대한민국";
   String encode_para = URLEncoder.encode(para, "euc-kr");

   // 지정 페이지(URL)로 강제 이동
   // response.sendRedirect("./response_para_check.jsp?para=" + para);   
   response.sendRedirect("./response_para_check.jsp?para=" + encode_para);
  %>
 
</body>
</html>