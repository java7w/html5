<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>

<%@ page import="java.util.*"%>

<!DOCTYPE html>
<html>
<head>
 <meta charset="euc-kr">
 <title>request 내장 객체(request_header.jsp)</title>
</head>
<body>
 <b>request 내장 객체 - [헤더관련 정보]</b><Br>
 <%
  Enumeration<String> enu = request.getHeaderNames();
 
  while (enu.hasMoreElements()) {
  
     String head_name = (String)enu.nextElement();
     String head_value = request.getHeader(head_name);

     out.print("헤더 이름 : " + head_name + "<Br>" 
             + "헤더 값 : " + head_value + "<Br>");
  }
 %> 

</body>
</html>