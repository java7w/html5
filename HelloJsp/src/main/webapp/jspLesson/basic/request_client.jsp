<%@ page language="java" contentType="text/html; charset=euc-kr" pageEncoding="euc-kr"%>

<!DOCTYPE html>
<html>
<head>
 <meta charset="euc-kr">
 <title>request 내장 객체(request_client.jsp)</title>
</head>
<body>
 <b>request 내장 객체 - [클라이언트관련 정보]</b><Br>
 클라이언트 IP 주소 : <%= request.getRemoteAddr()%><Br>
 클라이언트 이름 : <%= request.getRemoteHost()%><Br>
 클라이언트 포트 : <%= request.getRemotePort()%><Br>
 클라이언트 사용자 : <%= request.getRemoteUser()%><Br>
 getCharacterEncoding : <%= request.getCharacterEncoding()%><Br>
 getContentLength : <%= request.getContentLength()%><Br>
 getContextPath : <%= request.getContextPath()%><Br>
 getContentType : <%= request.getContentType()%><Br>
 getHeaderNames : <%= request.getHeaderNames()%><Br>
 getRequestedSessionId : <%= request.getRequestedSessionId()%><Br>
 

</body>
</html>