<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
 <title>jstl core(jstl_loop.jsp)</title>
 <meta charset="UTF-8">
</head>

<body>
 <b> 반복 처리(forEach)</b><Br>
  <c:set var="sum" value="0"></c:set>
  
  <c:forEach var="i" begin="1" end="10" step="${i = i + 2 }">
    <c:set var="sum" value="${sum = sum + i}"></c:set>  
  </c:forEach>

  <c:out value="1부터 10까지 홀수 합 = ${sum}"></c:out><p>

 <b> 반복 처리(forTokens)</b><Br> 
  <c:forTokens var="msg" items="JSTL program test!" delims=" ">
    ${msg}
  </c:forTokens>
  
</body>
</html>
