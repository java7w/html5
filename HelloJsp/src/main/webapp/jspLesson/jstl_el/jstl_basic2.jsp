<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>JSP표준태그라이브러리(jstl),  표현언어(el)</title>

</head>
<body>
<h3>jstl, el : 기초사용방법</h3>
<hr>
<h4>변수설정(set):</h4>
<p>변수 jumsu = 99</p>
<c:set var="jumsu" value="99"></c:set>

<p>변수출력(out):
<c:out value="${jumsu}"></c:out>
</p>

<p>조건처리(if):
<c:if test="${jumsu<=100}">jumsu= ${jumsu} </c:if></p>

<p>
<c:out value="jumsu=${jumsu}"></c:out></p>

</body>
</html>