<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
 <title>jstl core(jstl_redirect.jsp)</title>
 <meta charset="UTF-8">
</head>

<body>
 <b> 페이지 이동(파라미터 전송)(redirect - param)</b><Br>  
  <c:redirect url="./jstl_redirect_para.jsp">
    <c:param name="para" value="massage"></c:param>
  </c:redirect>  
  
</body>
</html>
