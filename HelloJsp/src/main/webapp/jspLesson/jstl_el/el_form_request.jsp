<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<% // 전송 한글 데이터 처리
   request.setCharacterEncoding("UTF-8"); %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>표현 언어 사용(el_form_request.jsp)</title>
</head>
<body>
 <%
  // 전송 데이터 변수 할당
  String cust_id = (String)session.getAttribute("cust_id");
  String cust_name = (String)session.getAttribute("cust_name");
 %>
 
 <b>표현식과 out 객체 사용</b><Br>
 <%=cust_id %><Br>
 <%=cust_name %><Br>

 <%
  out.print("(" + cust_id + ")" + "(" + cust_name
		                  + ")님 로그인 환영합니다 !<p>");
 %>
 <p>
 <b>표현 언어와 내장객체 사용</b><Br> 
  ${cust_id}<Br>  
  ${cust_name}<Br>
 ${cust_id}(${cust_name})님 로그인 환영합니다 !<br> <br>
  
  내장객체 sessionScope(cust_id) : ${sessionScope.cust_id}<Br>
  내장객체 sessionScope(cust_name) : ${sessionScope.cust_name}</p>  

<p>내장객체 param(cust_id) : ${param.cust_id}<Br>
  내장객체 param(cust_name) : ${param.cust_name}</p>  
 <p><a href="./el_form.jsp">[ 확인 ]</a>   
</body>
</html>