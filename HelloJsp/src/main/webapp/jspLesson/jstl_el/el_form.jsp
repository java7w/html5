<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 <%
 // 세션 객체의 속성(세션 변수) 설정     	 
 session.setAttribute("cust_id", "admin");
 session.setAttribute("cust_name", "관리자"); 
 %>

<!DOCTYPE html>
<html>
<head>
<title>표현 언어 사용(el_form.jsp)</title>
<meta charset="UTF-8">
<style>
 table {width:200px; text-align:center; border-collapse:collapse;}
 th, td {border:1px solid grey; padding:3px;}
 th {width:50%; background-color:#CFD0ED;}
 td {text-align:left;}
</style>
</head>

<body>
 <form name="login_form" method="POST" action="./el_form_request.jsp">
  <table>
   <tr>
    <th>아 이 디</th>
    <td><input type="text" name="cust_id" size="10" maxlength="10" required autofocus></td>
   </tr>
   <tr>
    <th>이   름</th>
    <td><input type="text" name="cust_name" size="10" maxlength="10" required></td>
   </tr>
   <tr>
    <td colspan="2" style="text-align:center;">
        <input type="submit" value="전송"></td>
   </tr>
  </table>
 </form>
</body>
</html>