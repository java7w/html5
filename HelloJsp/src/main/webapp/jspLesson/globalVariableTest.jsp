<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>global variable test</title>
</head>
<body>
	<pre>
expression tag에서 선언한 변수는 global 변수로 사용됩니다.
scriptlet 에서 선언한 변수는 local 변수로 사용됩니다. 
</pre>

	<b>전역변수와 지역변수 적용범위</b>
	<Br>
	<%!// 선언문 - 전역변수 선언
	int global_var = 0;%>

	<%
	// 스크립트릿 - 지역변수 선언
	int local_var = 0;

	out.print("global_var = " + ++global_var + "<Br>");
	out.print("local_var = " + ++local_var + "<p>");
	%>

	<h3>
		표현식:
		<%=global_var%></h3>
	<h3>
		쪽코드:
		<%=local_var%></h3>
</body>
</html>