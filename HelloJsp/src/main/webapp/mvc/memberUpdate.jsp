<%@page import="mvc.MemberVO"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>회원정보수정: memberUpdate.jsp</title>
</head>
<body>
<h3>회원정보 검색</h3>
<form name="memberSearchForUpdate" action="memberSearch.do" method="post">
	아이디: <input type="text" name="id"><br>
	<input type="hidden" name="job" value="update"><br>
	<input type="submit" value="검색">
</form>
<%
request.setCharacterEncoding("euc-kr");
MemberVO member = (MemberVO)request.getAttribute("member");
if(member !=null){
%>
<form name="memberUpdateForm" action="memberUpdate.do" method="post">
	아이디: <input type="text" name="id"  readonly value="${member.id}"><br>
	비밀번호: <input type="password" name="pw"  value="${member.pw}"><br>
	이름: <input type="text" name="name"  value="${member.name}"><br>
	이메일: <input type="text" name="mail"  value="${member.mail}"><br>
	<input type="submit" value="변경">
</form>
<%}
else{
%>
${result}
<%
}
%>

</body>
</html>