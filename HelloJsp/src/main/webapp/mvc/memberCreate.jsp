<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="dbconnclose.DbConnClose"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>회원정보 테이블생성: memberCreate.jsp</title>
</head>
<body>
<%
Connection conn=null;
PreparedStatement pstmt=null;

conn = DbConnClose.getConnection();
String sql = "CREATE TABLE member("
			+"id VARCHAR(10),"
			+"pw VARCHAR(10),"
			+"name VARCHAR(10),"
			+"mail VARCHAR(20),"
			+"CONSTRAINT pk_member PRIMARY KEY(id)"
			+")";
pstmt = conn.prepareStatement(sql);
pstmt.executeUpdate();

DbConnClose.resourceClose(pstmt, conn);

out.println("<script>");
out.println(" alert('member 테이블 생성하였습니다!!');");
out.println(" history.back();");
out.println("</script>");
%>
</body>
</html>