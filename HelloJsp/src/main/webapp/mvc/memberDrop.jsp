<%@page import="dbconnclose.DbConnClose"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>회원정보 테이블 삭제: memberDrop.jsp</title>
</head>
<body>
<%
Connection conn = null;
PreparedStatement pstmt = null;

conn  = DbConnClose.getConnection();
String sql  = "drop table member";
pstmt = conn.prepareStatement(sql);

pstmt.executeUpdate();

DbConnClose.resourceClose(pstmt, conn);

out.println("<script>");
out.println(" alert('member테이블 삭제하였습니다!');");
out.println(" history.back();");
out.println("</script>");
%>
</body>
</html>