<%@page import="mvc.MemberVO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>회원목록: memberListResult.jsp</title>
<style>
  	table {
  		width:100%;
  	 	text-align:center; 
  	 	border:2px solid black; 
  	 	border-collapse:collapse;
  	}
	caption {font-size:20pt; font-weight:bold;}
	th, td {border:1px solid grey; padding:3px;}
	th {width:25%; background-color:#CFD0ED;}
	td {background-color:#FAFAEE; text-align:left;}
	.msg_red {font-size:10pt; color:red;}
	.msg_blue {font-size:10pt; color:blue;}
</style>
</head>
<body>
<%
ArrayList<MemberVO> list = (ArrayList<MemberVO>)request.getAttribute("list");
if(!list.isEmpty()){
%>
	<table border="1">
		<caption>회원목록</caption>
		<tr>
			<th>아이디</th>
			<th>패스워드</th>
			<th>이름</th>
			<th>메일</th>
		</tr>
		<% for(int i=0;i<list.size();i++) {
			MemberVO member = list.get(i);
		%>
		<tr>
			<td><%=member.getId() %></td>
			<td><%=member.getPw() %></td>
			<td><%=member.getName() %></td>
			<td><%=member.getMail() %></td>
		</tr>
		<%} %>
	</table>
<%
}else{
	out.println("<h3>회원정보가 없습니다</h3><br>");		
	
}
%>

<%@ include file="home.jsp"%>
</body>
</html>