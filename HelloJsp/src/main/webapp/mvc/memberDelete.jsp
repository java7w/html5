<%@page import="mvc.MemberVO"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>회원정보삭제: memberDelete.jsp</title>
</head>
<body>
<h3>회원정보삭제</h3>
<form name="memberSearchForDelete" action="memberSearch.do" method="post">
	아이디: <input type="text" name="id"><br>
	<input type="hidden" name="job" value="delete">
	<input type="submit" value="검색">
</form>
<%
request.setCharacterEncoding("euc-kr");
MemberVO member = (MemberVO)request.getAttribute("member");

if(member !=null){
%>
<form name="memberDeleteForm" action="memberDelete.do" method="post">
	아이디: <input type="text" value="${member.id}" readonly><br>
	비밀번호: <input type="password" value="${member.pw}" readonly><br>
	이름: <input type="text" value="${member.name}" readonly><br>
	이메일: <input type="text" value="${member.mail}" readonly><br>
	<input type="submit" value="삭제">
</form>
<%
}else{
%>
	${result}
<%
}
%>
</body>
</html>