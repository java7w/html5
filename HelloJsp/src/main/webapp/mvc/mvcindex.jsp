<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>mvc패턴: mvcindex.jsp</title>
</head>
<body>

<h3>MVC패턴 회원정보보드</h3>
<a href="memberInsert.jsp">회원정보생성</a><p>
<a href="memberSearch.jsp">회원정보검색</a><p>
<a href="memberUpdate.jsp">회원정보수정</a><p>
<a href="memberDelete.jsp">회원정보삭제</a><p>
<a href="memberList.do">회원정보보기</a><p>
<a href="memberCreate.jsp">회원정보테이블생성</a><p>
<a href="memberDrop.jsp">회원정보테이블삭제</a><p>
</body>
</html>