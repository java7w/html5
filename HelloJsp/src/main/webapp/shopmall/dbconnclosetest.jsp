<%@page import="dbconnclose.DbConnClose"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%
//jdbc 프로그래밍을 위한 객체(object) 변수를 생성합니다.
Connection conn = null;
PreparedStatement pstmt = null;
ResultSet rs = null;
//연결: driver 로딩, connect 정보사용해서 db연결....
conn=DbConnClose.getConnection();

//select문장은 rs를 사용하므로 3가지 매개변수를 넘겨서 종료합니다.
DbConnClose.resourceClose(rs, pstmt, conn);
//update, delete, insert문장은 2가지 매개변수만 넘겨서 종료합니다.
//DbConnClose.resourceClose(pstmt, conn);
%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>jsp에서 dbconnclose에서 만든 api 테스트</title>
</head>
<body>
<h3>DB연결/종료 테스트페이지</h3>
<p>오류가 나는 경우에만 오류메시지를 출력합니다!</p>
</body>
</html>