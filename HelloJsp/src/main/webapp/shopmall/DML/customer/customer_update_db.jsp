<%@page import="dbconnclose.DbConnClose"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>고객정보검색(customer_retrieval_form.jsp)</title>
<meta charset="euc-kr">
<link rel="stylesheet" href="../../common/CSS/common.css">
</head>
<body>
<%
Connection conn=null;
PreparedStatement pstmt=null;

conn = DbConnClose.getConnection();

String sql = "UPDATE customer SET cust_pw=?, cust_name=?"
	+",cust_tel_no=?, cust_addr=?, cust_email=?"
	+" WHERE cust_id=?; ";

String cust_pw = request.getParameter("cust_pw");	
String cust_name = request.getParameter("cust_name");	
String cust_tel_no;
String tel_no_gubun = request.getParameter("tel_no_gubun");
String tel_no_guk = request.getParameter("tel_no_guk");
String tel_no_seq = request.getParameter("tel_no_seq");
cust_tel_no = tel_no_gubun+"-"+tel_no_guk+"-"+tel_no_seq;
String cust_addr = request.getParameter("cust_addr");	

String cust_email_1 = request.getParameter("cust_email_1");	
String cust_email_2 = request.getParameter("cust_email_2");	
String cust_email = cust_email_1+"@"+cust_email_2;
String cust_id = request.getParameter("cust_id");	
out.println("cust_tel_no: "+cust_tel_no);
try{
	pstmt = conn.prepareStatement(sql);
	
	pstmt.setString(1, cust_pw);
	pstmt.setString(2, cust_name);
	pstmt.setString(3, cust_tel_no);
	pstmt.setString(4, cust_addr);
	pstmt.setString(5, cust_email);
	pstmt.setString(6, cust_id);
	
	pstmt.executeUpdate();
}catch(Exception e){
	out.println("고객정보갱신(Update)처리 오류가 발생했습니다.");
	out.println(e.getMessage());
}finally{
	DbConnClose.resourceClose(pstmt, conn);
}
%>


<%
	out.println("고객정보가 수정되었습니다!!<br>");
	out.println("<script>");
	out.println("  alert('고객정보가 수정되었습니다!!');");
 	out.println("  location.href='./customer_maintenance.jsp';");
	out.println("</script>");
%>
</body>
</html>