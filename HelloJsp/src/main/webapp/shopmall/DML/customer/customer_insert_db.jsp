<%@page import="java.time.LocalDate"%>
<%@page import="dbconnclose.DbConnClose"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
    
<% request.setCharacterEncoding("euc-kr"); %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>개인고객신규가입처리:db입력 customer_insert_db.jsp()</title>
</head>
<body>
<%@ include file="../../common/include/jsp_id_check_irud.inc" %>

<!-- 아이디중복확인:db에서 검색해서 혹시나 이미 있는 아이디인지확인합니다. -->
<!-- 없으면...insert into 작업을 수행합니다. -->

<%
Connection conn = null;
PreparedStatement pstmt = null;
ResultSet rs = null;

conn=DbConnClose.getConnection();

try{
	String sql = "select * from customer where cust_id=?";
	pstmt = conn.prepareStatement(sql);
	pstmt.setString(1, cust_id);
	rs = pstmt.executeQuery();
	
	
	if(rs.next()){//고객정보확인-이미 존재하는 아이디정보!
		out.print("<script>");
		out.print("  alert('사용할 수 없는 아이디입니다!');");
		out.print("  history.back();");
		out.print("</script>");
	}else{
		//회원가입처리: db insert 처리
		  String cust_pw = request.getParameter("cust_pw");
		  String cust_name = request.getParameter("cust_name"); 

		  // 전화번호 처리
		  String tel_no_gubun = request.getParameter("tel_no_gubun");
		  String tel_no_guk = request.getParameter("tel_no_guk");
		  String tel_no_seq = request.getParameter("tel_no_seq");
		  String cust_tel_no = tel_no_gubun + "-"
		                     + tel_no_guk + "-" + tel_no_seq;

		  String cust_addr = request.getParameter("cust_addr");
		  String cust_gender = request.getParameter("cust_gender"); // null

		  // 이메일 처리
		  String cust_email;
		  String cust_email_1 = request.getParameter("cust_email_1");
		  String cust_email_2 = request.getParameter("cust_email_2");

		  if ((cust_email_1.length() == 0) && (cust_email_2.length() == 0)) {
		      cust_email = "";  // empty
		  } else {
			  cust_email = cust_email_1 + "@" + cust_email_2;
		  }
		  
		  sql = "insert into customer values(?,?,?,?,?,?,?,?)";
		  pstmt = conn.prepareStatement(sql);
		  pstmt.setString(1, cust_id);
		  pstmt.setString(2, cust_pw);
		  pstmt.setString(3, cust_name);
		  pstmt.setString(4, cust_tel_no);
		  pstmt.setString(5, cust_addr);
		  pstmt.setString(6, cust_gender);
		  pstmt.setString(7, cust_email);
		  pstmt.setString(8, LocalDate.now().toString());
		  pstmt.executeUpdate();
	}
	
}catch(Exception e){
	out.println("SQL 처리에 오류가 있습니다!");
	out.println(e.getMessage());
}finally{
	DbConnClose.resourceClose(rs, pstmt, conn);
}
String name=request.getParameter("cust_name");
out.println("고객테이블(customer) 입력 저장완료!<br>");
out.println("<script>alert('"+name+"회원가입을 축하합니다!');");
out.println("  location.href='./customer_maintenance.jsp';</script>");


%>
</body>
</html>