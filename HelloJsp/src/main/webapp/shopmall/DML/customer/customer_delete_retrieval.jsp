<%@page import="java.time.LocalDate"%>
<%@page import="dbconnclose.DbConnClose"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>

<%
request.setCharacterEncoding("euc-kr");
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>고객정보삭제(customer_delete_retrieval.jsp)</title>
<link rel="stylesheet" href="../../common/CSS/common.css">
</head>
<body>
	<%@ include file="../../common/include/jsp_id_check_irud.inc"%>

	<!-- 아이디중복확인:db에서 검색해서 이미 있는 아이디인지확인합니다. -->
	<!-- 있으면...select * 작업을 수행합니다. -->
	<!-- form에 값을 입력해서 출력합니다. -->

	<%
	Connection conn = null;
	PreparedStatement pstmt = null;
	ResultSet rs = null;

	conn = DbConnClose.getConnection();
	try {
		 String sql = "SELECT * FROM customer WHERE (cust_id = ?)";      
	     pstmt = conn.prepareStatement(sql);
	     pstmt.setString(1, cust_id);
	     rs = pstmt.executeQuery();

	     // 질의처리 결과(rset) 체크
	     // cursor - 초기 상태 아니거나 행(결과)이 없으면 false 리턴
	     if (!rs.isBeforeFirst()) {
	        out.print("<script>alert('존재하지 않는 아이디입니다!!');"
	                        + "history.back();"
	                + "</script>");
	     }
			 // 첫 번째 레코드 커서 이동
	     rs.next();
	         
	     cust_id = rs.getString("cust_id");
	     String cust_pw = rs.getString("cust_pw");
	     String cust_name = rs.getString("cust_name");
	     String custTelNo = rs.getString("cust_tel_no");
	     String cust_addr = rs.getString("cust_addr");
	     String cust_gender = rs.getString("cust_gender");
	     String cust_email = rs.getString("cust_email");
	     String cust_join_date = rs.getString("cust_join_date");
			
			
	%>
	<form name="customer_form" method="get" action="./customer_delete_db.jsp">
		<table>
			<caption>회원정보갱신</caption>
			<tr style="border-style: hidden hidden solid hidden;">
				<td colspan="2" style="background-color: white; text-align: right;">
					<span class="msg_red">*부분은 필수입력 항목입니다!</span>
				</td>
			</tr>
			
			<%@ include file="../../common/include/html_output_urd.inc" %>
			
			<tr>
				<td colspan="2" style="text-align: center;">
					<input type="submit" value="탈퇴하겠습니까?(클릭!)"></input>
					<a href="./customer_maintenance.jsp">[고객정보관리 페이지]</a>
				</td>
			</tr>
			
			
		</table>
	</form>

	<%
		
	} catch (Exception e) {
	out.println("SQL 처리에 오류가 있습니다!");
	out.println(e.getMessage());
	} finally {
		DbConnClose.resourceClose(rs, pstmt, conn);
	}
	
	out.println("회원정보가 삭제되었습니다!<br>");
	%>
</body>
</html>