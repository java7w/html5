<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>고객정보갱신(customer_update_form.jsp)</title>
<meta charset="euc-kr">
<link rel="stylesheet" href="../../common/CSS/common.css">
</head>
<body>
	<form name="customer_form" method="get" 
		action="customer_update_retrieval.jsp"> 
	
	<table>
		<caption>회원정보갱신(회원데이터조회)</caption>
		<tr style="border-style:hidden hidden solid hidden;">
			<td colspan="2" style="background-color:white;text-align:right;">
				<span class="msg_red">*부분은 필수입력 항목입니다.</span>
			</td>
		</tr>
		
		<%@ include file="../../common/include/html_input1_irud.inc" %>
		<%@ include file="../../common/include/html_input2_rud.inc" %>
		<tr>
			<td colspan="2" style="text-align:center;">
				<input type="submit" value="회원정보검색">
					<a href="./customer_maintenance.jsp">[고객정보관리]</a>
				</input>
			</td>
		</tr>
	</table>
	
	</form>
</body>
</html>