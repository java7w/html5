<%@page import="dbconnclose.DbConnClose"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>[관리자] 고객정보 테이블 검색: 갱신,삭제(customer_retrieval_table.jsp)</title>
<link rel="stylesheet" href="../../common/CSS/table_retrieval.css">
</head>
<body>
<%
Connection conn=null;
Statement stmt=null;
ResultSet rs=null;
conn = DbConnClose.getConnection();
String sql = "select * from customer order by cust_id asc";

try{
	stmt = conn.createStatement();
	rs = stmt.executeQuery(sql);
	
	if(!rs.isBeforeFirst()){
		out.print("<script>");
		out.print("  alert('고객테이블(customer)이 비어 있습니다!');");
		out.print("  history.back();");
		out.print("</script>");
	}
%>

<form name="customer_form_table">
	<table>
		<caption>운영자 고객정보 관리</caption>
		<tr>
			<th>아이디</th>
			<th>비밀번호</th>
			<th>이름</th>
			<th>전화번호</th>
			<th>주소</th>
			<th>성별</th>
			<th>이메일</th>
			<th>가입일</th>
			<th>갱신</th>
			<th>삭제</th>
		</tr>
<%
	while(rs.next()){
		String cust_id = rs.getString("cust_id");
		String cust_pw = rs.getString("cust_pw");
		String cust_name = rs.getString("cust_name");
		String cust_tel_no = rs.getString("cust_tel_no");
		String cust_addr = rs.getString("cust_addr");
		String cust_gender = rs.getString("cust_gender");
		String cust_email = rs.getString("cust_email");
		String cust_join_date = rs.getString("cust_join_date");
		if(cust_gender.equals("M")){
			cust_gender = "남자";
		}else if(cust_gender.equals("F")){
			cust_gender ="여자";
		}else{
			cust_gender = "etc";
		}
%>
		<tr>
			<td><%=cust_id%></td>
			<td><%=cust_pw%></td>
			<td><%=cust_name%></td>
			<td><%=cust_tel_no%></td>
			<td><%=cust_addr%></td>
			<td><%=cust_gender%></td>
			<td><%=cust_email%></td>
			<td><%=cust_join_date%></td>
			<td><a href="./customer_update_retrieval.jsp?cust_id=<%=cust_id %>">[갱신]</a></td>
			<td><a href="./customer_delete_retrieval.jsp?cust_id=<%=cust_id%>">[삭제]</a></td>
		</tr>
<%
	}
}catch(Exception e){
	out.println("SQL오류발생! (select)");
	out.println(e.getMessage());
}finally{
	

%>		
	</table>
</form>
<%
	rs.last();
	int row_cnt = rs.getRow();
	out.println("고객테이블(customer) " + row_cnt+"개 레코드 검색했습니다! <br>");
	
	DbConnClose.resourceClose(rs, stmt, conn);
}
%>
</body>
</html>