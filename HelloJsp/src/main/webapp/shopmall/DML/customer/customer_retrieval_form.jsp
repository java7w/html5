<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>고객정보검색(customer_retrieval_form.jsp)</title>
<meta charset="euc-kr">
<link rel="stylesheet" href="../../common/CSS/common.css">
</head>
<body>
	<form name="customer_form" method="get" action="customer_retrieval_db.jsp">
		<table>
			<caption>회원정보검색</caption>
			<tr style="border-style:hidden hidden solid hidden;">
				<td colspan="2" style="background-color:white; text-align:right;">
					<span class="msg_red">*부분은 필수입력 항목입니다</span></td>
			</tr>

			<%@ include file="../../common/include/html_input1_irud.inc" %>
			
			<tr>
				<th><span class="msg_red">*</span>비밀번호</th>
				<td><input type="password" name="cust_pw"
					style="ime-mode:disabled"
					size="11" maxlength="10" disabled></input>
					<span style="msg_blue">(영,숫자 10자리이내)</span>비밀번호</td>
			</tr>
			
			<tr>
				<th><span class="msg_red">*</span>이름</th>
				<td><input type="text" name="cust_name"
					size="15" maxlength="15"
					style="ime-mode:active" 
					disabled></input>
				</td>
			</tr>
			
			<tr>
				<th><span class="msg_red">*</span>전화번호</th>
				<td><select name="tel_no_gubun" disabled>
						<option>서비스구분</option>
						<option value="010">010</option>
						<option value="011">011</option>
					</select>-
					<input type="text" name="tel_no_guk" 
						size="5" maxlength="4" disabled></input>-
					<input type="text" name="tel_no_seq" 
						size="5" maxlength="4" disabled></input>
				</td>
			</tr>
			
			<tr>
				<th><span class="msg_red">*</span>주소</th>
				<td><input type="text" name="cust_addr"
					size="60" maxlength="60"
					disabled></input>
				</td>
			</tr>
			
			<tr>
				<th>성별</th>
				<td><input type="radio" name="cust_gender" value="M" disabled>남자</input>
					<input type="radio" name="cust_gender" value="F" disabled>여자</input>
				</td>
			</tr>
			
			<tr>
				<th>이메일</th>
				<td><input type="text" name="cust_email_1"
					size="15" maxlength="10"
					style="ime-mode:disabled;" disabled></input>@
					<input type="text" name="cust_email_2"
					size="15" maxlength="10"
					style="ime-mode:disabled;" disabled></input>
				</td>
			</tr>
			
			<tr>
				<td colspan="2" style="text-align:center;">
					<input type="submit" value="회원검색" ></input>
					<a href="./customer_maintenance.jsp">[고객정보관리 페이지]</a>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>