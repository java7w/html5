CREATE DATABASE shopmall;

USE shopmall

DROP TABLE customer;
CREATE TABLE customer(
	cust_id VARCHAR(10) NOT NULL,
	cust_pw VARCHAR(10),
	cust_name VARCHAR(15),
	cust_tel_no VARCHAR(13),
	cust_addr VARCHAR(100),
	cust_gender CHAR(1),
	cust_email VARCHAR(30),
	cust_join_date DATE,
	CONSTRAINT pk_customer PRIMARY KEY(cust_id)
) ENGINE=InnoDB DEFAULT CHARSET=EUCKR_korean_ci COMMENT='게시판 고객 테이블';

CREATE TABLE customer(
	cust_id VARCHAR(10) NOT NULL,
	cust_pw VARCHAR(10),
	cust_name VARCHAR(15),
	cust_tel_no VARCHAR(13),
	cust_addr VARCHAR(100),
	cust_gender CHAR(1),
	cust_email VARCHAR(30),
	cust_join_date DATE,
	CONSTRAINT pk_customer PRIMARY KEY(cust_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='게시판 고객 테이블';
DESC customer;

# select 명령어 test...
SELECT * FROM customer
SELECT * FROM customer WHERE cust_id='id132';

# update 명령어 test ...
UPDATE customer SET cust_pw=?, cust_name=?
 ,cust_tel_no=?, cust_addr=?, cust_email=?
 WHERE cust_id=?;
 
UPDATE customer SET cust_pw='1234', cust_name='명길동'
 ,cust_tel_no='010-1234-2345', cust_addr='서울시 금천구 가산동 1234'
 ,cust_email='hong3@gmail.com'
 WHERE cust_id='id132'; 


# delete sql 테스트...
DELETE FROM customer WHERE cust_id='id001' AND cust_pw='123';
SELECT * FROM customer;
INSERT INTO customer(cust_id, cust_pw, cust_name, cust_tel_no
	, cust_addr, cust_gender, cust_email, cust_join_date) 
  VALUES('id001', '123', '홍길동', '010-1234-1235'
	, '서울시 금천구 가산동', 'M', 'hong1@gmail.com'
	, date_format(NOW(),'%Y-%m-%d') )
  
   
SELECT date_format(NOW(),'%Y-%m-%d %H:%i:%s') 



