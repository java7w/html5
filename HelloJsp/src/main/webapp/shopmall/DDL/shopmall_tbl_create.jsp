<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>

<!DOCTYPE html>
<html>
<head>
 <title>쇼핑몰데이터베이스(shopmall) table 생성(shopmall_tbl_create.jsp)</title>
 <meta charset="UTF-8">
</head>

<body>
 <% 
  // JDBC 드라이버 로딩(loading JDBC driver)
  String driverClass = "org.mariadb.jdbc.Driver";
	 
  try {
     Class.forName(driverClass);
     out.println("JDBC Driver loading 성공!<Br>");
  } catch (ClassNotFoundException err) {
     out.println("JDBC Driver loading 실패!!...WEB-INF/lib 폴더 확인<Br>");
  }
    
  // MariaDB 서버 연결(connect server)
  // "jdbc:mariadb://server_IP:3306/datbase_name
  String url = "jdbc:mariadb://localhost:3306/shopmall";
  String id = "dev";      // DB 사용자 아이디
  String pw = "pass";     // DB 사용자 패스워드
   
  // 객체 참조 변수
  Connection conn = null;
  PreparedStatement pstmt = null;
  try{
  conn = DriverManager.getConnection(url, id, pw);
  
  // SQL 질의어 처리(Perform SQL query(DDL))
  // 쇼핑몰데이터베이스(shopmall) 스키마 생성
  String sql = "CREATE TABLE customer("
			+"cust_id VARCHAR(10) NOT NULL,"
			+"cust_pw VARCHAR(10),"
			+"cust_name VARCHAR(15),"
			+"cust_tel_no VARCHAR(13),"
			+"cust_addr VARCHAR(100),"
			+"cust_gender CHAR(1),"
			+"cust_email VARCHAR(30),"
			+"cust_join_date DATE,"
			+"CONSTRAINT pk_customer PRIMARY KEY(cust_id) )";
  pstmt = conn.prepareStatement(sql);
  pstmt.executeUpdate();    
  out.println("고객 테이블(customer) 생성 성공!<Br>");
  }catch(Exception e){
	  out.println("shopmall 데이터베이스 생성 실패!");
	  out.println(e.getMessage());
  }finally{
	  if(pstmt != null){try{pstmt.close();}catch(Exception e){e.getMessage();}}
	  if(conn != null){try{conn.close();}catch(Exception e){e.getMessage();}}
  }
%>

</body>
</html>