<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>java bean을 사용하기 hellobean.jsp</title>
</head>
<body>
	<jsp:useBean id="hello" class="servlet.HelloBean"></jsp:useBean>
	<h3>getProperty를 사용해서 bean클래스에 저장된 값을 출력하기:</h3>
	<jsp:getProperty name="hello" property="name"></jsp:getProperty><br>
	<jsp:getProperty name="hello" property="number"></jsp:getProperty><br>
	
	<h3>setProperty를 사용해서 bean클래스에 값을 저장하기:</h3>
	<jsp:setProperty name="hello" property="name" param="홍길동2"></jsp:setProperty><br>
	<jsp:setProperty name="hello" property="number" param="010-2345-3456"></jsp:setProperty><br>
	<jsp:setProperty property="*" name="hello"></jsp:setProperty>
	
	<h3>getProperty를 사용해서 bean클래스에 저장된 값을 출력하기:</h3>
	<jsp:getProperty name="hello" property="name"></jsp:getProperty><br>
	<jsp:getProperty name="hello" property="number"></jsp:getProperty><br>
	
</body>
</html>