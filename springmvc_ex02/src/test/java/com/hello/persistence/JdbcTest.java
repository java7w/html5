package com.hello.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Test;

import lombok.extern.log4j.Log4j;

@Log4j
public class JdbcTest {
	static String className = "com.mysql.jdbc.Driver";
	//String driverClass = "org.mariadb.jdbc.Driver";
	String url = "jdbc:mysql://localhost:3306/testdb";
	//String url = "jdbc:mariadb://localhost:3306/shopmall";
	String id = "dev";
	String pw = "pass";

	static {
		try {
			Class.forName(className);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, id, pw);
			log.info("db connection success!!");
		} catch (SQLException e) {
			e.printStackTrace();
			log.info("db connection failed!!");
		}
	}
}
