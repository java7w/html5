package com.hello.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.WebApplicationContext;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@Log4j
@WebAppConfiguration
@ContextConfiguration({"file:src/main/webapp/WEB-INF/spring/root-context.xml"
	, "file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class BoardControllerTests {
	
	@Setter(onMethod_= {@Autowired})
	private WebApplicationContext ctx;
	
	private MockMvc mockMvc;
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
	}
	
//	@Test
	public void testList() {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/board/list"); 
		ModelMap map = null;
		try {
			map = mockMvc.perform(requestBuilder).andReturn().getModelAndView().getModelMap();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info(map);
	}
	
//	@Test
	public void testRegister() {
		RequestBuilder requestBuilder 
			= MockMvcRequestBuilders.post("/board/register")
				.param("title", "스프링웹프로젝트") 
				.param("content", "스프링웹프로젝트게시판만들기") 
				.param("writer", "홍길동"); 
		String view = null;
		try {
			view = mockMvc.perform(requestBuilder).andReturn().getModelAndView().getViewName();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.info(view);
	}
	
//	@Test
	public void testGet() {
		ModelMap map = null;
		
		RequestBuilder requestBuilder =MockMvcRequestBuilders.get("/board/get")
				.param("bno", "12");
		try {
			map = mockMvc.perform(requestBuilder).andReturn()
					.getModelAndView().getModelMap();//return데이터는 BoardVo 데이터
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.info(map);
	}
	public void testGet(String bno) {
		ModelMap map = null;
		
		RequestBuilder requestBuilder 
		  = MockMvcRequestBuilders.get("/board/get")
				.param("bno", bno);
		try {
			map = mockMvc.perform(requestBuilder).andReturn()
					.getModelAndView().getModelMap();//return데이터는 BoardVo 데이터
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.info(map);
	}
	
	public void testModify() {
		RequestBuilder requestBuilder 
			= MockMvcRequestBuilders.post("/board/modify")
				.param("title", "스프링웹프로젝트2") 
				.param("content", "스프링웹프로젝트게시판만들기 더 쉽게~") 
				.param("writer", "홍길동") 
				.param("bno", "12"); 
		String view = null;
		try {
			view = mockMvc.perform(requestBuilder).andReturn().getModelAndView().getViewName();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.info(view);
	}
	public void testModify(String bno) {
		RequestBuilder requestBuilder 
		= MockMvcRequestBuilders.post("/board/modify")
		.param("title", "스프링웹프로젝트2") 
		.param("content", "스프링웹프로젝트게시판만들기 더 쉽게~") 
		.param("writer", "홍길동") 
		.param("bno", bno); 
		String view = null;
		try {
			view = mockMvc.perform(requestBuilder).andReturn().getModelAndView().getViewName();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		log.info(view);
	}
	
//	@Test
	public void testRemove() {
		String view = null;
		RequestBuilder requestBuilder 
		= MockMvcRequestBuilders.post("/board/remove")
			.param("bno", "12");
		try {
			view = mockMvc.perform(requestBuilder).andReturn()
					.getModelAndView().getViewName();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info(view);
	}
	
	@Test
	public void boardControllerTestMain() {
		testModify("12");
		testGet("12");
		
//		testRemove();
//		testList();
	}
}
