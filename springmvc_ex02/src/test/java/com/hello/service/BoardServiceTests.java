package com.hello.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hello.domain.BoardVo;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/root-context.xml")
// Java Config
// @ContextConfiguration(classes = {org.zerock.config.RootConfig.class} )
@Log4j
public class BoardServiceTests {

	@Setter(onMethod_ = { @Autowired })
	private BoardService service;

//	@Test
	public void testExist() {

		log.info(service);
		assertNotNull(service);
	}

//	@Test
	public void testRegister() {
		BoardVo board = new BoardVo();
		String title = "자바북스토리";
		String content = "자바성장스토리";
		String writer = "홍길동";
		board.setTitle(title);
		board.setContent(content);
		board.setWriter(writer);
		
		 service.register(board);
		 log.info("책이 등록되었습니다");
//		 log.info("생성된 게시물 번호: " + board.getBno());
	}
	
//	@Test
	public void testGetList() {
		service.getList().forEach(board->log.info(board));
	}
	
//	@Test
	public void testGetBoard() {
		int bno = 11;
		BoardVo board = null;
		board = service.get(bno);
		log.info(board);
	}
	
//	@Test
	public void testUpdateBoard() {
		BoardVo board = new BoardVo();
		String title = "자바북스토리";
		String content = "자바성장스토리2";
		String writer = "홍길동";
		int bno = 11;
		board.setTitle(title);
		board.setContent(content);
		board.setWriter(writer);
		board.setBno(bno);
		
		 service.modify(board);
		 log.info("책정보가 변경되었습니다");
	}
	public void testUpdateBoard(int bno) {
		BoardVo board = new BoardVo();
		String title = "자바북스토리";
		String content = "자바성장스토리";
		String writer = "홍길동";
		
		board.setTitle(title);
		board.setContent(content);
		board.setWriter(writer);
		board.setBno(bno);
		
		service.modify(board);
		log.info("책정보가 변경되었습니다");
	}
	
	public void testDeleteBoard() {
		int bno = 11;
		service.remove(bno);
		log.info("책정보가 삭제되었습니다");
	}
	
	public void testDeleteBoard(int bno) {
		service.remove(bno);
		log.info("책정보가 삭제되었습니다");
	}
	
	@Test
	public void testMain() {
		//this.testDeleteBoard(11);
		this.testGetList();
	}
}
