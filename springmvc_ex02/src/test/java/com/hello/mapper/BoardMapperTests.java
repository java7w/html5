package com.hello.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.hello.domain.BoardVo;

import lombok.Setter;
import lombok.extern.log4j.Log4j;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/spring/root-context.xml")

@Log4j
public class BoardMapperTests {
	
	@Setter(onMethod_ = @Autowired)
	private BoardMapper mapper;
	
	@Test
	public void testGetList() {
		try {
			mapper.getList().forEach(board->log.info(board));
			log.info("success!!");
		}catch(Exception e) {
			e.printStackTrace();
			log.info("failed!!!!");
		}
	}
	
//	@Test
	public void testInsert() {
		BoardVo board = new BoardVo();
		String title = "테스트북";
		String content = "테스트코드내용";
		String writer = "홍길동";
		board.setTitle(title);
		board.setContent(content);
		board.setWriter(writer);
		
		mapper.insert(board);
		
		log.info("insert board: "+board);
	}
	
	
//	@Test
	public void testRead() {
		BoardVo board = mapper.read(10);
		log.info("insert board: "+board);
	}
	
//	@Test
	public void testDelete() {
		int result = 0;
		result = mapper.delete(10);
		log.info("delete: "+result);
	}
	
	public void testUpdate() {
		BoardVo board = new BoardVo();
		String title="스프링북";
		String content="스프링프레임코딩";
		String writer="홍길서";
		board.setBno(1);
		board.setTitle(title);
		board.setContent(content);
		board.setWriter(writer);
		int count = mapper.update(board);
		log.info("update count: " + count);
	}
	
	@Test
	public void mytest() {
		testUpdate();
		this.testGetList();
	}
}
