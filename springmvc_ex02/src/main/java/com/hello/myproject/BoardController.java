package com.hello.myproject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.hello.domain.BoardVo;
import com.hello.service.BoardService;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Controller
@RequestMapping("/board/*")
public class BoardController {
	private static final Logger log = LoggerFactory.getLogger(BoardController.class);
	
	@Autowired
	private BoardService service;
	
	// url:  /board/list
	@GetMapping("/list")
	public void list(Model model) {
		log.info("list");
		model.addAttribute("list", service.getList());
		
	}
	
	@GetMapping("/register")
	public void register() {
		//페이지를보여주는 기능만함
	}
	
//    @RequestMapping(value="/register", 
//            method=RequestMethod.POST, 
//            produces="application/text;charset=euc-kr")
	@PostMapping("/register")
	public String register(BoardVo board, RedirectAttributes rttr) {
		log.info("register: " + board);
		//service.register(board);
		service.getBnoAfterRegister(board);
		int bno = board.getBno();
		log.info("bno: " + bno);
		rttr.addFlashAttribute("result", bno);
		
		//목록으로 이동!
		//redirect: 는 response.sendRedirect()를 처리....
		String page="/board/list";
		String url = null;
		url = "redirect:" + page;
		return url;
	}
	
	//request 주소url:  /board/get?bno=12
	@GetMapping({"/get", "/modify"})
	public void get(@RequestParam("bno") int bno, Model model) {
		log.info("/get or /modify: ");
		log.info("bno: " + bno);
		BoardVo board = service.get(bno);
		model.addAttribute("board", board);
	}
	
	@PostMapping("/modify")
	public String modify(BoardVo board, RedirectAttributes rttr) {
		log.info("modify: " + board);
		
		boolean result = service.modify(board);
		if(result) {
			rttr.addFlashAttribute("result", "success");
		}else {
			//history.back()
		}
		
		//목록으로 이동!
		//redirect: 는 response.sendRedirect()를 처리....
		String page="/board/list";
		String url = null;
		url = "redirect:" + page;
		return url;
	}
	
	@PostMapping("/remove")
	public String remove(@RequestParam("bno") int bno, RedirectAttributes rttr) {
		log.info("remove..." + bno);
		
		if(service.remove(bno)==true) {
			rttr.addFlashAttribute("result", "success");
		}else {
			//history.back()
		}
		return "redirect:/board/list";
	}
}
