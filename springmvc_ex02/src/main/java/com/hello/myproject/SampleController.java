package com.hello.myproject;

import java.util.ArrayList;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hello.domain.SampleDto;
import com.hello.domain.TodoDto;

@Controller
@RequestMapping("/sample/*")
public class SampleController {
	private static final Logger log = LoggerFactory.getLogger(SampleController.class);
	
	@RequestMapping("")
	public void basic() {
		String msg = "basic........";
		log.debug(msg);
		log.trace(msg);
		log.info(msg);
		log.warn(msg);
		log.error(msg);
	}
	
	@RequestMapping(value="/basic", method= {RequestMethod.GET, RequestMethod.POST})
	public void basicGet() {
		String msg = "basic get....";
		log.info(msg);
	}
	
	@GetMapping("/basicOnlyGet")
	public void basicGet2() {
		String msg = "basic get only get ....";
		log.info(msg); // basicOnlyGet.jsp
	}
	
	@GetMapping("/ex01")
	public String ex01(SampleDto dto) {
		String msg = ""+dto;
		log.info(msg);
		return "ex01";//ex01.jsp
	}
	
	@GetMapping("/ex02")
	public String ex02(@RequestParam("name") String name
			, @RequestParam("age") int age) {
		String msg = "name: " + name;
		log.info(msg);
		msg = "age: " + age;
		log.info(msg);
		return "ex02";//ex02.jsp
	}
	
	// ����Ʈ, ���, ��,....
	//http://localhost:8080/sample/ex02List?ids=aaa&ids=bbb&ids=ccc
	@GetMapping("/ex02List")
	public String ex02List(@RequestParam("ids") ArrayList<String> ids) {
		String msg = "ids: " + ids;
		log.info(msg);
		return "ex02List";
	}

	//http://localhost:8080/sample/ex02Array?ids=aaa&ids=bbb&ids=ccc
	@GetMapping("/ex02Array")
	public String ex02Array(@RequestParam("ids") String[] ids) {
		String msg = "array ids: " + Arrays.toString(ids);
		log.info(msg);
		return "ex02Array";
	}
	
	//http://localhost:8080/sample/ex03?title=abc&dueDate=2021/03/02
	@GetMapping("/ex03")
	public String ex04(TodoDto todo) {
		String msg = ""+ todo;
		log.info(msg);
		return "ex02";
	}
	
	//http://localhost:8080/sample/ex04?name=aaa&age=12&page=100
	@GetMapping("/ex04")
	public String ex04(SampleDto dto, @ModelAttribute("page") int page) {
		String msg = "dto: " + dto;
		log.info(msg);
		msg = "page: " + page;
		log.info(msg);
		return "/sample/ex04";
	}
	
	@GetMapping("/ex05")
	public void ex05() {
		String msg = "ex05....";
		log.info(msg);//ex05.jsp
	}
	
	@GetMapping("/ex06")
	public @ResponseBody SampleDto ex06() {
		String msg = "ex06....";
		log.info(msg);
		SampleDto dto = new SampleDto();
		dto.setName("ȫ�浿");
		dto.setAge(20);
		return dto;
	}
	
	@GetMapping("/ex07")
	public ResponseEntity<String> ex07(){
		String msg = "{'name': 'ȫ�浿'}";
		HttpHeaders header = new HttpHeaders();
		header.add("Content-type", "application/json;charset=utf-8");
		
		return new ResponseEntity<> (msg, header, HttpStatus.OK);
	}
	
	@GetMapping("/exUpload")
	public void exUpload() {
		String msg = "exUpload...";
		log.info(msg);
	}
	
	@PostMapping("/exUploadPost")
	public void exUploadPost(ArrayList<MultipartFile> files) {
		String msg = "exUploadPost...";
		log.info(msg);
		if(files!=null) {
		files.forEach(file->{
			log.info("---------------------------");
			log.info("name: " + file.getOriginalFilename());
			log.info("size: " + file.getSize());
		
		});
		}else {
			log.info("files null!!");
		}
	} 
	
}
