package com.hello.domain;

import lombok.Data;

@Data //lombok: get, set, toString....
public class SampleDto {
	private String name;
	private int age;
	
}
