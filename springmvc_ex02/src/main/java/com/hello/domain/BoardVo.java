package com.hello.domain;

import java.util.Date;

import lombok.Data;

//bno int AUTO_INCREMENT,
//title VARCHAR(200) NOT NULL,
//content VARCHAR(2000) NOT NULL,
//writer VARCHAR(50) NOT NULL,
//regdate DATE NOT null,
//updatedate DATE NOT null,
@Data
public class BoardVo {
	private int bno;
	private String title;
	private String content;
	private String writer;
	private Date regdate;
	private Date updatedate;
}
