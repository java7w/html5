package com.hello.service;

import java.util.List;

import com.hello.domain.BoardVo;

public interface BoardService {
	public void register(BoardVo board);//회원등록 서비스
	public int getBnoAfterRegister(BoardVo board);//회원등록 서비스
	public BoardVo get(int bno);//회원개별조회(회원검색) 서비스
	public boolean modify(BoardVo board);//회원정보변경 서비스
	public boolean remove(int bno); //회원정보삭제(탈퇴) 서비스
	public List<BoardVo> getList(); //회원전체목록출력 서비스
}
