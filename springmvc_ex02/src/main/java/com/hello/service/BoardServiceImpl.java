package com.hello.service;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hello.domain.BoardVo;
import com.hello.mapper.BoardMapper;
import com.hello.persistence.BoardDao;

import lombok.AllArgsConstructor;
import lombok.Setter;

//@Log4j
@Service
@AllArgsConstructor
public class BoardServiceImpl implements BoardService {
	private static final Logger log = LoggerFactory.getLogger(BoardServiceImpl.class);
	
	@Setter(onMethod_=@Autowired)
	private BoardMapper mapper;
	
	@Override
	public void register(BoardVo board) {
		log.info("register......" + board);
		mapper.insert(board);
	}
	
	@Override
	public int getBnoAfterRegister(BoardVo board) {
		int bno = 0;
		log.info("register......" + board);
		bno = mapper.getBnoAfterInsert(board);
		return bno;
	}

	@Override
	public BoardVo get(int bno) {
		log.info("get......" + bno);
		BoardVo vo = null;
		vo = mapper.read(bno);
		return vo;
	}

	@Override
	public boolean modify(BoardVo board) {
		log.info("modify......" + board);
		int result = 0;
		
		result = mapper.update(board);
		
		return result==1?true:false;
	}

	@Override
	public boolean remove(int bno) {
		log.info("remove......" + bno);
		int result = 0;
		result = mapper.delete(bno);
		
		return result==1?true:false;
	}

	@Override
	public List<BoardVo> getList() {
		List<BoardVo> list = null;
		list = mapper.getList();
		return list;
	}

}
