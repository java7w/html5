package com.hello.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

//AOP: 관점지향적인 프로그래밍 방식
//핵심기능은 아니지만 공통으로 처리하는 것이 편리한 방법일 때 공통으로 방법을 적용한 방식
@ControllerAdvice
public class CommonExceptionAdvice {
	private static final Logger log = LoggerFactory.getLogger(CommonExceptionAdvice.class);
	
	@ExceptionHandler(Exception.class)
	public String except(Exception e, Model model) {
		log.error("Exception: "+e.getMessage());
		model.addAttribute("exception", e);
		log.error(""+model);
		return "error_page";
	}
	
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public String hadle404(NoHandlerFoundException e) {
		return "custom404";
	}
}
