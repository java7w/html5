package com.hello.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.hello.domain.BoardVo;

public interface BoardMapper {
//	@Select("select * from tbl_board")
	public List<BoardVo> getList();
	public void insert(BoardVo board);
	public int getBnoAfterInsert(BoardVo board);
	public int update(BoardVo board);//date입력은 updatedate만 값만 사용합니다!
//	public void insertSelectKey(BoardVo board); // oracle db를 사용하는 경우에 실습하세요!!
	public BoardVo read(int bno);
	public int delete(int bno); // delete 성공-> 1 (true)이상의 값, 실패->0(false) 
}
