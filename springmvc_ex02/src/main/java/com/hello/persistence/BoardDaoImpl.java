package com.hello.persistence;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.hello.domain.BoardVo;

@Repository
public class BoardDaoImpl implements BoardDao {

	private static String namespace="com.hello.mapper.BoardMapper";
	
	@Inject
	private SqlSession session;
	

	@Override
	public void insert(BoardVo vo) {
		String statement = namespace + ".insert";
		session.insert(statement, vo); 
	}

	@Override
	public BoardVo read(int bno) {
		String statement = namespace + ".read";
		BoardVo board = null;
		board = session.selectOne(statement, bno);
		return board;
	}

	@Override
	public void update(BoardVo vo) {
		String statement = namespace + ".update";
		session.update(statement, vo);
	}

	@Override
	public void delete(int bno) {
		String statement = namespace + ".delete";
		session.delete(statement, bno);
	}

	@Override
	public List<BoardVo> listAll() {
		List<BoardVo> list = null;
		String statement = namespace + ".getList";
		list = session.selectList(statement);
		return list;
	}

}
