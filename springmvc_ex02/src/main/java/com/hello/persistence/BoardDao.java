package com.hello.persistence;

import java.util.List;

import com.hello.domain.BoardVo;

public interface BoardDao {
	public void insert(BoardVo vo);
	public BoardVo read(int bno);
	public void update(BoardVo board);
	public void delete(int bno);
	public List<BoardVo> listAll();
	//
	//
	//
	//
}
