<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
<!-- <title>List Page...</title> -->
    <title>SB Admin 2 - Bootstrap Admin Theme</title>
    
    <%@include file="../includes/head_info.jsp" %>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        $(".sidebar-nav")
        .attr("class", "sidebar-nav navbar-collapse collapse")
        .attr("aria-expanded", 'false')
        .attr("style", "height:1px");
    });
    </script>
    
    <script>
    	$(document).ready(function(){
    		var result='<c:out value="${result}" />';
    		checkModal(result);
    		function checkModal(result){
    			if(result ===''){
    				return;
    			}
    			if(parseInt(result)>0){
    				$(".modal-body").html("게시글 " + parseInt(result)+"번 글이 등록되었습니다.");
    			}
    			$("#myModal").modal("show");
    		};
    		$("#regBtn").on("click", function(){
    			self.location="/board/register"
    		});
    	});
    </script>
</head>
<body>

<h3>목록 게시판 </h3>

<%@include file="../includes/header.jsp" %>
<%-- <%request.setCharacterEncoding("euc-kr");%>; --%>

<div class="row">
	<div class="col-1g-12">
		<h1 class="page-header">Board Register</h1>
	</div>
</div>
<div class="row">
	<div class="col-1g-12">
		<div class="panel-heading">Board Register</div>
		<div class="panel-body">
			<form role="form" action="/board/register" method="post" >
				<div class="form-group">
					<label>Title</label><input class="form-control" name="title">
				</div> 
				<div class="form-group">
					<label>Contents</label><textarea rows="3" class="form-control" name="content"></textarea>
				</div>
				<div class="form-group">
					<label>Writer</label><input class="form-control" name="writer">
				</div>
				<button type="submit" class="btn btn-default">등록</button>
				<button type="reset" class="btn btn-default">취소</button>
			</form>
		</div>
	</div>
</div>
<%@include file="../includes/footer.jsp" %>