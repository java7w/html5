<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
<!-- <title>List Page...</title> -->
    <title>SB Admin 2 - Bootstrap Admin Theme</title>
    
    <%@include file="../includes/head_info.jsp" %>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        $(".sidebar-nav")
        .attr("class", "sidebar-nav navbar-collapse collapse")
        .attr("aria-expanded", 'false')
        .attr("style", "height:1px");
    });
    </script>
        <script>
    	$(document).ready(function(){
    		var result='<c:out value="${result}" />';
    		console.log("result: " + result);
    		checkModal(result);
    		history.replaceState({}, null, null)
    		function checkModal(result){
    			if(result ==='' || history.state){
    				return;
    			}
    			if(parseInt(result)>0){
    				$(".modal-body").html("게시글 " + parseInt(result)+"번 글이 등록되었습니다.");
    			}
    			$("#myModal").modal("show");
    		};
    		$("#regBtn").on("click", function(){
    			self.location="/board/register";
    		});
    	});
    </script>

</head>
<body>
<h3>목록 게시판 </h3>

<%@include file="../includes/header.jsp" %>

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tables</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Board List Page
                            <button id="regBtn" type="button" class="btn btn-xs pull-right">Register New Board</button>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>#번호</th>
                                        <th>제목</th>
                                        <th>작성자</th>
                                        <th>작성일</th>
                                        <th>수정일</th>
                                    </tr>
                                </thead>
                                <c:forEach items="${list}" var="board">
                                	<tr>
                                		<td><c:out value="${board.bno}"></c:out></`td>
                                		<td><a class='move' href='/board/get?bno=<c:out value="${board.bno}"/>'><c:out value="${board.title}"></c:out></a></td>
                                		<td><c:out value="${board.writer}"></c:out></td>
                                		<td><fmt:formatDate pattern="yyyy-MM-dd" value="${board.regdate}"></fmt:formatDate></td>
                                		<td><fmt:formatDate pattern="yyyy-MM-dd" value="${board.updatedate}"></fmt:formatDate></td>
                                	</tr>
                                
                                </c:forEach>
                            </table>
                            
                            
                            <!-- Modal  추가 -->
							<div id="myModal" class="modal fade" tabindex="-1" role="dialog"
								aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal"
												aria-hidden="true">&times;</button>
											<h4 class="modal-title" id="myModalLabel">Modal title</h4>
										</div>
										<div class="modal-body">처리가 완료되었습니다.</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default"
												data-dismiss="modal" 
												onclick="location.href='/board/list'">확인</button>
											<button type="button" class="btn btn-primary" 
												onclick="location.href='/board/register'">등록</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
<%@include file="../includes/footer.jsp" %>