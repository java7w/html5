<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
<!-- <title>List Page...</title> -->
    <title>SB Admin 2 - Bootstrap Admin Theme</title>
    
    <%@include file="../includes/head_info.jsp" %>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
        $(".sidebar-nav")
        .attr("class", "sidebar-nav navbar-collapse collapse")
        .attr("aria-expanded", 'false')
        .attr("style", "height:1px");
    });
    </script>
    
    <script>
    	$(document).ready(function(){
    		var formObj = $("form");
    		$('button').on("click", function(e){
    			e.preventDefault();
    			var operation = $(this).data("oper");
    			alert(operation);
    			if(operation === "remove"){
    				formObj.attr("action", "/board/remove");
    			}else if(operation === "list"){
    				self.location="/board/list";
    				return;
    			}else if(operation === "modify"){
    				formObj.attr("action", "/board/modify");
    			}
    			formObj.submit();
    		});
   		});
    		
    </script>
</head>
<body>

<h3>목록 게시판 </h3>

<%@include file="../includes/header.jsp" %>
<%-- <%request.setCharacterEncoding("euc-kr");%>; --%>

<div class="row">
	<div class="col-1g-12">
		<h1 class="page-header">Board Register</h1>
	</div>
</div>
<div class="row">
	<div class="col-1g-12">
		<div class="panel-heading">Board Modify Page</div>
		<div class="panel-body">
			<form role="form" action="/board/modify" method="post" >
				<div class="form-group">
					<label>Bno</label><input class="form-control" name="bno"
						value="<c:out value='${board.bno}' />" readonly="readonly">
				</div> 
				<div class="form-group">
					<label>Title</label><input class="form-control" name="title"
						value="<c:out value='${board.title}' />" >
				</div> 
				<div class="form-group">
					<label>Contents</label><textarea rows="3" class="form-control" name="content"><c:out value='${board.content}'/></textarea>
				</div>
				<div class="form-group">
					<label>Writer</label><input class="form-control" name="writer"
						value="<c:out value='${board.writer}' />" readonly="readonly">
				</div>
				<div class="form-group" style="display:none;">
					<label>Reg Date</label><input class="form-control" name="regDate"
						value="<fmt:formatDate pattern='yyyy/MM/dd' value='${board.regdate}' />" readonly="readonly">
				</div>
				<div class="form-group" style="display:none;">
					<label>Update Date</label><input class="form-control" name="updateDate"
						value="<fmt:formatDate pattern='yyyy/MM/dd' value='${board.updatedate}' />" readonly="readonly">
				</div>
				<button type="submit" data-oper='modify' class="btn btn-default">수정</button>
				<button type="submit" data-oper='remove' class="btn btn-danger">삭제</button>
				<button type="submit" data-oper='list' class="btn btn-info">목록</button>
			</form>
		</div>
	</div>
</div>
<%@include file="../includes/footer.jsp" %>