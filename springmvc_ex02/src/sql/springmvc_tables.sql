
CREATE TABLE tbl_board(
	bno int AUTO_INCREMENT,
	title VARCHAR(200) NOT NULL,
	content VARCHAR(2000) NOT NULL,
	writer VARCHAR(50) NOT NULL,
	regdate DATE NOT null,
	updatedate DATE NOT null,
	CONSTRAINT pk_tble_bloard PRIMARY KEY(bno)
);
insert into testdb.tbl_board(title, content, writer, regdate, updatedate)
 VALUES('테스트제목', '테스트 내용', '홍길동', NOW(), NOW());
SELECT * FROM testdb.tbl_board;
